package kr.checkbill.request;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TerminalStartActivity extends AppCompatActivity {
    private static final String TAG = "TerminalStartActivity";
    EditText edit_biz_num, edit_tid, edit_ip_address, edit_port_number;
    Button btn_payment, btn_input_values, btn_clear;

    private final int REQUEST_CODE = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminal_start);
        setTitle("단말기 개시 거래");

        edit_biz_num = (EditText) findViewById(R.id.edit_biz_num);
        edit_tid = (EditText) findViewById(R.id.edit_tid);
        edit_ip_address = (EditText) findViewById(R.id.edit_ip_address);
        edit_port_number = (EditText) findViewById(R.id.edit_port_number);
        btn_payment = (Button) findViewById(R.id.btn_payment);
        btn_input_values = (Button) findViewById(R.id.btn_input_values);
        btn_clear = (Button) findViewById(R.id.btn_clear);

        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClassName("kr.checkbill.cb2", "kr.checkbill.cb2.externalcall.ExtMain"); // 패키지명과 클래스명을 적어준다.
                intent.putExtra("tranType", 0);
                intent.putExtra("bizNum", edit_biz_num.getText().toString());
                intent.putExtra("tid", edit_tid.getText().toString());
                intent.putExtra("ipAddress", edit_ip_address.getText().toString());
                intent.putExtra("portNumber", edit_port_number.getText().toString());
                intent.putExtra("id", MainActivity.id);
                intent.putExtra("password", MainActivity.password);
                intent.putExtra("packageName", MainActivity.packageName);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
        btn_input_values.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_biz_num.setText("3128642208");
                edit_tid.setText("11000910");
                edit_ip_address.setText("222.106.99.133");
                edit_port_number.setText("10071");
            }
        });
        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_biz_num.setText("");
                edit_tid.setText("");
                edit_ip_address.setText("");
                edit_port_number.setText("");
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d("requestCode", requestCode + "");
        switch (requestCode){
            case REQUEST_CODE:
                if (resultCode == RESULT_OK){
                    if (data.getStringExtra("resultCode").equals("0")){
                        final AlertDialog alertDialog = new AlertDialog.Builder(TerminalStartActivity.this).create();
                        StringBuilder sb = new StringBuilder();
                        sb.append("응답코드: " + data.getStringExtra("resultCode"));
                        sb.append("\n");
                        sb.append("단말기번호: " + data.getStringExtra("tid"));
                        sb.append("\n");
                        sb.append("개시거래일자: " + data.getStringExtra("date"));
                        sb.append("\n");
                        sb.append("가맹점명: " + data.getStringExtra("shopName"));
                        sb.append("\n");
                        sb.append("가맹전전화번호: " + data.getStringExtra("shopTel"));
                        sb.append("\n");
                        sb.append("가맹점대표자명: " + data.getStringExtra("shopCeo"));
                        sb.append("\n");
                        sb.append("가맹점주소: " + data.getStringExtra("shopAddress"));
                        sb.append("\n");
                        sb.append("관리대리점: " + data.getStringExtra("agencyName"));
                        sb.append("\n");
                        sb.append("관리대리점전화번호: " + data.getStringExtra("agencyTel"));
                        sb.append("\n");
                        sb.append("암호키만료일: " + data.getStringExtra("expireDate"));
                        sb.append("\n");
                        sb.append("스크린메세지: " + data.getStringExtra("screenMessage"));
                        sb.append("\n");

                        Log.d(TAG, "resultCode: " + data.getStringExtra("resultCode"));
                        Log.d(TAG, "Tid: " + data.getStringExtra("tid"));
                        Log.d(TAG, "Date: " + data.getStringExtra("date")); // yyyyMMddHHmmSS
                        Log.d(TAG, "ShopName: : " + data.getStringExtra("shopName"));
                        Log.d(TAG, "ShopTel: " + data.getStringExtra("shopTel"));
                        Log.d(TAG, "ShopCeo: " + data.getStringExtra("shopCeo"));
                        Log.d(TAG, "ShopAddress: " + data.getStringExtra("shopAddress"));
                        Log.d(TAG, "AgencyName: " + data.getStringExtra("agencyName"));
                        Log.d(TAG, "AgencyTel: " + data.getStringExtra("agencyTel"));
                        Log.d(TAG, "ExpireDate: " + data.getStringExtra("expireDate"));
                        Log.d(TAG, "ScreenMessage: " + data.getStringExtra("screenMessage"));

                        alertDialog.setMessage(sb.toString());
                        alertDialog.show();
                    }
                    else {
                        final AlertDialog alertDialog = new AlertDialog.Builder(TerminalStartActivity.this).create();
                        StringBuilder sb = new StringBuilder();
                        sb.append("응답코드: " + data.getStringExtra("resultCode"));
                        sb.append("\n");
                        sb.append("스크린메세지: " + data.getStringExtra("screenMessage"));
                        sb.append("\n");

                        Log.d(TAG, "resultCode: " + data.getStringExtra("resultCode"));
                        Log.d(TAG, "ScreenMessage: " + data.getStringExtra("screenMessage"));
                        alertDialog.setMessage(sb.toString());
                        alertDialog.show();
                    }


                }
                else if (resultCode == RESULT_CANCELED){
                    Toast.makeText(this, "개시거래취소", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}

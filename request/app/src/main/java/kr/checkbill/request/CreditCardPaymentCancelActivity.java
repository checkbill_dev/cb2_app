package kr.checkbill.request;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class CreditCardPaymentCancelActivity extends AppCompatActivity {
    private static final String TAG = "CreditCardPaymentCancelActivity";
    Button btn_payment, btn_input_values, btn_clear;
    private final int REQUEST_CODE = 0;

    EditText edit_tid, edit_total_val, edit_date, edit_approve_num, edit_month;
    CheckBox cb_union_pay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card_payment_cancel);

        setTitle("카드취소");

        edit_tid = (EditText) findViewById(R.id.edit_tid);
        edit_total_val = (EditText) findViewById(R.id.edit_total_val);
        edit_approve_num = (EditText) findViewById(R.id.edit_approve_num);
        edit_date = (EditText) findViewById(R.id.edit_date);
        edit_month = (EditText) findViewById(R.id.edit_month);
        cb_union_pay = (CheckBox) findViewById(R.id.cb_union_pay);
        btn_payment = (Button) findViewById(R.id.btn_payment);
        btn_input_values = (Button) findViewById(R.id.btn_input_values);
        btn_clear = (Button) findViewById(R.id.btn_clear);



        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClassName("kr.checkbill.cb2", "kr.checkbill.cb2.externalcall.ExtMain"); // 패키지명과 클래스명을 적어준다.
                intent.putExtra("tranType", 2);
                intent.putExtra("tid", edit_tid.getText().toString());
                intent.putExtra("totalVal", Integer.parseInt(edit_total_val.getText().toString()));
                intent.putExtra("month", edit_month.getText().toString());
                intent.putExtra("date", edit_date.getText().toString());
                intent.putExtra("approveNum", edit_approve_num.getText().toString());
//                intent.putExtra("unionPay", !cb_union_pay.isChecked() ? 0 : 1);
                intent.putExtra("id", MainActivity.id);
                intent.putExtra("password", MainActivity.password);
                intent.putExtra("packageName", MainActivity.packageName);
                startActivityForResult(intent, REQUEST_CODE);
                Log.d("토탈금액", edit_tid.getText().toString());
            }
        });
        btn_input_values.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_tid.setText("11000910");
                edit_total_val.setText("1004");
                edit_date.setText("");
                edit_approve_num.setText("");
                edit_month.setText("0");
            }
        });
        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_tid.setText("");
                edit_total_val.setText("");
                edit_approve_num.setText("");
                edit_date.setText("");
                edit_month.setText("");
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d("requestCode", requestCode + "");
        switch (requestCode){
            case REQUEST_CODE:
                if (resultCode == RESULT_OK){
                    if (data.getStringExtra("resultCode").equals("0")){
                        final AlertDialog alertDialog = new AlertDialog.Builder(CreditCardPaymentCancelActivity.this).create();
                        StringBuilder sb = new StringBuilder();
                        sb.append("응답코드: " + data.getStringExtra("resultCode"));
                        sb.append("\n");
                        sb.append("단말기번호: " + data.getStringExtra("tid"));
                        sb.append("\n");
                        sb.append("승인날짜: " + data.getStringExtra("date"));
                        sb.append("\n");
                        sb.append("거래고유번호: " + data.getStringExtra("uniqueNum"));
                        sb.append("\n");
                        sb.append("승인번호: " + data.getStringExtra("approveNum"));
                        sb.append("\n");
                        sb.append("발급사: " + data.getStringExtra("issueName"));
                        sb.append("\n");
                        sb.append("매입사: " + data.getStringExtra("cardName"));
                        sb.append("\n");
                        sb.append("할부개월: " + data.getStringExtra("month"));
                        sb.append("\n");
                        sb.append("총금액: " + data.getStringExtra("totalVal"));
                        sb.append("\n");
                        sb.append("잔액: " + data.getStringExtra("balance"));
                        sb.append("\n");
                        sb.append("프린트메세지: " + data.getStringExtra("printMessage"));
                        sb.append("\n");
                        sb.append("스크린메세지: " + data.getStringExtra("screenMessage"));
                        sb.append("\n");

                        Log.d(TAG, "ResultCode: " + data.getStringExtra("resultCode"));
                        Log.d(TAG, "Tid: " + data.getStringExtra("tid"));
                        Log.d(TAG, "Date: " + data.getStringExtra("date")); // yyyyMMddHHmmSS
                        Log.d(TAG, "UniqueNum: : " + data.getStringExtra("uniqueNum"));
                        Log.d(TAG, "ApproveNum: " + data.getStringExtra("approveNum"));
                        Log.d(TAG, "IssueName: " + data.getStringExtra("issueName"));
                        Log.d(TAG, "CardName: " + data.getStringExtra("cardName"));
                        Log.d(TAG, "Month: " + data.getStringExtra("month"));
                        Log.d(TAG, "TotalVal: " + data.getStringExtra("totalVal"));
                        Log.d(TAG, "Balance: " + data.getStringExtra("balance"));
                        Log.d(TAG, "PrintMessage: " + data.getStringExtra("printMessage"));
                        Log.d(TAG, "ScreenMessage: " + data.getStringExtra("screenMessage"));

                        alertDialog.setMessage(sb.toString());
                        alertDialog.show();
                    }
                    else {
                        final AlertDialog alertDialog = new AlertDialog.Builder(CreditCardPaymentCancelActivity.this).create();
                        StringBuilder sb = new StringBuilder();
                        sb.append("응답코드: " + data.getStringExtra("resultCode"));
                        sb.append("\n");
                        sb.append("결제실패: " + data.getStringExtra("message"));
                        sb.append("\n");
                        sb.append("스크린메세지: " + data.getStringExtra("screenMessage"));
                        sb.append("\n");

                        Log.d(TAG, "resultCode: " + data.getStringExtra("resultCode"));
                        Log.d(TAG, "ScreenMessage: " + data.getStringExtra("screenMessage"));
                        alertDialog.setMessage(sb.toString());
                        alertDialog.show();
                    }


                }
                else if (resultCode == RESULT_CANCELED){
                    Toast.makeText(this, "결제취소", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}

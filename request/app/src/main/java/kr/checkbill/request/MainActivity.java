package kr.checkbill.request;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static String id = "testid";
    public static String password = "testpw";
    public static String packageName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        packageName = this.getPackageName();
    }
    public void mOnClick(View v){
        Intent intent;
        switch (v.getId()){
            case R.id.btn_card:
                intent = new Intent(this, CreditCardPaymentActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_card_cancel:
                intent = new Intent(this, CreditCardPaymentCancelActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_cash:
                intent = new Intent(this, CashReceiptPublishActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_cash_cancel:
                intent = new Intent(this, CashReceiptPublishCancelActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_terminal_start:
                intent = new Intent(this, TerminalStartActivity.class);
                startActivity(intent);
                break;
        }
    }
}

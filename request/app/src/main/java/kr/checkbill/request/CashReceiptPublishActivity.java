package kr.checkbill.request;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class CashReceiptPublishActivity extends AppCompatActivity {
    private static final String TAG = "CashReceiptPublishActivity";
    Button btn_payment, btn_input_values, btn_clear;
    RadioButton rbtn_type1, rbtn_type2, rbtn_type3;
    private final int REQUEST_CODE = 0;
    EditText edit_tid, edit_total_val, edit_origin_val, edit_vat_val, edit_service_val;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_receipt_publish);
        setTitle("현금결제");

        edit_tid = (EditText) findViewById(R.id.edit_tid);
        edit_total_val = (EditText) findViewById(R.id.edit_total_val);
        edit_origin_val = (EditText) findViewById(R.id.edit_origin_val);
        edit_vat_val = (EditText) findViewById(R.id.edit_vat_val);
        edit_service_val = (EditText) findViewById(R.id.edit_service_val);
        btn_payment = (Button) findViewById(R.id.btn_payment);
        btn_input_values = (Button) findViewById(R.id.btn_input_values);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        rbtn_type1 = (RadioButton) findViewById(R.id.rbtn_type1);
        rbtn_type2 = (RadioButton) findViewById(R.id.rbtn_type2);
        rbtn_type3 = (RadioButton) findViewById(R.id.rbtn_type3);


        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClassName("kr.checkbill.cb2", "kr.checkbill.cb2.externalcall.ExtMain"); // 패키지명과 클래스명을 적어준다.
                intent.putExtra("tranType", 3);
                intent.putExtra("tid", edit_tid.getText().toString());
                intent.putExtra("totalVal", Integer.parseInt(edit_total_val.getText().toString()));
                intent.putExtra("originVal", Integer.parseInt(edit_origin_val.getText().toString()));
                intent.putExtra("vatVal", Integer.parseInt(edit_vat_val.getText().toString()));
                intent.putExtra("serviceVal", Integer.parseInt(edit_service_val.getText().toString()));
                if (rbtn_type1.isChecked()){
                    intent.putExtra("type", "1");
                }
                else if (rbtn_type2.isChecked()){
                    intent.putExtra("type", "2");
                }
                else if (rbtn_type3.isChecked()){
                    intent.putExtra("type", "3");
                }
                intent.putExtra("id", MainActivity.id);
                intent.putExtra("password", MainActivity.password);
                intent.putExtra("packageName", MainActivity.packageName);
                startActivityForResult(intent, REQUEST_CODE);
                Log.d("토탈금액", edit_tid.getText().toString());
            }
        });
        btn_input_values.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_tid.setText("11000910");
                edit_total_val.setText("1004");
                edit_origin_val.setText("913");
                edit_vat_val.setText("91");
                edit_service_val.setText("0");
                rbtn_type2.setChecked(true);
            }
        });
        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_tid.setText("");
                edit_total_val.setText("");
                edit_origin_val.setText("");
                edit_vat_val.setText("");
                edit_service_val.setText("");
                rbtn_type1.setChecked(false);
                rbtn_type2.setChecked(false);
                rbtn_type3.setChecked(false);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d("requestCode", requestCode + "");
        switch (requestCode){
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (data.getStringExtra("resultCode").equals("0")){
                        final AlertDialog alertDialog = new AlertDialog.Builder(CashReceiptPublishActivity.this).create();
                        StringBuilder sb = new StringBuilder();
                        sb.append("응답코드: " + data.getStringExtra("resultCode"));
                        sb.append("\n");
                        sb.append("단말기번호: " + data.getStringExtra("tid"));
                        sb.append("\n");
                        sb.append("승인날짜: " + data.getStringExtra("date"));
                        sb.append("\n");
                        sb.append("거래고유번호: " + data.getStringExtra("uniqueNum"));
                        sb.append("\n");
                        sb.append("승인번호: " + data.getStringExtra("approveNum"));
                        sb.append("\n");
                        sb.append("부가세: " + data.getStringExtra("vatVal"));
                        sb.append("\n");
                        sb.append("봉사료: " + data.getStringExtra("serviceVal"));
                        sb.append("\n");
                        sb.append("물품가액: " + data.getStringExtra("originVal"));
                        sb.append("\n");
                        sb.append("총금액: " + data.getStringExtra("totalVal"));
                        sb.append("\n");
                        sb.append("프린트메세지: " + data.getStringExtra("printMessage"));
                        sb.append("\n");
                        sb.append("스크린메세지: " + data.getStringExtra("screenMessage"));
                        sb.append("\n");

                        Log.d(TAG, "ResultCode: " + data.getStringExtra("resultCode"));
                        Log.d(TAG, "Tid: " + data.getStringExtra("tid"));
                        Log.d(TAG, "Date: " + data.getStringExtra("date")); // yyyyMMddHHmmSS
                        Log.d(TAG, "UniqueNum: : " + data.getStringExtra("uniqueNum"));
                        Log.d(TAG, "ApproveNum: " + data.getStringExtra("approveNum"));
                        Log.d(TAG, "VatVal: " + data.getStringExtra("vatVal"));
                        Log.d(TAG, "ServiceVal: " + data.getStringExtra("serviceVal"));
                        Log.d(TAG, "OriginVal: " + data.getStringExtra("originVal"));
                        Log.d(TAG, "TotalVal: " + data.getStringExtra("totalVal"));
                        Log.d(TAG, "PrintMessage: " + data.getStringExtra("printMessage"));
                        Log.d(TAG, "ScreenMessage: " + data.getStringExtra("screenMessage"));

                        alertDialog.setMessage(sb.toString());
                        alertDialog.show();
                    }
                    else {
                        final AlertDialog alertDialog = new AlertDialog.Builder(CashReceiptPublishActivity.this).create();
                        StringBuilder sb = new StringBuilder();
                        sb.append("응답코드: " + data.getStringExtra("resultCode"));
                        sb.append("\n");
                        sb.append("결제실패: " + data.getStringExtra("message"));
                        sb.append("\n");
                        sb.append("스크린메세지: " + data.getStringExtra("screenMessage"));
                        sb.append("\n");

                        Log.d(TAG, "resultCode: " + data.getStringExtra("resultCode"));
                        Log.d(TAG, "ScreenMessage: " + data.getStringExtra("screenMessage"));
                        alertDialog.setMessage(sb.toString());
                        alertDialog.show();
                    }


                }
                else if (resultCode == RESULT_CANCELED){
                    Toast.makeText(this, "결제취소", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}

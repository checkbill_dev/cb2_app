package kr.checkbill.cb2.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.checkbill.cbapi.CBApi;

import java.util.Calendar;
import java.util.Date;

import kr.checkbill.cb2.activity.base.DaouActivity;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.Util;

public class MainActivity extends DaouActivity
        implements View.OnClickListener, TextWatcher{
    private static final String TAG = "MainActivity";
    private LinearLayout ll_payment, ll_serach, ll_setting, ll_use_shop_name, ll_use_shop_name_not;
    private RadioButton rbtn_card_payment, rbtn_cash_payment;
    private View v_underline1, v_underline2;
    private EditText edit_password;
    private Button btn_password_input_ok, btn_password_input_ok_tint;
    private TextView txt_agency;

    private DBHelper mDB;
    private boolean mIsSecondCustomPick = false;//범위지정조회에 사용하는 변수
    private Calendar mFirstCal;//범위지정조회에 사용하는 변수

    Button btn_bt_connect, btn_bt_disconnect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ll_payment = (LinearLayout) findViewById(R.id.ll_payment);
        ll_serach = (LinearLayout) findViewById(R.id.ll_serach);
        ll_setting = (LinearLayout) findViewById(R.id.ll_setting);
        ll_use_shop_name = (LinearLayout) findViewById(R.id.ll_use_shop_name);
        ll_use_shop_name_not = (LinearLayout) findViewById(R.id.ll_use_shop_name_not);
        rbtn_card_payment = (RadioButton) findViewById(R.id.rbtn_card_payment);
        rbtn_cash_payment = (RadioButton) findViewById(R.id.rbtn_cash_payment);
        v_underline1 = findViewById(R.id.v_underline1);
        v_underline2 = findViewById(R.id.v_underline2);
        edit_password = (EditText) findViewById(R.id.edit_password);
        btn_password_input_ok = (Button) findViewById(R.id.btn_ok);
        btn_password_input_ok_tint = (Button) findViewById(R.id.btn_ok_tint);
        txt_agency = (TextView) findViewById(R.id.txt_agency);

        btn_bt_connect = (Button) findViewById(R.id.btn_bt_connect);
        btn_bt_disconnect = (Button) findViewById(R.id.btn_bt_disconnect);


        mDB = new DBHelper(this);
        mDB.deletePaymentHistoryFor80days();
        mDB.setSystemSwAuthNum(DAOU_GetSWNum());

        SetText(R.id.txt_sw_auth_num, DAOU_GetSWNum());

        registerOnClickListener();
        registerTextWatcher();


    }

    @Override
    public void onClick(View v) {
        Intent intent;
        String startDate;
        String endDate;

        Calendar cal = Calendar.getInstance();
        Date startWeekDate = new Date();
        Date endWeekDate = new Date();

        int thisweek, monday, saturday; // 이번주 지난주 계산에사용
        int firstDay, today, lastDay, firstDayMinus, lastDayMinus; // 이번달 지난달 계산에 사용
        int year, month, day; //일간조회, 월간조회에 이용
        DatePickerDialog dlg; //일간조회, 월간조회에 이용
        String cardOrCash = null;
        if (rbtn_card_payment.isChecked()){
            cardOrCash = "card";
        }
        else if (rbtn_cash_payment.isChecked()){
            cardOrCash = "cash";
        }
        switch (v.getId()) {
            case R.id.btn_bt_connect:
                final boolean packageBT = !this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
                if(packageBT){
                    Toast.makeText(MainActivity.this, "블루투스를 지원하지 않습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (BT_IsConnect() == false) {
                    //showDialog("리더기와 통신중입니다....");

                    if (mDB.getSystemBtAutoConnect().equals("0")) { // 자동연결아님
                        BT_Connect(null);//블루투스 연결요청메소드
                    }
                    else if (mDB.getSystemBtAutoConnect().equals("1")) { // 자동연결
                        if (mDB.getSystemRtcBtNum().equals("0")) { // 최근에 연결한 기기가 없을경우 리스트를 띄워서 블루투스연결
                            BT_Connect(null);//블루투스 연결요청메소드
                        }
                        else { // 최근에 연결한 기기가 있을경우 리스트를 띄워서 블루투스연결
                            BT_Connect(mDB.getSystemRtcBtNum());//주소를 이용한 블루투스연결
                        }
                    }

                } else {
                    Toast.makeText(MainActivity.this, "이미 블루투스 연결 중입니다.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_bt_disconnect:
                SetText(R.id.txt_hw_auth_num, "");// 간혹 블루투스가 해제되도 텍스트 클리어가 안되는 경우가 있어서 버튼안에다가 값이 클리어되도록 해줫음
                BT_Disconnect();
                break;
            case R.id.btn_credit_card_payment:
                if (BT_IsConnect()){
                    if (mDB.getSystemUseTid().equals("0")){
                        showAlertDialog("알림", new String[]{"단말기 개시 거래를 한후에 이용해주세요."}, true,
                                true, false, false, "확인", null, null, null, null, null);
                    }
                    else{
                        intent = new Intent(MainActivity.this, BizListActivity.class);
                        intent.putExtra("CallReason", "MainActivityCard");
                        startActivity(intent);
                    }
                } else {
                    showAlertDialog("알림", new String[]{"앱을 리더기와 연결을 한 후에 이용해주세요."}, true,
                            true, false, false, "확인", null, null, null, null, null);
                }


                break;
            case R.id.btn_credit_card_payment_search:
                ll_payment.setVisibility(View.GONE);
                ll_serach.setVisibility(View.VISIBLE);
                ll_setting.setVisibility(View.GONE);
                rbtn_card_payment.setChecked(true);
                rbtn_cash_payment.setChecked(false);
                v_underline1.setVisibility(View.VISIBLE);
                v_underline2.setVisibility(View.INVISIBLE);

                break;
            case R.id.btn_cash_receipt_p​ublish:


                if (BT_IsConnect()){
                    if (mDB.getSystemUseTid().equals("0")){
                        showAlertDialog("알림", new String[]{"단말기 개시 거래를 한후에 이용해주세요."}, true,
                                true, false, false, "확인", null, null, null, null, null);
                    }
                    else{
                        intent = new Intent(MainActivity.this, BizListActivity.class);
                        intent.putExtra("CallReason", "MainActivityCash");
                        startActivity(intent);
                    }
                } else {
                    showAlertDialog("알림", new String[]{"앱을 리더기와 연결을 한 후에 이용해주세요."}, true,
                            true, false, false, "확인", null, null, null, null, null);
                }



                break;
            case R.id.btn_cash_payment_search:
                ll_payment.setVisibility(View.GONE);
                ll_serach.setVisibility(View.VISIBLE);
                ll_setting.setVisibility(View.GONE);
                rbtn_card_payment.setChecked(false);
                rbtn_cash_payment.setChecked(true);
                v_underline1.setVisibility(View.INVISIBLE);
                v_underline2.setVisibility(View.VISIBLE);
                break;
            case R.id.rbtn_card_payment:
                v_underline1.setVisibility(View.VISIBLE);
                v_underline2.setVisibility(View.INVISIBLE);
                break;
            case R.id.rbtn_cash_payment:
                v_underline1.setVisibility(View.INVISIBLE);
                v_underline2.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_search_payment_history_today:
                startDate = DBHelper.mStartSdf.format(new Date());
                endDate = DBHelper.mEndSdf.format(new Date());
                intent = new Intent(MainActivity.this, PayListActivity.class);
                intent.putExtra("CallReason", "frame2");
                intent.putExtra("CardOrCash", cardOrCash);
                intent.putExtra("StartDate", startDate);
                intent.putExtra("EndDate", endDate);
                startActivity(intent);
                break;
            case R.id.btn_search_payment_history_yesterday:
                Date yesterday = new Date();
                yesterday.setDate(yesterday.getDate()-1);
                startDate = DBHelper.mStartSdf.format(yesterday);
                endDate = DBHelper.mEndSdf.format(yesterday);
                intent = new Intent(MainActivity.this, PayListActivity.class);
                intent.putExtra("CallReason", "frame2");
                intent.putExtra("CardOrCash", cardOrCash);
                intent.putExtra("StartDate", startDate);
                intent.putExtra("EndDate", endDate);
                startActivity(intent);
                break;
            case R.id.btn_search_payment_history_this_week:
                startWeekDate = new Date();
                endWeekDate = new Date();
                thisweek = cal.get(Calendar.DAY_OF_WEEK);//오늘이 몇번째날인지
                monday = thisweek - 1; // 오늘과 일요일과의 차, 오늘에서 이것을 빼주면 월요일이 된다.
                saturday = -(thisweek - 7); // 오늘과 토요일의 차, 오늘에서 이것을 더해주면 토요일이된다.
                startWeekDate.setDate(startWeekDate.getDate() - monday);
                endWeekDate.setDate(endWeekDate.getDate() + saturday);

                startDate = DBHelper.mStartSdf.format(startWeekDate);
                endDate = DBHelper.mEndSdf.format(endWeekDate);

                intent = new Intent(MainActivity.this, PayListActivity.class);
                intent.putExtra("CallReason", "frame2");
                intent.putExtra("CardOrCash", cardOrCash);
                intent.putExtra("StartDate", startDate);
                intent.putExtra("EndDate", endDate);
                startActivity(intent);

                break;
            case R.id.btn_search_payment_history_last_week:
                thisweek = cal.get(Calendar.DAY_OF_WEEK);//일주일전 오늘이 몇번째날인지
                monday = thisweek - 1; // 오늘과 일요일과의 차, 오늘에서 이것을 빼주면 월요일이 된다.
                saturday = -(thisweek - 7); // 오늘과 토요일의 차, 오늘에서 이것을 더해주면 토요일이된다.

                startWeekDate.setDate(startWeekDate.getDate() - monday - 7); // 일주일 전이므로 7씩 빼준다.
                endWeekDate.setDate(endWeekDate.getDate() + saturday -7); // 일주일 전이므로 7씩 빼준다.

                startDate = DBHelper.mStartSdf.format(startWeekDate);
                endDate = DBHelper.mEndSdf.format(endWeekDate);
                intent = new Intent(MainActivity.this, PayListActivity.class);
                intent.putExtra("CallReason", "frame2");
                intent.putExtra("CardOrCash", cardOrCash);
                intent.putExtra("StartDate", startDate);
                intent.putExtra("EndDate", endDate);
                startActivity(intent);
                break;
            case R.id.btn_search_payment_history_this_month:
                firstDay = cal.getMinimalDaysInFirstWeek(); // 이번달의 첫째날
                today = cal.get(Calendar.DAY_OF_MONTH); // 오늘의 일자
                lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH); // 이번달의 마지막 날짜의 일자
                firstDayMinus = today - firstDay; // 오늘에서 첫번째날을 짼 차, 오늘날짜에 이것을 빼주면 첫날이된다.
                lastDayMinus = lastDay - today; //마지막날에서 오늘을 뺀 차, 오늘날짜에다가 이것을 더해주면 마지막 날이된다.

                startWeekDate.setDate(startWeekDate.getDate() - firstDayMinus);
                endWeekDate.setDate(endWeekDate.getDate() + lastDayMinus);

                startDate = DBHelper.mStartSdf.format(startWeekDate);
                endDate = DBHelper.mEndSdf.format(endWeekDate);
                intent = new Intent(MainActivity.this, PayListActivity.class);
                intent.putExtra("CallReason", "frame2");
                intent.putExtra("CardOrCash", cardOrCash);
                intent.putExtra("StartDate", startDate);
                intent.putExtra("EndDate", endDate);
                startActivity(intent);
                break;
            case R.id.btn_search_payment_history_last_month:
                cal.add(Calendar.MONTH, -1);
                cal.set(Calendar.DAY_OF_MONTH,1);
                int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                Date date = cal.getTime();
                startDate = DBHelper.mStartSdf.format(date);
                cal.set(Calendar.DAY_OF_MONTH, maxDay);
                date = cal.getTime();
                endDate = DBHelper.mEndSdf.format(date);

                intent = new Intent(MainActivity.this, PayListActivity.class);
                intent.putExtra("CallReason", "frame2");
                intent.putExtra("CardOrCash", cardOrCash);
                intent.putExtra("StartDate", startDate);
                intent.putExtra("EndDate", endDate);
                startActivity(intent);
                break;
            case R.id.btn_search_payment_history_date_unit:
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);

                final String finalCardOrCash1 = cardOrCash;
                dlg = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    int nCheck = 0;//DateTimeFicker에 두번 실행되는 버그가 잇어서 한번만 실행되도록 해주는 변수
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (nCheck == 0){
                            nCheck = 1;
                            LOG.i(TAG,"선택된 year:"+year+" month:"+monthOfYear+ " day:"+dayOfMonth);
                            Calendar cal = Calendar.getInstance();
                            cal.set(Calendar.YEAR , year);
                            cal.set(Calendar.MONTH, monthOfYear);
                            cal.set(Calendar.DAY_OF_MONTH , dayOfMonth);
                            Date date = cal.getTime();

                            String startDate = DBHelper.mStartSdf.format(date);
                            String endDate = DBHelper.mEndSdf.format(date);
                            LOG.i(TAG,"startDate:"+startDate+" endDate:"+endDate);

                            Intent intent = new Intent(MainActivity.this, PayListActivity.class);
                            intent.putExtra("CallReason", "frame2");
                            intent.putExtra("CardOrCash", finalCardOrCash1);
                            intent.putExtra("StartDate", startDate);
                            intent.putExtra("EndDate", endDate);
                            startActivity(intent);
                        }
                    }
                }, year, month, day);
                dlg.setTitle("일별조회");
                dlg.show();
                break;
            case R.id.btn_search_payment_history_month_unit:
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);
                final String finalCardOrCash = cardOrCash;
                dlg = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    int nCheck = 0; //DateTimeFicker에 두번 실행되는 버그가 잇어서 한번만 실행되도록 해주는 변수
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (nCheck == 0){
                            nCheck = 1;
                            LOG.i(TAG,"선택된 year:"+year+" month:"+monthOfYear+ " day:"+dayOfMonth);
                            Calendar cal = Calendar.getInstance();
                            cal.set(Calendar.YEAR , year);
                            cal.set(Calendar.MONTH, monthOfYear);
                            cal.set(Calendar.DAY_OF_MONTH , 1);



                            Date date = cal.getTime();
                            String startDate = DBHelper.mStartSdf.format(date);

                            int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
                            cal.set(Calendar.DAY_OF_MONTH, maxDay);
                            date = cal.getTime();

                            String endDate = DBHelper.mEndSdf.format(date);
                            LOG.i(TAG,"startDate:"+startDate+" endDate:"+endDate);

                            Intent intent = new Intent(MainActivity.this, PayListActivity.class);
                            intent.putExtra("CallReason", "frame2");
                            intent.putExtra("CardOrCash", finalCardOrCash);
                            intent.putExtra("StartDate", startDate);
                            intent.putExtra("EndDate", endDate);
                            startActivity(intent);
                        }
                    }
                }, year, month, day);
                dlg.setTitle("월별조회");
                dlg.show();
                break;
            case R.id.btn_search_payment_history_range_designation:
                year = cal.get(Calendar.YEAR);
                month = cal.get(Calendar.MONTH);
                day = cal.get(Calendar.DAY_OF_MONTH);
                final String finalCardOrCash2 = cardOrCash;
                dlg = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    int nCheck = 0; //DateTimeFicker에 두번 실행되는 버그가 잇어서 한번만 실행되도록 해주는 변수
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (nCheck == 0){
                            nCheck = 1;
                            LOG.i(TAG,"선택된 year:"+year+" month:"+monthOfYear+ " day:"+dayOfMonth);
                            if( !mIsSecondCustomPick){
                                mFirstCal = Calendar.getInstance();
                                mFirstCal.set(Calendar.YEAR , year);
                                mFirstCal.set(Calendar.MONTH, monthOfYear);
                                mFirstCal.set(Calendar.DAY_OF_MONTH , dayOfMonth);
                                mIsSecondCustomPick=true;

                                DatePickerDialog dlg = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                                    int nnCheck = 0; //DateTimeFicker에 두번 실행되는 버그가 잇어서 한번만 실행되도록 해주는 변수
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        if (nnCheck == 0){
                                            nnCheck = 1;
                                            mIsSecondCustomPick=false;
                                            Calendar secondCal = Calendar.getInstance();
                                            secondCal.set(Calendar.YEAR , year);
                                            secondCal.set(Calendar.MONTH, monthOfYear);
                                            secondCal.set(Calendar.DAY_OF_MONTH , dayOfMonth);

                                            Date date = mFirstCal.getTime();
                                            String startDate = DBHelper.mStartSdf.format(date);

                                            date = secondCal.getTime();
                                            String endDate = DBHelper.mEndSdf.format(date);
                                            LOG.i(TAG,"startDate:"+startDate+" endDate:"+endDate);

                                            Intent intent = new Intent(MainActivity.this, PayListActivity.class);
                                            intent.putExtra("CallReason", "frame2");
                                            intent.putExtra("CardOrCash", finalCardOrCash2);
                                            intent.putExtra("StartDate" , startDate);
                                            intent.putExtra("EndDate" , endDate);
                                            if (rbtn_card_payment.isChecked()){
                                                intent.putExtra("TranType", "creditCard");
                                            }
                                            else if(rbtn_cash_payment.isChecked()){
                                                intent.putExtra("TranType", "cash");
                                            }
                                            startActivity(intent);
                                        }
                                    }
                                }, year, monthOfYear, dayOfMonth);
                                dlg.setTitle("조회종료일");
                                dlg.show();
                            }else{

                            }
                        }
                    }
                }, year, month, day);
                dlg.setTitle("조회시작일");
                dlg.show();
                break;
            case R.id.txt_agency_tel:
                intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + GetText(R.id.txt_agency_tel)));
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //전화 퍼미션 체크
                    return;
                }
                startActivity(intent);
                break;
            case R.id.btn_ok:
                if(GetText(R.id.edit_password).length()!=6){
                    showAlertDialog("알림", new String[]{"비밀번호를 정확히 입력 해주세요."}, true,
                            true, false, false, "확인", null, null, null, null, null);
                }
                else{
                    if (!BT_IsConnect()) {
                        showAlertDialog("알림", new String[]{"앱을 리더기와 연결을 한 후에 이용해주세요."}, true,
                                true, false, false, "확인", null, null, null, null, null);
                    }
                    else if(mDB.getSystemPassword().equals(GetText(R.id.edit_password))){
                        intent = new Intent(MainActivity.this, SettingActivity.class);
                        startActivity(intent);
                        edit_password.requestFocus();
                        SetText(R.id.edit_password, "");
                    }
                    else if(GetText(R.id.edit_password).equals("111111")) {
                        mDB.setSystemEullYeonOn();
                        showToast("은련카드기능ON");
                    }
                    else if(GetText(R.id.edit_password).equals("222222")) {
                        mDB.setSystemEullYeonOff();
                        showToast("은련카드기능OFF");
                    }
                    else{
                        showAlertDialog("알림", new String[]{"비밀번호가 일치하지 않습니다."}, true,
                                true, false, false, "확인", null, null, null, null, null);
                        SetText(R.id.edit_password, "");
                        edit_password.requestFocus();
                    }
                }
                break;
            case R.id.ll_change_search1:
                ll_payment.setVisibility(View.GONE);
                ll_serach.setVisibility(View.VISIBLE);
                ll_setting.setVisibility(View.GONE);
                rbtn_card_payment.setChecked(true);
                rbtn_cash_payment.setChecked(false);
                v_underline1.setVisibility(View.VISIBLE);
                v_underline2.setVisibility(View.INVISIBLE);
                SetText(R.id.edit_password, "");
                break;
            case R.id.ll_change_setting1:
                ll_payment.setVisibility(View.GONE);
                ll_serach.setVisibility(View.GONE);
                ll_setting.setVisibility(View.VISIBLE);
                SetText(R.id.edit_password, "");
                break;
            case R.id.ll_change_payment2:
                ll_payment.setVisibility(View.VISIBLE);
                ll_serach.setVisibility(View.GONE);
                ll_setting.setVisibility(View.GONE);
                SetText(R.id.edit_password, "");
                break;
            case R.id.ll_change_setting2:
                ll_payment.setVisibility(View.GONE);
                ll_serach.setVisibility(View.GONE);
                ll_setting.setVisibility(View.VISIBLE);
                SetText(R.id.edit_password, "");
                break;
            case R.id.ll_change_payment3:
                ll_payment.setVisibility(View.VISIBLE);
                ll_serach.setVisibility(View.GONE);
                ll_setting.setVisibility(View.GONE);
                SetText(R.id.edit_password, "");
                break;
            case R.id.ll_change_search3:
                ll_payment.setVisibility(View.GONE);
                ll_serach.setVisibility(View.VISIBLE);
                ll_setting.setVisibility(View.GONE);
                rbtn_card_payment.setChecked(true);
                rbtn_cash_payment.setChecked(false);
                v_underline1.setVisibility(View.VISIBLE);
                v_underline2.setVisibility(View.INVISIBLE);
                SetText(R.id.edit_password, "");
                break;
        }
    }
    private void registerOnClickListener(){
        (findViewById(R.id.btn_bt_connect)).setOnClickListener(this);
        (findViewById(R.id.btn_bt_disconnect)).setOnClickListener(this);
        (findViewById(R.id.btn_credit_card_payment)).setOnClickListener(this);
        (findViewById(R.id.btn_credit_card_payment_search)).setOnClickListener(this);
        (findViewById(R.id.btn_cash_receipt_p​ublish)).setOnClickListener(this);
        (findViewById(R.id.btn_cash_receipt_p​ublish)).setOnClickListener(this);
        (findViewById(R.id.btn_cash_payment_search)).setOnClickListener(this);
        (findViewById(R.id.btn_search_payment_history_today)).setOnClickListener(this);
        (findViewById(R.id.btn_search_payment_history_yesterday)).setOnClickListener(this);
        (findViewById(R.id.btn_search_payment_history_this_week)).setOnClickListener(this);
        (findViewById(R.id.btn_search_payment_history_last_week)).setOnClickListener(this);
        (findViewById(R.id.btn_search_payment_history_this_month)).setOnClickListener(this);
        (findViewById(R.id.btn_search_payment_history_last_month)).setOnClickListener(this);
        (findViewById(R.id.btn_search_payment_history_date_unit)).setOnClickListener(this);
        (findViewById(R.id.btn_search_payment_history_month_unit)).setOnClickListener(this);
        (findViewById(R.id.btn_search_payment_history_range_designation)).setOnClickListener(this);
        (findViewById(R.id.txt_agency_tel)).setOnClickListener(this);
        (findViewById(R.id.ll_change_search1)).setOnClickListener(this);
        (findViewById(R.id.ll_change_setting1)).setOnClickListener(this);
        (findViewById(R.id.ll_change_payment2)).setOnClickListener(this);
        (findViewById(R.id.ll_change_setting2)).setOnClickListener(this);
        (findViewById(R.id.ll_change_payment3)).setOnClickListener(this);
        (findViewById(R.id.ll_change_search3)).setOnClickListener(this);
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (edit_password.hasFocus()) {
            if (GetText(R.id.edit_password).length() == 6) {
                btn_password_input_ok.setVisibility(View.VISIBLE);
                btn_password_input_ok_tint.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void DAOU_GetHWNum_CB(String hwNum) {
        super.DAOU_GetHWNum_CB(hwNum);
        if(hwNum != null) {
            SetText(R.id.txt_hw_auth_num, hwNum);
        }
    }
    @Override
    public void BT_CB(final int resultCode, String BTAdress) {
        super.BT_CB(resultCode, BTAdress);
        runOnUiThread(new Runnable() {
            public void run() {
                switch (resultCode) {
                    case CBApi.BT_DISCONNECTED:
                        SetText(R.id.txt_hw_auth_num, "");
                        break;
                }
            }
        });
    }

    @Override
    public void onBackPressed() { // 뒤로가기 종료기능
        if (ll_payment.getVisibility() == View.VISIBLE){
            View.OnClickListener event = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    moveTaskToBack(true);
                    finish();
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            };
            showAlertDialog("알림", new String[]{"체크빌을 종료하시겠습니까?"}, true,
                    true, false, false, "종료", "취소", null, event, null, null);
        }
        else if (ll_serach.getVisibility() == View.VISIBLE
                || ll_setting.getVisibility() == View.VISIBLE){
            ll_payment.setVisibility(View.VISIBLE);
            ll_serach.setVisibility(View.GONE);
            ll_setting.setVisibility(View.GONE);
            SetText(R.id.edit_password, "");
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_notice_list) {
            String url ="http://www.checkbill.kr/board/tot.list.php?BBS_GUBUN=1";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_device_list) {
            String url ="http://www.checkbill.kr/board/compat.list.php";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        super.onResume();
        SetText(R.id.txt_hw_auth_num, getHwNum());

        BizInfo bizInfo = Util.getBizInfo(this);

        if (bizInfo.shopName == null){
            ll_use_shop_name.setVisibility(View.GONE);
            ll_use_shop_name_not.setVisibility(View.VISIBLE);
        }
        else {
            txt_agency.setVisibility(View.VISIBLE);
            SetText(R.id.txt_use_shop_name, bizInfo.shopName);
            SetText(R.id.txt_agency_name, bizInfo.agencyName);
            SpannableString sps = new SpannableString(bizInfo.agencyTel); // 밑줄 쳐주는 코드
            sps.setSpan(new UnderlineSpan(), 0, sps.length(), 0);
            SetText(R.id.txt_agency_tel, sps);
            ll_use_shop_name.setVisibility(View.VISIBLE);
            ll_use_shop_name_not.setVisibility(View.GONE);
        }
    }

    private void registerTextWatcher() {
        edit_password.addTextChangedListener(this);
    }

    /**
     * 안쓰는 메소드들
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void afterTextChanged(Editable s) {
    }


}



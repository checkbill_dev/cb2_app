package kr.checkbill.cb2.externalcall;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.checkbill.cbapi.CBApi;
import com.checkbill.cbapi.ResultPayment;

import java.text.NumberFormat;

import kr.checkbill.cb2.activity.base.DaouPayActivity;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.SignPadActivity;
import kr.checkbill.cb2.util.LOG;

public class ExtCardPayActivity extends DaouPayActivity
        implements View.OnClickListener {
    private static final String TAG = "ExtCreditCardPaymentActivity";

    private Button btn_ok, btn_ok_tint;
    private CheckBox cb_eullyeon;

    private static final int REQUEST_CODE = 1;

    private String tid;
    private int totalVal;
    private int originVal;
    private int vatVal;
    private int serviceVal;
    private String month;
//    private int unionPay;
    private String id;
    private String password;
    private String packageName;
    private boolean isWaitSign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ext_card_pay);

        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok_tint = (Button) findViewById(R.id.btn_ok_tint);
        cb_eullyeon = (CheckBox) findViewById(R.id.cb_eullyeon);

        tid = getIntent().getStringExtra("tid");
        totalVal = getIntent().getIntExtra("totalVal", -1);
        originVal = getIntent().getIntExtra("originVal", -1);
        vatVal = getIntent().getIntExtra("vatVal", -1);
        serviceVal = getIntent().getIntExtra("serviceVal", -1);
        month = getIntent().getStringExtra("month");
//        unionPay = getIntent().getIntExtra("unionPay", -1); // 0:은련카드아님 1:은련카드
        id = getIntent().getStringExtra("id");
        password = getIntent().getStringExtra("password");
        packageName = getIntent().getStringExtra("packageName");

        LOG.d(TAG, "tid : " + tid);
        LOG.d(TAG, "totalVal : " + totalVal);
        LOG.d(TAG, "originVal : " + originVal);
        LOG.d(TAG, "vatVal : " + vatVal);
        LOG.d(TAG, "serviceVal : " + serviceVal);
        LOG.d(TAG, "month : " + month);
//        LOG.d(TAG, "unionPay : " + unionPay);
        LOG.d(TAG, "id : " + id);
        LOG.d(TAG, "password : " + password);
        LOG.d(TAG, "packageName : " + packageName);

        SetText(R.id.txt_sw_auth_num, DAOU_GetSWNum());

        registerOnClickListener();

        NumberFormat nf = NumberFormat.getInstance();
        String commaTotalVal = nf.format(totalVal);

        SetText(R.id.txt_amount, commaTotalVal);
        if (month.equals("0") || month.equals("1")){
            month = "0";
            SetText(R.id.txt_month, "일시불");
        }
//        if (unionPay == 0) {
//            cb_eullyeon.setVisibility(View.INVISIBLE);
//            cb_eullyeon.setChecked(false);
//        }
//        else if (unionPay == 1) {
//            cb_eullyeon.setVisibility(View.VISIBLE);
//            cb_eullyeon.setChecked(true);
//        }

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                if (cb_eullyeon.isChecked()) { // 은련카드일때는 50,000 원 초과 검사를 하지않는다.(무조건 싸인을 받음)
                    Intent intent = new Intent(this, SignPadActivity.class);
                    intent.putExtra("Amount", GetText(R.id.edit_input_val));
                    intent.putExtra("IsEullyeon", "eullyeon_yes");
                    startActivityForResult(intent, REQUEST_CODE);
                }
                else {
                    if (totalVal > 50000) {
                        Intent intent = new Intent(this, SignPadActivity.class);
                        intent.putExtra("Amount", GetText(R.id.edit_input_val));
                        intent.putExtra("IsEullyeon", "eullyeon_no");
                        startActivityForResult(intent, REQUEST_CODE);
                    }
                    else {
                        DAOU_Credit(PaymentHistory.TRANTYPE_CREDIT_CARD, null, null,
                                String.valueOf(totalVal), String.valueOf(originVal),
                                String.valueOf(vatVal), String.valueOf(serviceVal),
                                month, 0);
                    }
                }
                break;
        }
    }

    @Override
    public void BT_CB(final int resultCode, String BTAdress) {
        super.BT_CB(resultCode, BTAdress);
        runOnUiThread(new Runnable() {
            public void run() {
                switch (resultCode) {
                    case CBApi.BT_DISCONNECTED:
                        SetText(R.id.txt_hw_auth_num, "");
                        break;
                    case CBApi.BT_CANCELED:
                        Intent intent = new Intent();
                        intent.putExtra("message", "블루투스가 취소 되었습니다.");
                        setResult(RESULT_CANCELED, intent);
                        finish();
                        break;
                }
            }
        });
    }
    @Override
    public void DAOU_GetHWNum_CB(final String hwNum) {
        super.DAOU_GetHWNum_CB(hwNum);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(hwNum != null) {
                    SetText(R.id.txt_hw_auth_num, hwNum);
                }
            }
        });
    }



    @Override
    public void DAOU_Payment_CB(final ResultPayment resultPayment) {
        super.DAOU_Payment_CB(resultPayment);
        runOnUiThread(new Runnable() {
            public void run() {
                if (resultPayment == null){
                    LOG.e(TAG, "resultPayment is NULL");
                    return;
                }
                runOnUiThread(new Runnable() {
                    public void run() {
                        BT_Disconnect();
                        if (resultPayment.resultCode == 0){
                            Intent intent = new Intent();
                            intent.putExtra("resultCode", resultPayment.resultCode + ""); // resultCode
                            intent.putExtra("tid", resultPayment.termNum + ""); // 단말기번호
                            intent.putExtra("date", resultPayment.date + ""); // 승인날짜 yyyyMMddHHmmss
                            intent.putExtra("uniqueNum", resultPayment.uniqueNum + ""); // 거래고유번호
                            intent.putExtra("approveNum", resultPayment.approveNum + ""); // 승인번호
                            intent.putExtra("issueName", resultPayment.issueName + ""); // 발급사
                            intent.putExtra("cardName", resultPayment.cardName + ""); // 매입사
                            intent.putExtra("month", resultPayment.option + ""); // 할부개월
                            intent.putExtra("vatVal", resultPayment.taxPrice + ""); // 부가세
                            intent.putExtra("serviceVal", resultPayment.servicePrice + ""); // 봉사료
                            intent.putExtra("originVal", resultPayment.taxFreePrice + ""); // 물품가액
                            intent.putExtra("totalVal", resultPayment.totalPrice + ""); // 총금액
                            intent.putExtra("balance", resultPayment.balance + ""); // 잔액
                            intent.putExtra("printMessage", resultPayment.printMessage); // 프린트 메세지
                            intent.putExtra("screenMessage", resultPayment.screenMessage); // 스크린 메세지

                            LOG.d(TAG, "resultCode: " + resultPayment.resultCode + "");
                            LOG.d(TAG, "tid: " + resultPayment.termNum + "");
                            LOG.d(TAG, "date: " + resultPayment.date + "");
                            LOG.d(TAG, "uniqueNum: " + resultPayment.uniqueNum + "");
                            LOG.d(TAG, "approveNum: " + resultPayment.approveNum + "");
                            LOG.d(TAG, "issueName: " + resultPayment.issueName + "");
                            LOG.d(TAG, "cardName: " + resultPayment.cardName + "");
                            LOG.d(TAG, "month: " + resultPayment.option + "");
                            LOG.d(TAG, "vatVal: " + resultPayment.taxPrice + "");
                            LOG.d(TAG, "serviceVal: " + resultPayment.servicePrice + "");
                            LOG.d(TAG, "originVal: " + resultPayment.taxFreePrice + "");
                            LOG.d(TAG, "totalVal: " + resultPayment.totalPrice + "");
                            LOG.d(TAG, "balance: " + resultPayment.balance + "");
                            LOG.d(TAG, "printMessage: " + resultPayment.printMessage + "");
                            LOG.d(TAG, "screenMessage: " + resultPayment.screenMessage + "");
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                        else if (resultPayment.resultCode != 0){
                            Intent intent = new Intent();
                            intent.putExtra("resultCode", resultPayment.resultCode + "");
                            intent.putExtra("screenMessage", resultPayment.screenMessage); // 스크린 메세지

                            LOG.d(TAG, "resultCode: " + resultPayment.resultCode + "");
                            LOG.d(TAG, "ScreenMessage: " + resultPayment.screenMessage);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                });
            }
        });
    }
    @Override
    public void DAOU_OnICInsert() {
        super.DAOU_OnICInsert();
    }

    @Override
    public void DAOU_OnICRemove() {
        super.DAOU_OnICRemove();
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }
    @Override
    public void DAOU_OnFallback(final int fallback) {
        super.DAOU_OnFallback(fallback);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }
    @Override
    public void DAOU_OnMSRRead() {
        super.DAOU_OnMSRRead();
    }
    @Override
    public void DAOU_OnCardData(final String maskedCarData) {
        super.DAOU_OnCardData(maskedCarData);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "****-####-####-****");
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void DAOU_Error_CB(final int code) {
        super.DAOU_Error_CB(code);
    }
    @Override
    public void DAOU_CardError_CB(int code){
        super.DAOU_CardError_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }
    @Override
    public void DAOU_Payment_Incomplete(int resultCode, String message) {
        super.DAOU_Payment_Incomplete(resultCode, message);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == RESULT_OK){
                    byte[] signByteArray = data.getByteArrayExtra("SignData");
                    String eullyeon = data.getStringExtra("IsEullyeon");
                    String password = data.getStringExtra("Password");

                    Bitmap bitmap = BitmapFactory.decodeByteArray(signByteArray, 0, signByteArray.length);

                    if (eullyeon.equals("eullyeon_yes")) {
                        DAOU_Credit(PaymentHistory.TRANTYPE_CREDIT_CARD, password, bitmap,
                                String.valueOf(totalVal), String.valueOf(originVal),
                                String.valueOf(vatVal), String.valueOf(serviceVal),
                                month, 0);
                    }
                    else {
                        DAOU_Credit(PaymentHistory.TRANTYPE_CREDIT_CARD, null, bitmap,
                                String.valueOf(totalVal), String.valueOf(originVal),
                                String.valueOf(vatVal), String.valueOf(serviceVal),
                                month, 0);
                    }



                }
                else if (resultCode == RESULT_CANCELED){
                    BT_Disconnect();
                    Intent intent = new Intent();
                    setResult(RESULT_CANCELED, intent);
                    finish();
                }
                break;
        }
    }
    @Override
    public void onBackPressed() {
        BT_Disconnect();
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        final boolean packageBT = !this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        if(packageBT){
            showToast("BLE를 지원하지 않습니다.");
            return;
        }

        if (BT_IsConnect() == false) {
            LOG.d(TAG, "블루투스 연결요청");

            if(isWaitSign == false) {
                BT_Connect(null);
            }
            else {
                isWaitSign = false;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(BT_IsConnect() == true) {

            if(isWaitSign == false) {
                BT_Disconnect();
            }


        }
    }
    private void registerOnClickListener() {
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }
}

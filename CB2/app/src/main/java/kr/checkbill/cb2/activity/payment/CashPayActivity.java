package kr.checkbill.cb2.activity.payment;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.checkbill.cbapi.ResultPayment;

import kr.checkbill.cb2.activity.base.DaouPayActivity;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.ResultPrice;
import kr.checkbill.cb2.util.Util;

public class CashPayActivity extends DaouPayActivity
        implements View.OnClickListener, TextWatcher, View.OnFocusChangeListener {
    private final static String TAG = "CashReceiptPublishActivity";
    private TextView txt_cash_number_title;
    private EditText edit_cash_receipt_number, edit_input_val, edit_service_val;
    private Button btn_ok, btn_ok_tint;
    private RadioButton rbtn_defalut_receipt, rbtn_cash_receipt, rbtn_income_deduction, rbtn_expense_evidence, rbtn_voluntary_issue;
    private View v_center_space, v_underline1, v_underline2, v_underline3, v_underline4, v_underline5;
    private LinearLayout ll_cash_receipt_group, ll_bottom_space;
    private FrameLayout ll_cash_number_group;

    private boolean isKeyIn = true; // 직접 입력시 true, 현금 영수증 카드 이용시 false
    private String tranType = null; // DB에서 쓰는것
    /**
     * 카드를 긁은것인지 직접 현금영수증 번호를 입력한것인지 정확히 판단하기 위해서 사용
     * 카드데이터가 들어오면 true가되고 EditText에 포커스가오면 false가 된다.
     */
    private boolean isOnCardLeading = false;
    private String cardData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_pay);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txt_cash_number_title = (TextView) findViewById(R.id.txt_cash_number_title);
        edit_cash_receipt_number = (EditText) findViewById(R.id.edit_cash_receipt_number);
        edit_input_val = (EditText) findViewById(R.id.edit_input_val);
        edit_service_val = (EditText) findViewById(R.id.edit_service_val);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok_tint = (Button) findViewById(R.id.btn_ok_tint);
        rbtn_defalut_receipt = (RadioButton) findViewById(R.id.rbtn_defalut_receipt);
        rbtn_cash_receipt = (RadioButton) findViewById(R.id.rbtn_cash_receipt);
        rbtn_income_deduction = (RadioButton) findViewById(R.id.rbtn_income_deduction);
        rbtn_expense_evidence = (RadioButton) findViewById(R.id.rbtn_expense_evidence);
        rbtn_voluntary_issue = (RadioButton) findViewById(R.id.rbtn_voluntary_issue);
        v_center_space = findViewById(R.id.v_center_space);
        v_underline1 = findViewById(R.id.v_underline1);
        v_underline2 = findViewById(R.id.v_underline2);
        v_underline3 = findViewById(R.id.v_underline3);
        v_underline4 = findViewById(R.id.v_underline4);
        v_underline5 = findViewById(R.id.v_underline5);
        ll_cash_receipt_group = (LinearLayout) findViewById(R.id.ll_cash_receipt_group);
        ll_bottom_space = (LinearLayout) findViewById(R.id.ll_bottom_space);
        ll_cash_number_group = (FrameLayout) findViewById(R.id.ll_cash_number_group);

        edit_cash_receipt_number.requestFocus();


        registerOnClickListener();
        registerTextWatcher();
        registerOnFocusChangeListener();
        vatValInclude();
        btn_ok_tint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast(GetText(R.id.edit_cash_receipt_number));
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.edit_cash_receipt_number:
                isOnCardLeading = false;
                break;
            case R.id.rbtn_cash_receipt:
                vatValInclude();
                edit_cash_receipt_number.setClickable(true);
                edit_cash_receipt_number.setHint("번호를 입력 해주세요.");
                v_underline1.setVisibility(View.INVISIBLE);
                v_underline2.setVisibility(View.INVISIBLE);
                v_underline3.setVisibility(View.INVISIBLE);
                v_underline4.setVisibility(View.INVISIBLE);
                v_underline5.setVisibility(View.VISIBLE);

                if(GetText(R.id.edit_input_val).length()>=1
                        &&(rbtn_income_deduction.isChecked() || rbtn_expense_evidence.isChecked() || rbtn_voluntary_issue.isChecked())){
                    btn_ok.setVisibility(View.VISIBLE);
                    btn_ok_tint.setVisibility(View.GONE);
                }
                else{
                    btn_ok.setVisibility(View.GONE);
                    btn_ok_tint.setVisibility(View.VISIBLE);
                }
                edit_cash_receipt_number.setClickable(true);
                ll_cash_receipt_group.setVisibility(View.VISIBLE);
                ll_bottom_space.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, (float) 0.1));
                txt_cash_number_title.setVisibility(View.VISIBLE);
                ll_cash_number_group.setVisibility(View.VISIBLE);
                v_center_space.setVisibility(View.VISIBLE);
                break;
            case R.id.rbtn_defalut_receipt:
                vatValInclude();
                edit_cash_receipt_number.setClickable(false);
                edit_cash_receipt_number.setHint("-");
                v_underline1.setVisibility(View.INVISIBLE);
                v_underline2.setVisibility(View.INVISIBLE);
                v_underline3.setVisibility(View.INVISIBLE);
                v_underline4.setVisibility(View.VISIBLE);
                v_underline5.setVisibility(View.INVISIBLE);
                if(GetText(R.id.edit_input_val).length()>=1
                        &&rbtn_defalut_receipt.isChecked()){
                    btn_ok.setVisibility(View.VISIBLE);
                    btn_ok_tint.setVisibility(View.GONE);
                }
                else{
                    btn_ok.setVisibility(View.GONE);
                    btn_ok_tint.setVisibility(View.VISIBLE);
                }
                ll_cash_receipt_group.setVisibility(View.GONE);
                ll_bottom_space.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, (float) 2.1));
                rbtn_income_deduction.setChecked(false);
                rbtn_income_deduction.setChecked(false);
                rbtn_expense_evidence.setChecked(false);
                edit_cash_receipt_number.setClickable(false);

                txt_cash_number_title.setVisibility(View.GONE);
                ll_cash_number_group.setVisibility(View.GONE);
                v_center_space.setVisibility(View.GONE);
                break;
            case R.id.rbtn_income_deduction:
                edit_cash_receipt_number.setClickable(true);
                edit_cash_receipt_number.setHint("번호를 입력 해주세요.");
                v_underline1.setVisibility(View.VISIBLE);
                v_underline2.setVisibility(View.INVISIBLE);
                v_underline3.setVisibility(View.INVISIBLE);
                if(GetText(R.id.edit_cash_receipt_number).length()>=1
                        && GetText(R.id.edit_input_val).length()>=1
                        &&(rbtn_income_deduction.isChecked() || rbtn_expense_evidence.isChecked() || rbtn_voluntary_issue.isChecked())){
                    btn_ok.setVisibility(View.VISIBLE);
                    btn_ok_tint.setVisibility(View.GONE);
                }
                else{
                    btn_ok.setVisibility(View.GONE);
                    btn_ok_tint.setVisibility(View.VISIBLE);
                }
                edit_cash_receipt_number.setClickable(true);
                break;
            case R.id.rbtn_expense_evidence:
                edit_cash_receipt_number.setClickable(true);
                edit_cash_receipt_number.setHint("번호를 입력 해주세요.");
                v_underline1.setVisibility(View.INVISIBLE);
                v_underline2.setVisibility(View.VISIBLE);
                v_underline3.setVisibility(View.INVISIBLE);
                if(GetText(R.id.edit_cash_receipt_number).length()>=1
                        && GetText(R.id.edit_input_val).length()>=1
                        &&(rbtn_income_deduction.isChecked() || rbtn_expense_evidence.isChecked() || rbtn_voluntary_issue.isChecked())){
                    btn_ok.setVisibility(View.VISIBLE);
                    btn_ok_tint.setVisibility(View.GONE);
                }
                else{
                    btn_ok.setVisibility(View.GONE);
                    btn_ok_tint.setVisibility(View.VISIBLE);
                }
                edit_cash_receipt_number.setClickable(true);
                break;
            case R.id.rbtn_voluntary_issue:
                v_underline1.setVisibility(View.INVISIBLE);
                v_underline2.setVisibility(View.INVISIBLE);
                v_underline3.setVisibility(View.VISIBLE);
                if(GetText(R.id.edit_cash_receipt_number).length()>=1
                        && GetText(R.id.edit_input_val).length()>=1
                        &&(rbtn_income_deduction.isChecked() || rbtn_expense_evidence.isChecked() || rbtn_voluntary_issue.isChecked())){
                    btn_ok.setVisibility(View.VISIBLE);
                    btn_ok_tint.setVisibility(View.GONE);
                }
                else{
                    btn_ok.setVisibility(View.GONE);
                    btn_ok_tint.setVisibility(View.VISIBLE);
                }
                edit_cash_receipt_number.setClickable(true);
                break;
            case R.id.btn_cash_receipt_number_clear:
                SetText(R.id.edit_cash_receipt_number, "");
                break;
            case R.id.btn_input_val_clear:
                SetText(R.id.edit_input_val, "");
                break;
            case R.id.btn_service_val_clear:
                SetText(R.id.edit_service_val, "");
                break;
            case R.id.btn_ok:
                if (rbtn_defalut_receipt.isChecked()){
                    tranType = PaymentHistory.TRANTYPE_CASH_DEFAULT;
                }
                else if (rbtn_income_deduction.isChecked()){ //소득공제
                    tranType = PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION;
                }
                else if (rbtn_expense_evidence.isChecked()){ //지출증빙
                    tranType = PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE;
                }
                else if (rbtn_voluntary_issue.isChecked()){ // 자진발급
                    tranType = PaymentHistory.TRANTYPE_CASH_VOLUNTARY;
                }
                if (rbtn_defalut_receipt.isChecked()){ //일반영수증 - 부가세없음, 결제내용을 DB에만 저장한다.
                    DAOU_Fake(tranType, null, GetText(R.id.edit_input_val), GetText(R.id.edit_service_val), null, null);
                }
                else {//현금 영수증인경우에는 결제 함수를 실행한다.
                    DAOU_Cash(tranType, GetText(R.id.edit_input_val), GetText(R.id.edit_service_val), tranType,
                            isKeyIn, cardData);
                }
                break;
        }
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (edit_cash_receipt_number.hasFocus()) {
            if (isOnCardLeading) {
                // MSR이 긁힌 경우에 텍스트가 변경되었을때는 isKeyIn을 false로 변경
                isKeyIn = false;
                LOG.d(TAG, "isKeyIn: " + isKeyIn);
            }
            else {
                // EditText에 포커스가 간다음에 텍스트가 변경되었을때는 isKeyIn을 true로 변경
                isKeyIn = true;
                if (isKeyIn) {
                    edit_cash_receipt_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
                }
                LOG.d(TAG, "isKeyIn: " + isKeyIn);
            }
            if(GetText(R.id.edit_cash_receipt_number).length()>=1
                    && GetText(R.id.edit_input_val).length()>=1
                    &&(rbtn_defalut_receipt.isChecked() || rbtn_income_deduction.isChecked() || rbtn_expense_evidence.isChecked() || rbtn_voluntary_issue.isChecked())){
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }
        }
        else if (edit_input_val.hasFocus()) {
            if (rbtn_defalut_receipt.isChecked()){
                if(GetText(R.id.edit_input_val).length()>=1){
                    btn_ok.setVisibility(View.VISIBLE);
                    btn_ok_tint.setVisibility(View.GONE);
                }
            }
            else if(rbtn_income_deduction.isChecked() || rbtn_expense_evidence.isChecked() || rbtn_voluntary_issue.isChecked()){
                if(GetText(R.id.edit_cash_receipt_number).length()>=1
                        && GetText(R.id.edit_input_val).length()>=1){
                    btn_ok.setVisibility(View.VISIBLE);
                    btn_ok_tint.setVisibility(View.GONE);
                }
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }

            String commaRemove = (GetText(R.id.edit_input_val)).replace(",", "");;
            if (!commaRemove.isEmpty()) {
                if (Double.valueOf(commaRemove) >= 500000000) {
                    StringBuilder sb = new StringBuilder(commaRemove);
                    sb.setCharAt(commaRemove.length() - 1, 'A');
                    SetText(R.id.edit_input_val, sb.toString().replace("A", ""));
                    showToast("500,000,000미만의 값만 입력가능합니다.");
                }
                else {
                    ResultPrice price = Util.calculatePrice(CashPayActivity.this, GetText(R.id.edit_input_val), GetText(R.id.edit_service_val), rbtn_cash_receipt.isChecked());

                    SetText(R.id.txt_total_val, Util.commaFormat(price.totalPrice));
                    SetText(R.id.txt_origin_val, Util.commaFormat(price.taxFreePrice));
                    SetText(R.id.txt_service_val, Util.commaFormat(price.servicePrice));
                    SetText(R.id.txt_vat_val, Util.commaFormat(price.taxPrice));

                    if (!s.toString().equals(Util.commaFormat(commaRemove))) {
                        SetText(R.id.edit_input_val, Util.commaFormat(commaRemove));
                        edit_input_val.setSelection(GetText(R.id.edit_input_val).length());
                    }
                }
            }

        }
        else if (edit_service_val.hasFocus()) {
            if(GetText(R.id.edit_cash_receipt_number).length()>=1
                    && GetText(R.id.edit_input_val).length()>=1){
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }

            String commaRemove = (GetText(R.id.edit_service_val)).replace(",", "");
            if (!commaRemove.isEmpty()) {
                if (Double.valueOf(commaRemove) >= 500000000) {
                    StringBuilder sb = new StringBuilder(commaRemove);
                    sb.setCharAt(commaRemove.length() - 1, 'A');
                    SetText(R.id.edit_service_val, sb.toString().replace("A", ""));
                    showToast("500,000,000미만의 값만 입력가능합니다.");
                }
                else {
                    ResultPrice price = Util.calculatePrice(CashPayActivity.this, GetText(R.id.edit_input_val), GetText(R.id.edit_service_val), rbtn_cash_receipt.isChecked());

                    SetText(R.id.txt_total_val, Util.commaFormat(price.totalPrice));
                    SetText(R.id.txt_origin_val, Util.commaFormat(price.taxFreePrice));
                    SetText(R.id.txt_service_val, Util.commaFormat(price.servicePrice));
                    SetText(R.id.txt_vat_val, Util.commaFormat(price.taxPrice));

                    if (!s.toString().equals(Util.commaFormat(commaRemove))) {
                        SetText(R.id.edit_service_val, Util.commaFormat(commaRemove));
                        edit_service_val.setSelection(GetText(R.id.edit_service_val).length());
                    }
                }
            }
        }
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            v.setBackgroundResource(R.drawable.blue_radius);
        }
        else {
            v.setBackgroundResource(R.drawable.gray_radius);
        }
        switch (v.getId()) {
            case R.id.edit_cash_receipt_number:
                if(hasFocus){//포커스를 얻을때
                    isOnCardLeading = false;
                    if(isKeyIn == true) {
                        SetText(R.id.edit_cash_receipt_number, "");
                    }
                }
                else{
                    if(isKeyIn == true) {
                        cardData = GetText(R.id.edit_cash_receipt_number);

                        StringBuilder sb = new StringBuilder();
                        for(int i=0; i<cardData.length(); i++) {
                            if(i>=3 && i<7 ) {
                                sb.append("*");
                            }
                            else {
                                sb.append(cardData.charAt(i));
                            }
                        }
                        SetText(R.id.edit_cash_receipt_number, sb.toString());
                        SetText(R.id.edit_cash_receipt_number, Util.cashNumMasking(GetText(R.id.edit_cash_receipt_number)));
                    }
                }
                break;
        }
    }

    @Override
    public void BT_CB(final int resultCode, String BTAdress) {
        super.BT_CB(resultCode, BTAdress);
    }


    @Override
    public void DAOU_Payment_CB(final ResultPayment resultPayment) {
        super.DAOU_Payment_CB(resultPayment);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (resultPayment.resultCode != 0) {
                    SetText(R.id.edit_cash_receipt_number, "");
                }
            }
        });
    }
    @Override
    public void DAOU_OnMSRRead() {
        super.DAOU_OnMSRRead();
    }
    @Override
    public void DAOU_OnCardData(final String maskedCarData) {
        super.DAOU_OnCardData(maskedCarData);
        runOnUiThread(new Runnable() {
            public void run() {
                isOnCardLeading = true;
                cardData = maskedCarData;
                edit_cash_receipt_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                SetText(R.id.edit_cash_receipt_number, "****-####-####-****");
                isKeyIn = false;
            }
        });
    }

    @Override
    public void DAOU_Error_CB(int code) {
        super.DAOU_Error_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.edit_cash_receipt_number, "");
            }
        });
    }
    @Override
    public void DAOU_CardError_CB(int code){
        super.DAOU_CardError_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.edit_cash_receipt_number, "");
            }
        });
    }
    @Override
    public void DAOU_Payment_Incomplete(int resultCode, String message) {
        super.DAOU_Payment_Incomplete(resultCode, message);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetText(R.id.edit_cash_receipt_number, "");
            }
        });
    }
    private void registerOnClickListener() {
        (findViewById(R.id.img_back)).setOnClickListener(this);
        (findViewById(R.id.edit_cash_receipt_number)).setOnClickListener(this);
        (findViewById(R.id.rbtn_cash_receipt)).setOnClickListener(this);
        (findViewById(R.id.rbtn_defalut_receipt)).setOnClickListener(this);
        (findViewById(R.id.rbtn_income_deduction)).setOnClickListener(this);
        (findViewById(R.id.rbtn_expense_evidence)).setOnClickListener(this);
        (findViewById(R.id.rbtn_voluntary_issue)).setOnClickListener(this);
        (findViewById(R.id.btn_cash_receipt_number_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_input_val_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_service_val_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }
    private void registerTextWatcher() {
        ((EditText)findViewById(R.id.edit_cash_receipt_number)).addTextChangedListener(this);
        ((EditText)findViewById(R.id.edit_input_val)).addTextChangedListener(this);
        ((EditText)findViewById(R.id.edit_service_val)).addTextChangedListener(this);
    }
    private void registerOnFocusChangeListener() {
        (findViewById(R.id.edit_cash_receipt_number)).setOnFocusChangeListener(this);
        (findViewById(R.id.edit_input_val)).setOnFocusChangeListener(this);
        (findViewById(R.id.edit_service_val)).setOnFocusChangeListener(this);
    }

    private void vatValInclude() {
        BizInfo bizInfo = Util.getBizInfo(this);
        if (rbtn_defalut_receipt.isChecked()) {
            SetText(R.id.txt_vat_val_include1, "부가세 없음");
            SetText(R.id.txt_vat_val_include2, "부가세 없음");
        }
        else if (bizInfo.vatValInclude.equals("0")) {
            SetText(R.id.txt_vat_val_include1, "부가세 포함 " + bizInfo.vatValPer + "%");
            SetText(R.id.txt_vat_val_include2, "부가세 포함 " + bizInfo.serviceValPer + "%");
        }
        else if (bizInfo.vatValInclude.equals("1")) {
            SetText(R.id.txt_vat_val_include1, "부가세 별도 " + bizInfo.vatValPer + "%");
            SetText(R.id.txt_vat_val_include2, "부가세 별도 " + bizInfo.serviceValPer + "%");
        }
    }

    /**
     * 안쓰는 메소드 들임
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void afterTextChanged(Editable s) {
    }
}

package kr.checkbill.cb2.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.checkbill.cbapi.ResultStartBusness;

import kr.checkbill.cb2.activity.base.DaouStartBizActivity;
import kr.checkbill.cb2.activity.listview.BizListAdapter;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.payment.CashPayActivity;
import kr.checkbill.cb2.activity.payment.CardPayActivity;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.util.Util;

public class BizListActivity extends DaouStartBizActivity
        implements View.OnClickListener, AdapterView.OnItemClickListener{
    private static final String TAG = "BizListActivity";
    private ListView lv_biz_list;
    private BizListAdapter bizListAdapter;
    private TextView txt_biz_empty_message;
    private String callReason;
    private DBHelper mDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biz_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lv_biz_list = (ListView) findViewById(R.id.lv_biz_list);
        txt_biz_empty_message = (TextView) findViewById(R.id.txt_biz_empty_message);

        registerOnClickListener();
        registerOnItemClickListener();

        bizListAdapter = new BizListAdapter();
        lv_biz_list.setAdapter(bizListAdapter);


        mDB = new DBHelper(this);
        BizInfo bizInfo = new BizInfo();

        callReason = getIntent().getStringExtra("CallReason");
        if (callReason.equals("MainActivityCard")
                || callReason.equals("MainActivityCash")){
            SetText(R.id.txt_title, "사업자 선택  ");
        }
        else if (callReason.equals("SettingActivity")) {

        }

        /*리턴받은 cursor를 기반으로 fetch메소드를 실행해서 쿼리한 값을 전달 받는다*/
        Cursor bizInfoCursor = mDB.getBizInfoCursor();
        while (bizInfoCursor.moveToNext()){
            bizInfo.fetch(bizInfoCursor);
            bizListAdapter.addItem(
                    bizInfo._id,
                    bizInfo.tid,
                    bizInfo.bizNum,
                    bizInfo.shopName,
                    bizInfo.shopCeo,
                    bizInfo.shopAddress,
                    bizInfo.shopTel,
                    bizInfo.shopId);
        }
        if (bizInfoCursor.getCount() == 1) {
            if (callReason.equals("MainActivityCard")) {
                Intent intent = new Intent(BizListActivity.this, CardPayActivity.class);
                startActivity(intent);
                finish();
            }
            else if (callReason.equals("MainActivityCash")) {
                Intent intent = new Intent(BizListActivity.this, CashPayActivity.class);
                startActivity(intent);
                finish();
            }
        }

        /*등록된 사업자 정보가 없으면 메세지출력*/
        if ((bizListAdapter.getCount()+"").equals("0")){
        }
        else {
            txt_biz_empty_message.setVisibility(View.GONE);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        /*BizInfo 객체생성 및 클릭된 곳의 아이템들을 저장*/
        final BizInfo bizInfo = new BizInfo();
        bizInfo._id = bizListAdapter.getItem(position)._id;//테이블의 기본키로 사용하기때문에 실제로 UI에 보여줄필요는없다.
        bizInfo.tid = bizListAdapter.getItem(position).tid;
        bizInfo.bizNum = bizListAdapter.getItem(position).bizNum;
        bizInfo.shopName = bizListAdapter.getItem(position).shopName;
        bizInfo.shopCeo = bizListAdapter.getItem(position).shopCeo;
        bizInfo.shopAddress = bizListAdapter.getItem(position).shopAddress;
        bizInfo.shopTel = bizListAdapter.getItem(position).shopTel;
        bizInfo.shopId = bizListAdapter.getItem(position).shopId;

        if (callReason.equals("MainActivityCard")
                || callReason.equals("MainActivityCash")){ // 결제인 경우
            if (BT_IsConnect()){
                if (bizInfo.tid.equals(mDB.getSystemUseTid())) { // 선택한 사업자와 개시거래된 사업자가 같은경우
                    if (callReason.equals("MainActivityCard")) {
                        Intent intent = new Intent(BizListActivity.this, CardPayActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else if (callReason.equals("MainActivityCash")) {
                        Intent intent = new Intent(BizListActivity.this, CashPayActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
                else { // 선택한 사업자와 개시거래된 사업자가 다른경우 : 개시거래 실시
                    DAOU_StartBusness(Util.getBizInfo(BizListActivity.this ,bizInfo.tid).ipAdress, Util.getBizInfo(BizListActivity.this ,bizInfo.tid).portNumber, bizInfo.bizNum,
                            bizInfo.tid, bizInfo.shopId, false);
                }
            } else {
                showAlertDialog("알림", new String[]{"앱을 리더기와 연결을 한 후에 이용해주세요."}, true,
                        true, false, false, "확인", null, null, null, null, null);
            }


        }
        else if (callReason.equals("SettingActivity")) {
            View.OnClickListener posiveEvent = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mDB.deleteBizInfo(bizInfo._id).equals("success")){
                        mDB.setSystemMainTid(1, bizInfo.tid);
                        finish();
                        showToast("사업자정보가 삭제되었습니다.");
                    }
                    else{
                        showToast("사업자정보가 삭제에 실패하였습니다.");
                    }
                }
            };
            View.OnClickListener negativeEvent = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BizListActivity.this, BizSettingActivity.class);
                    intent.putExtra("tid", bizInfo.tid);
                    startActivity(intent);
                }
            };
            View.OnClickListener neutralEvent = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bizInfo.tid.equals(mDB.getSystemUseTid())) { // 선택한 사업자와 개시거래된 사업자가 같은경우
                        showToast("사업자가 " + bizInfo.shopName + "으(로) 변경되었습니다.");
                    }
                    else { // 선택한 사업자와 개시거래된 사업자가 다른경우 : 개시거래 실시
                        DAOU_StartBusness(Util.getBizInfo(BizListActivity.this ,bizInfo.tid).ipAdress, Util.getBizInfo(BizListActivity.this ,bizInfo.tid).portNumber, bizInfo.bizNum,
                                bizInfo.tid, bizInfo.shopId, false);
                        showToast("사업자가 " + bizInfo.shopName + "으(로) 변경되었습니다.");
                    }
                }
            };
            showAlertDialog("사업자 정보", new String[]{bizInfo.tid, bizInfo.bizNum, bizInfo.shopName, bizInfo.shopCeo, bizInfo.shopAddress, bizInfo.shopTel, bizInfo.shopId}, true,
                    true, true, true, "사업자삭제", "사업자설정", "사업자변경", posiveEvent, negativeEvent, neutralEvent);
        }
    }

    @Override
    public void DAOU_StartBusness_CB(final ResultStartBusness resultStartBusness) {
        super.DAOU_StartBusness_CB(resultStartBusness);
    }

    private void registerOnClickListener() {
        (findViewById(R.id.img_back)).setOnClickListener(this);
    }
    private void registerOnItemClickListener() {
        lv_biz_list.setOnItemClickListener(this);
    }
}

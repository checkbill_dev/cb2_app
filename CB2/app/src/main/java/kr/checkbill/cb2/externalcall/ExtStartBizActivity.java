package kr.checkbill.cb2.externalcall;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import com.checkbill.cbapi.CBApi;
import com.checkbill.cbapi.ResultStartBusness;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.base.DaouActivity;
import kr.checkbill.cb2.util.LOG;

public class ExtStartBizActivity extends DaouActivity
        implements View.OnClickListener{
    private static final String TAG = "ExtTerminalStartActivity";

    private String bizNum;
    private String tid;
    private String ipAddress;
    private String portNumber;
    private String id;
    private String password;
    private String packageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ext_start_biz);


        bizNum = getIntent().getStringExtra("bizNum");
        tid = getIntent().getStringExtra("tid");
        ipAddress = getIntent().getStringExtra("ipAddress");
        portNumber = getIntent().getStringExtra("portNumber");
        id = getIntent().getStringExtra("id");
        password = getIntent().getStringExtra("password");
        packageName = getIntent().getStringExtra("packageName");

        LOG.d(TAG, "bizNum : " + bizNum);
        LOG.d(TAG, "tid : " + tid);
        LOG.d(TAG, "ipAddress : " + ipAddress);
        LOG.d(TAG, "portNumber : " + portNumber);
        LOG.d(TAG, "id : " + id);
        LOG.d(TAG, "password : " + password);
        LOG.d(TAG, "packageName : " + packageName);

        registerOnClickListener();

        SetText(R.id.txt_biz_num, bizNum);
        SetText(R.id.txt_tid, tid);
        SetText(R.id.txt_ip_address, ipAddress);
        SetText(R.id.txt_port_number, portNumber);
        SetText(R.id.txt_sw_auth_num, DAOU_GetSWNum());
    }
    private void registerOnClickListener() {
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                DAOU_StartBusness(ipAddress, portNumber, getHwNum(), bizNum,
                        tid, getVanSerial(), null);
                break;
        }
    }

    @Override
    public void BT_CB(final int resultCode, String BTAdress) {
        super.BT_CB(resultCode, BTAdress);
        runOnUiThread(new Runnable() {
            public void run() {
                switch (resultCode) {
                    case CBApi.BT_DISCONNECTED:
                        SetText(R.id.txt_hw_auth_num, "");
                        break;
                    case CBApi.BT_CANCELED:
                        Intent intent = new Intent();
                        intent.putExtra("message", "블루투스가 취소 되었습니다.");
                        setResult(RESULT_CANCELED, intent);
                        finish();
                        break;
                }
            }
        });
    }
    @Override
    public void DAOU_GetHWNum_CB(String hwNum) {
        super.DAOU_GetHWNum_CB(hwNum);
        if(hwNum != null) {
            SetText(R.id.txt_hw_auth_num, hwNum);
        }
    }
    @Override
    public void DAOU_StartBusness_CB(final ResultStartBusness resultStartBusness) {
        super.DAOU_StartBusness_CB(resultStartBusness);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (resultStartBusness.resultCode == 0) {
                    Intent intent = new Intent();
                    intent.putExtra("resultCode", resultStartBusness.resultCode + ""); // resultCode
                    intent.putExtra("tid", GetText(R.id.txt_tid) + ""); // 단말기번호
                    intent.putExtra("date", resultStartBusness.date + ""); // 개시거날일 yyyyMMddHHmmss
                    intent.putExtra("shopName", resultStartBusness.name + ""); // 가맹점명
                    intent.putExtra("shopTel", resultStartBusness.tel + ""); // 가맹점 전화번호
                    intent.putExtra("shopCeo", resultStartBusness.ceo + ""); // 가맹점대표자명
                    intent.putExtra("shopAddress", resultStartBusness.addr + ""); // 가맹점주소
                    intent.putExtra("agencyName", resultStartBusness.agencyName + ""); // 관리대리점
                    intent.putExtra("agencyTel", resultStartBusness.agencyTel + ""); // 관리대리점 전화번호
                    intent.putExtra("expireDate", resultStartBusness.expireDate + ""); // 암호키만료일
                    intent.putExtra("screenMessage", resultStartBusness.screenMessage); // 스크린 메세지

                    LOG.d(TAG, "resultCode: " + resultStartBusness.resultCode + "");
                    LOG.d(TAG, "Tid: " + GetText(R.id.txt_tid));
                    LOG.d(TAG, "Date: " + resultStartBusness.date); // yyyyMMddHHmmSS
                    LOG.d(TAG, "ShopName: : " + resultStartBusness.name);
                    LOG.d(TAG, "ShopTel: " + resultStartBusness.tel);
                    LOG.d(TAG, "ShopCeo: " + resultStartBusness.ceo);
                    LOG.d(TAG, "ShopAddress: " + resultStartBusness.addr);
                    LOG.d(TAG, "AgencyName: " + resultStartBusness.agencyName);
                    LOG.d(TAG, "AgencyTel: " + resultStartBusness.agencyTel);
                    LOG.d(TAG, "ExpireDate: " + resultStartBusness.expireDate);
                    LOG.d(TAG, "ScreenMessage: " + resultStartBusness.screenMessage);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                else if (resultStartBusness.resultCode > 0){
                    Intent intent = new Intent();
                    intent.putExtra("resultCode", resultStartBusness.resultCode + ""); // resultCode
                    intent.putExtra("screenMessage", resultStartBusness.screenMessage); // 스크린 메세지

                    LOG.d(TAG, "resultCode: " + resultStartBusness.resultCode + "");
                    LOG.d(TAG, "ScreenMessage: " + resultStartBusness.screenMessage);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }


    @Override
    public void DAOU_Error_CB(final int code) {
        super.DAOU_Error_CB(code);
    }
    @Override
    public void DAOU_CardError_CB(int code){
        super.DAOU_CardError_CB(code);
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();
        final boolean packageBT = !this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        if (packageBT) {
            showToast("BLE를 지원하지 않습니다.");
            return;
        }

        if (BT_IsConnect() == false) {
            LOG.d(TAG, "블루투스 연결요청");
            BT_Connect(null);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(BT_IsConnect() == true) {
            BT_Disconnect();
        }
    }
}

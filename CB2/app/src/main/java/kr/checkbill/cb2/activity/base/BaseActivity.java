package kr.checkbill.cb2.activity.base;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.server.GetOrders;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.SingleTon;
import kr.checkbill.cb2.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 프로젝트에서 공통으로 처리하는 부분을 정의합니다.
 */
public class BaseActivity extends AppCompatActivity implements Callback<Object>{
    private static final String TAG = "BaseActivity";
    private ProgressDialog mLoadingDialog;
    private Toast mToast;
    private AlertDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDialog(this);
    }

    public void initDialog(Context context) {
        mLoadingDialog = new ProgressDialog(context);
        mLoadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mLoadingDialog.setCancelable(false);
    }

    public void showDialog(String msg) {
        if(mLoadingDialog == null) {
            return;
        }

        if(mLoadingDialog.isShowing() == true) {
            mLoadingDialog.dismiss();
        }
        mLoadingDialog.setMessage(msg);
        mLoadingDialog.show();
    }

    public void dismissDialog() {
        if(mLoadingDialog == null) {
            return;
        }

        mLoadingDialog.dismiss();
    }
    public void SetText(@IdRes int id, CharSequence text) {
        ((TextView) findViewById(id)).setText(text);
    }
    public void SetText(LinearLayout linearLayout, @IdRes int id, CharSequence text) {
        ((TextView) linearLayout.findViewById(id)).setText(text);
    }
    public String GetText(@IdRes int id) {
        return ((TextView) findViewById(id)).getText().toString();
    }
    public String GetText(LinearLayout linearLayout, @IdRes int id) {
        return ((TextView) linearLayout.findViewById(id)).getText().toString();
    }

    @Override
    public void onResponse(Call<Object> call, Response<Object> response) {
    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {

    }
    public void onPassword(String password){

    }

    public void showToast(String msg) {

        if(mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }
    public void showAlertDialog(String title, final String[] message, boolean isCancelable,
                                boolean isPositiveButton, boolean isNegativeButton, boolean isNeutralButton,
                                String positiveName, String negativeName, String neutralName,
                                View.OnClickListener positiveEvent, View.OnClickListener negativeEvent, View.OnClickListener neutralEvent) {
        if (dialog != null) {
            dialog.dismiss();
        }
        final LinearLayout linear_dialog = (LinearLayout) View.inflate(this, R.layout.dialog_notice,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(linear_dialog);

        Button btn_positive = (Button) linear_dialog.findViewById(R.id.btn_positive);
        Button btn_negative = (Button) linear_dialog.findViewById(R.id.btn_negative);
        Button btn_neutral = (Button) linear_dialog.findViewById(R.id.btn_neutral);

        builder.setCancelable(isCancelable);
        SetText(linear_dialog, R.id.txt_title, title);

        LOG.d(TAG, "message.length: " + message.length);

        if (message.length == 1) { // 일반 다이얼로그
            (linear_dialog.findViewById(R.id.ll_message)).setVisibility(View.VISIBLE);
            SetText(linear_dialog, R.id.txt_message, message[0]);
        }
        else if (message.length > 1) { // 사업자 정보 표시
            (linear_dialog.findViewById(R.id.ll_biz_info)).setVisibility(View.VISIBLE);
            SetText(linear_dialog, R.id.txt_tid, message[0]);
            SetText(linear_dialog, R.id.txt_biz_num, message[1]);
            SetText(linear_dialog, R.id.txt_shop_name, message[2]);
            SetText(linear_dialog, R.id.txt_shop_ceo, message[3]);
            SetText(linear_dialog, R.id.txt_shop_address, message[4]);
            SetText(linear_dialog, R.id.txt_shop_tel, message[5]);
            SetText(linear_dialog, R.id.txt_shop_id, message[6]);
        }

        if (isPositiveButton) {
            btn_positive.setVisibility(View.VISIBLE);
            SetText(linear_dialog, R.id.btn_positive, positiveName);
            if (positiveEvent == null) {
                btn_positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
            else {
                btn_positive.setOnClickListener(positiveEvent);
            }
        }
        if (isNegativeButton) {
            btn_negative.setVisibility(View.VISIBLE);
            SetText(linear_dialog, R.id.btn_negative, negativeName);
            if (negativeEvent == null) {
                btn_negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
            else {
                btn_negative.setOnClickListener(negativeEvent);
            }
        }
        if (isNeutralButton) {
            btn_neutral.setVisibility(View.VISIBLE);
            SetText(linear_dialog, R.id.btn_neutral, neutralName);
            if (neutralEvent == null) {
                btn_neutral.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
            else {
                btn_neutral.setOnClickListener(neutralEvent);
            }
        }

        dialog = builder.create();
        dialog.show();
    }

    public String getHwNum() {
        return SingleTon.getInstance().getHWNum();
    }

    public String getVanSerial() {
        return SingleTon.getInstance().getVanSerial();
    }
}

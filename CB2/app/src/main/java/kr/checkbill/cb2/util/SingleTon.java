package kr.checkbill.cb2.util;

/**
 * Created by kwonhyeokjin on 2017. 1. 20..
 */

public class SingleTon{
    private static SingleTon uniqueInstance;
    private static String HWNum;
    private static String VanSerial;
    private SingleTon(){}

    public static SingleTon getInstance(){
        if (uniqueInstance == null){
            uniqueInstance = new SingleTon();
        }
        return uniqueInstance;
    }
    /*하드웨어 여신인증번호*/
    public void setHWNum(String HWNum){
        uniqueInstance.HWNum = HWNum;

    }
    public String getHWNum(){
        return uniqueInstance.HWNum;
    }
    /*생산일련번호(밴시리얼)*/
    public void setVanSerial(String VanSerial){
        uniqueInstance.VanSerial = VanSerial;
    }
    public String getVanSerial(){
        return uniqueInstance.VanSerial;
    }

}

package kr.checkbill.cb2.util;

import android.content.Context;
import android.database.Cursor;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.BizInfo;

/**
 * Created by Administrator on 2017-03-29.
 */

public class Util {
    private static final String TAG = "Util";
    /**
     * 현재 사용중인 BizInfo 반환
     * @param context
     * @return
     */
    public static BizInfo getBizInfo(Context context) {
        BizInfo bizInfo = new BizInfo();
        DBHelper db  = new DBHelper(context);
        Cursor bizInfoCorsor = db.getBizInfoUseCursorOneRecord();

        while (bizInfoCorsor.moveToNext()){
            bizInfo.fetch(bizInfoCorsor);
        }

        return bizInfo;
    }

    /**
     * tid에 해당하는 BizInfo 반환
     * @param context
     * @param tid
     * @return
     */
    public static BizInfo getBizInfo(Context context, String tid) {
        BizInfo bizInfo = new BizInfo();
        DBHelper db  = new DBHelper(context);
        Cursor bizInfoCorsor = db.getBizInfoUseCursorOneRecordForTid(tid);

        while (bizInfoCorsor.moveToNext()){
            bizInfo.fetch(bizInfoCorsor);
        }

        return bizInfo;
    }

    /**
     * 결제 금액 계산 함수
     * @param context        Context
     * @param normal_price   입력 금액
     * @param service_price  입력 봉사료
     * @param isVan          밴사 사용유무 true:밴사 사용 결제  false:일반영수증
     * @return ResultPrice
     */
    public static ResultPrice calculatePrice(Context context, String normal_price, String service_price, boolean isVan) {


        ResultPrice price = new ResultPrice();

        BizInfo bizInfo = getBizInfo(context);

        // 1. 부가세 여부
        int vatValInclude = 0;  //부가세 여부

        if(isVan == true) {
            if (bizInfo.vatValInclude.equals("0")) {
                vatValInclude = 0;
            } else if (bizInfo.vatValInclude.equals("1")) {
                vatValInclude = 1;
            }
        }
        else { // 일반영수증 일때 비과세
            vatValInclude = -1;
        }

        // 2. 실제 계산할 때  세금 부가세율
        double inputValPer;    //입력금액에 대한 세금 부가세율
        double serviceValPer;  //봉사료에 대한 세금 부게세율
        inputValPer = (Double.parseDouble(bizInfo.vatValPer) * 0.01) + 1.0;
        serviceValPer = (Double.parseDouble(bizInfo.serviceValPer) * 0.01) + 1.0;

//     * @param totalPrice   결제금액
//     * @param taxPrice     부가세
//     * @param servicePrice 봉사료
//     * @param taxFreePrice 물품가격

        long inputVal;         //입력된 금액
        long inputServiceVal; //입력된 봉사료

        long totalPrice;      //결제금액
        long inputOriginVal;  //제품단가
        long inputVatVal;     //부가세
        long servicePrice;

        long resultTotalVal = 0;         // 최종 총 결제금액
        long resultOriginVal = 0;        // 최종 물품가액 (입력 금액에서 세금제외)
        long resultServiceOriginVal = 0; // 최종 봉사료 (입력 부가세에서 세금 제외)
        long resultVatVal = 0;           // 최종 부가세(입력금액 부가세 + 봉사료 부가세)


        // 계산용 버퍼
        long inputServiceOriginVal;
        long inputServiceVatVal;

        LOG.d(TAG, "vatValInclude: " + vatValInclude);

        /**
         * 부가세 포함일시
         * 0: 부가세포함
         * 1: 부가세별도
         * -1: 비과세
         */
        if (vatValInclude == 0) {
            inputVal = removeCommaFormat(normal_price);
            inputServiceVal = removeCommaFormat(service_price);

            inputOriginVal = (int)Math.ceil(inputVal / inputValPer);
            inputServiceOriginVal = (int)Math.ceil(inputServiceVal / serviceValPer);

            inputVatVal = inputVal - inputOriginVal;
            inputServiceVatVal = inputServiceVal - inputServiceOriginVal;

            resultTotalVal = inputVal + inputServiceVal;
            resultOriginVal = inputOriginVal;
            resultServiceOriginVal = inputServiceOriginVal;
            resultVatVal = inputVatVal + inputServiceVatVal;
        }
        else if (vatValInclude == 1) {
            inputOriginVal = removeCommaFormat(normal_price);
            inputServiceOriginVal = removeCommaFormat(service_price);

            inputVal = (int)Math.ceil(inputOriginVal * inputValPer);
            inputServiceVal = (int)Math.ceil(inputServiceOriginVal * serviceValPer);

            inputVatVal = inputVal - inputOriginVal;
            inputServiceVatVal = inputServiceVal - inputServiceOriginVal;

            resultTotalVal = inputVal + inputServiceVal;
            resultOriginVal = inputOriginVal;
            resultVatVal = inputVatVal + inputServiceVatVal;
            resultServiceOriginVal = inputServiceOriginVal;
        }
        else {
            inputVal = removeCommaFormat(normal_price);
            inputServiceVal = removeCommaFormat(service_price);

            resultTotalVal = inputVal + inputServiceVal;
            resultOriginVal = inputVal;
            resultServiceOriginVal = inputServiceVal;
            resultVatVal = 0;
        }

        price.totalPrice = resultTotalVal;
        price.taxFreePrice = resultOriginVal;
        price.servicePrice = resultServiceOriginVal;
        price.taxPrice = resultVatVal;

        return price;
    }


    /**
     * String to long
     * @param value String value
     * @param def   Default value
     * @return long
     */
    public static long parseLong(String value, long def) {
        long ret;
        try {
            ret = Integer.parseInt(value);
        }
        catch(NumberFormatException e) {
            ret = def;
        }
        return ret;
    }

    public static String commaFormat(long a) {
        NumberFormat nf = NumberFormat.getInstance();
        return nf.format(a);
    }

    public static String commaFormat(String a) {
        String temp = "";
        if(a == null || a.isEmpty()) {

        }
        else {
            long b;
            b = parseLong(a, 0);
            temp = commaFormat(b);
        }
        return temp;
    }

    public static long removeCommaFormat(String a) {
        // 1. 쉽표를 제거한다.
        String commaRemove; //입력값 String buffer
        if (a == null || a.equals("")){
            commaRemove = "0";
        }
        else {
            commaRemove = (a.replace(",", ""));
        }
        return parseLong(commaRemove, 0);
    }

    public static String getDateString(String s) {
        Date temp;
        String temp2 = "";

        try {

            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            temp = format.parse(s);
            temp2 = DBHelper.mSdf.format(temp);
        } catch (ParseException e) {
            temp2 = s;
        }
        return temp2;
    }

    /**
     * 임의의 숫자로 된 스트링을 생성함
     * @param length 스트링의 길이
     * @return 랜덤스트링
     */
    public static String getRandomString(int length) {
        StringBuffer buffer = new StringBuffer();
        Random random = new Random();
        int randomValue;
        char[] pbData = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        for(int i=0; i<length; i++)
        {
            randomValue = random.nextInt(pbData.length);
            buffer.append(pbData[randomValue]);
        }
        return buffer.toString();
    }

    public static void clearString(String cardData) {

        if(cardData == null || cardData.isEmpty()) {
            return;
        }

        int length = cardData.length();

        cardData = getRandomString(length);
        LOG.d(TAG, "결제후 카드데이터 maked card number step1: " + cardData);

        StringBuilder sb;
        sb = new StringBuilder(cardData);
        for (int i = 0; i < length; i++) {
            sb.setCharAt(i, 'F');
        }
        cardData = sb.toString();
        LOG.d(TAG, "결제후 카드데이터 maked card number step2: " + cardData);

        for (int i = 0; i < length; i++) {
            sb.setCharAt(i, '*');
        }
        cardData = sb.toString();
        LOG.d(TAG, "결제후 카드데이터 maked card number step3: " + cardData);


    }
    public static ResultPrice minusFormat(boolean isApproval, ResultPrice price) {
        if (!isApproval) {
            price.totalPrice = -price.totalPrice;
            price.taxFreePrice = price.taxFreePrice == 0 ? price.taxFreePrice : -price.taxFreePrice;
            price.taxPrice = price.taxPrice == 0 ? price.taxPrice : -price.taxPrice;
            price.servicePrice = price.servicePrice == 0 ? price.servicePrice : -price.servicePrice;
        }
        return price;
    }
    public static String cashNumMasking(String cashNum) {
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<cashNum.length(); i++) {
            if(i>=3 && i<7 ) {
                sb.append("*");
            }
            else {
                sb.append(cashNum.charAt(i));
            }
        }
        return sb.toString();
    }

}

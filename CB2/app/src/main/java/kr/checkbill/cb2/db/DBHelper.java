package kr.checkbill.cb2.db;

import android.content.ContentValues;
import android.content.Context;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.checkbill.cbdaouapi.DaouIntegrityCheck;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.db.table.SystemSetting;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.Util;

/**
 * Created by kwonhyeokjin on 2017. 1. 10..
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = "DBHelper";
    private static final String DB_NAME = "checkbill2.db.sqlite";
    private static final int DB_VERSION = 1;
    private static final String INIT_DB_FILE_NAME = "db/initDb.sql"; // assets 파일 밑에 저장되 SQL 초기화파일 경로

    public static SimpleDateFormat mSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 날짜 비교에 사용
    public static SimpleDateFormat mStartSdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
    public static SimpleDateFormat mEndSdf = new SimpleDateFormat("yyyy-MM-dd 23:59:59");

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public String initDB(Context context){
        SQLiteDatabase readDB = getReadableDatabase();
        String query = "SELECT * FROM sqlite_master WHERE type='table'";
        Cursor c = readDB.rawQuery(query, null);
        int tableCount = c.getCount();
        if(tableCount == 5){ // 테이블이 2개일때는 4, 테이블이 3개일때는 5 리턴, 4개일때는 6리턴
            c.close();
            return "success";
        }
        LOG.i(TAG, "데이터베이스가 없습니다. 새로 만듭니다.");
        LOG.i(TAG, String.valueOf(tableCount));
        return createDB(context);
    }

    /*실제로 테이블을 만드는 메소드*/
    private String createDB(Context context){
        /* 1. initDB.sql 파일을 얻어오는 과정*/
        SQLiteDatabase wDB = getWritableDatabase();
        InputStream is = null;

        try {
            is = context.getAssets().open(INIT_DB_FILE_NAME);
        } catch (IOException e) {
            LOG.e(TAG, "initDB.sql 파일이 없습니다.");
            e.printStackTrace();
            wDB.close();
            return "initDB.sql 파일이 없습니다.";
        }

        /*2. InputStreamReader을 생성하는 과정 */
        StringBuilder stringBuilder = new StringBuilder();
        InputStreamReader isReader = null;
        try {
            isReader = new InputStreamReader(is,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOG.e(TAG, "InputStreamReader 생성실패");
            e.printStackTrace();
            try {
                is.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            wDB.close();
            return "DB초기화중 InputStreamReader생성실패";
        }
        /*3. 실제로 파일을 한줄씩 읽어내는 과정, 읽어낸 텍스트는 stringBuilder 에 저장됨*/
        BufferedReader bfRead = new BufferedReader(isReader);
        String line = null;

        try {
            while ((line = bfRead.readLine()) != null){
                line = line.trim(); // 앞뒤공백제거
                if (line.length() >= 2 && line.charAt(0) == '-' && line.charAt(1) == '-') {
                    // line이 2이상이고, line의 맨앞글자가 '-'이고, 두번째글자가 '-'이면 read하지 않는다.(아마 sql파일에서 주석처리때문에 사용한듯)
                    continue;
                }

                stringBuilder.append(line);
                stringBuilder.append(" ");
            }
        } catch (IOException e) {
            LOG.e(TAG, "초기화 DB파일 해석 실패");
            e.printStackTrace();
            try {
                is.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            wDB.close();
            return "초기화 DB파일에 해석에 실패하였습니다.";
        }
        /*4. 실제로 테이블을 생성하는과정, 트랜잭션을 사용하는이유 : 훨씬빠르다.*/
        String sqlScript = stringBuilder.toString();
        String sqlList[] = sqlScript.split(";"); // ;를 기준으로 스플릿
        wDB.beginTransaction(); // 데이터베이스의 트랜잭션을 시작
        try{
            for (String sql : sqlList) {
                sql = sql.trim();
                if (sql.length() > 0) {
                    wDB.execSQL(sql);
                }
            }
        }catch(Exception e){
            LOG.e(TAG,"initDb.sql 실행 오류");
            e.printStackTrace();
            wDB.endTransaction(); //트랜잭션 종료
            wDB.close();
            return "데이터베이스 초기화에 실패하였습니다.";
        }

        wDB.setTransactionSuccessful();
        wDB.endTransaction();
        wDB.close();
        return "success";
    }
    /*저장된 비밀번호를 반환하는 메소드*/
    public String getSystemPassword(){
        SQLiteDatabase rDB = getReadableDatabase();
        String query = "SELECT PASSWORD FROM SYSTEM_SETTING";
        Cursor c = rDB.rawQuery(query, null);
        c.moveToNext();
        String str = c.getString(0);
        c.close();
        rDB.close();
        return str;
    }
    /* 비밀번호를 변경하는 메소드*/
    public String setSystemPassword(String newPassword){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.PASSWORD, newPassword);
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"비밀번호변경성공");
            wDB.close();
            return "success";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"비밀번호 변경 오류");
            wDB.close();
            return "비밀번호 변경 오류";
        }
    }
    /* 소프트웨어 여신 인증번호를 변경하는 메소드*/
    public String setSystemSwAuthNum(String SwAuthNum){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.SW_AUTH_NUM, SwAuthNum);
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"");
            wDB.close();
            return "소프트웨어 여신 인증번호 삽입성공";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"소프트웨어 여신 인증번호 삽입 오류");
            wDB.close();
            return "소프트웨어 여신 인증번호 삽입 오류";
        }
    }
    /* 블루투스 연결유지 여부를 ON으로 변경*/
    public String setSystemBtMaintainOn(){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.BT_MAINTAIN, "1");
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"");
            wDB.close();
            return "블루투스 연결유지 여부를 ON으로 변경성공";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"블루투스 연결유지 여부를 ON으로 변경 오류");
            wDB.close();
            return "블루투스 연결유지 여부를 ON으로 변경 오류";
        }
    }
    /* 블루투스 연결유지 여부를 OFF으로 변경*/
    public String setSystemBtMaintainOff(){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.BT_MAINTAIN, "0");
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"");
            wDB.close();
            return "블루투스 연결유지 여부를 OFF으로 변경성공";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"블루투스 연결유지 여부를 OFF으로 변경 오류");
            wDB.close();
            return "블루투스 연결유지 여부를 OFF으로 변경 오류";
        }
    }

    /*블루투스 연결 여부 반환*/
    public String getSystemBtMaintain(){
        SQLiteDatabase rDB = getReadableDatabase();
        String query = "SELECT BT_MAINTAIN FROM SYSTEM_SETTING";
        Cursor c = rDB.rawQuery(query, null);
        c.moveToNext();
        String str = c.getString(0);
        c.close();
        rDB.close();
        return str;
    }

    /* 은련카드 사용여부를 ON으로 변경*/
    public String setSystemEullYeonOn(){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.EULLYEON_USE, "1");
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"");
            wDB.close();
            return "은련카드 사용여부를 ON으로 변경성공";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"은련카드 사용여부를 ON으로 변경 오류");
            wDB.close();
            return "은련카드 사용여부를 ON으로 변경 오류";
        }
    }

    /* 은련카드 사용여부를 OFF으로 변경*/
    public String setSystemEullYeonOff(){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.EULLYEON_USE, "0");
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"");
            wDB.close();
            return "은련카드 사용여부를 OFF으로 변경성공";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"은련카드 사용여부를 OFF으로 변경 오류");
            wDB.close();
            return "은련카드 사용여부를 OFF으로 변경 오류";
        }
    }

    /*은련카드 사용 여부 반환*/
    public String getSystemEullYeon(){
        SQLiteDatabase rDB = getReadableDatabase();
        String query = "SELECT EULLYEON_USE FROM SYSTEM_SETTING";
        Cursor c = rDB.rawQuery(query, null);
        c.moveToNext();
        String str = c.getString(0);
        c.close();
        rDB.close();
        return str;
    }

    ///
    /* 자동결제 사용여부를 ON으로 변경*/
    public String setSystemIcAutoPayOn(){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.IC_AUTO_PAY, "1");
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"");
            wDB.close();
            return "자동결제 사용여부를 ON으로 변경성공";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"자동결제 사용여부를 ON으로 변경 오류");
            wDB.close();
            return "자동결제 사용여부를 ON으로 변경 오류";
        }
    }

    /* 자동결제 사용여부를 OFF으로 변경*/
    public String setSystemIcAutoPayOff(){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.IC_AUTO_PAY, "0");
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"");
            wDB.close();
            return "자동결제 사용여부를 OFF으로 변경성공";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"자동결제 사용여부를 OFF으로 변경 오류");
            wDB.close();
            return "자동결제 사용여부를 OFF으로 변경 오류";
        }
    }

    /*자동결제 사용 여부 반환*/
    public String getSystemIcAutoPay(){
        SQLiteDatabase rDB = getReadableDatabase();
        String query = "SELECT IC_AUTO_PAY FROM SYSTEM_SETTING";
        Cursor c = rDB.rawQuery(query, null);
        c.moveToNext();
        String str = c.getString(0);
        c.close();
        rDB.close();
        return str;
    }

    /* 블루투스 자동 연결 여부를 ON으로 변경*/
    public String setSystemBtAutoConnectON(){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.BT_AUTO_CONNECT, "1");
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"");
            wDB.close();
            return "블루투스 자동 연결 여부를 OFF으로 변경성공";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"블루투스 자동 연결 여부를 OFF으로 변경 오류");
            wDB.close();
            return "블루투스 자동 연결 여부를 OFF으로 변경 오류";
        }
    }

    /* 블루투스 자동 연결 여부를 OFF으로 변경*/
    public String setSystemBtAutoConnectOFF(){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.BT_AUTO_CONNECT, "0");
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"");
            wDB.close();
            return "블루투스 자동 연결 여부를 OFF으로 변경성공";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"블루투스 자동 연결 여부를 OFF으로 변경 오류");
            wDB.close();
            return "블루투스 자동 연결 여부를 OFF으로 변경 오류";
        }
    }


    /*블루투스 자동 연결 여부를 반환*/
    public String getSystemBtAutoConnect(){
        SQLiteDatabase rDB = getReadableDatabase();
        String query = "SELECT BT_AUTO_CONNECT FROM SYSTEM_SETTING";
        Cursor c = rDB.rawQuery(query, null);
        c.moveToNext();
        String str = c.getString(0);
        c.close();
        rDB.close();
        return str;
    }

    /* 블루투스를 연결후 리더기의 블루투스 주소를 최근 블루투스 연결 주소로 저장*/
    public String setSystemRtcBtNum(String address){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.RCT_BT_NUM, address);
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"");
            wDB.close();
            return "블루투스 주소저장 성공";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"블루투스 주소저장 오류");
            wDB.close();
            return "블루투스 주소저장 오류";
        }
    }



    /*최근 연결되었던 리더기의 블루투스 리더기의 주소를 저장*/
    public String getSystemRtcBtNum(){
        SQLiteDatabase rDB = getReadableDatabase();
        String query = "SELECT RCT_BT_NUM FROM SYSTEM_SETTING";
        Cursor c = rDB.rawQuery(query, null);
        c.moveToNext();
        String str = c.getString(0);
        c.close();
        rDB.close();
        return str;
    }

    /*현재 사용중인 단말기번호를 반환하는 메소드*/
    public String getSystemUseTid(){ // 초기값이 0이다. 0 이라면 단말기개시거래가 시작 하지 않은것이다.
        SQLiteDatabase rDB = getReadableDatabase();
        String query = "SELECT USE_TID FROM SYSTEM_SETTING";
        Cursor c = rDB.rawQuery(query, null);
        c.moveToNext();
        String str = c.getString(0);
        c.close();
        rDB.close();
        return str;
    }
    /* 현재 사용중인 단말기번호를 변경하는 메소드*/
    public String setSystemUseTid(String tid){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(SystemSetting.USE_TID, tid);
        long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"단말기번호 변경 성공");
            wDB.close();
            return "success";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"단말기 번호 변경 오류");
            wDB.close();
            return "단말기 번호 변경 오류";
        }
    }
    /*주사업자 단말기번호를 반환하는 메소드*/
    public String getSystemMainTid(){ // 초기값이 0이다. 0 이라면 단말기개시거래를 하지 않은것이다.
        SQLiteDatabase rDB = getReadableDatabase();
        String query = "SELECT MAIN_TID FROM SYSTEM_SETTING";
        Cursor c = rDB.rawQuery(query, null);
        c.moveToNext();
        String str = c.getString(0);
        c.close();
        rDB.close();
        return str;
    }

    /**
     * 주사업자 단말기번호를 변경하는 메소드
     * BizInfo 테이블의 갯수가 0인 경우에만 실행된다.
     * 개시거래할때는 사업자정보를 삽입하기전에 실행되어야하고, 사업자를 삭제할때는 삭제한뒤에 실행되어야한다.
     * indertOrDelete 0이면 사업자삽입시에 실행되는것이고, 1이면 사업자삭제시에 실행되는것이다.
     * 사업자 삽입시에는 받은 단말기번호를 삽입한다, 사업자삭제시에는 현재 단말기 번호가 주사업자인지 추가로 검사하고, 사업자목록이 있다면 다음 레코드의단말기번호를 삽입하고, 사업자목록이없다면 0을 삽입한다.
     * @param tid 변경하려는 단말기번호
     */
    public void setSystemMainTid(int indertOrDelete, String tid){
        if (indertOrDelete == 0
                && getBizInfoCursor().getCount() == 0) {
            SQLiteDatabase wDB = getWritableDatabase();
            wDB.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put(SystemSetting.MAIN_TID, tid);
            long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
            if (updateCnt==1){
                wDB.setTransactionSuccessful();
                wDB.endTransaction();
                LOG.e(TAG,"주 사업자 단말기번호 변경 성공");
                wDB.close();
            }
            else{
                wDB.endTransaction();
                LOG.e(TAG,"주 사업자 단말기번호 변경 오류");
                wDB.close();
            }
        }
        else if (indertOrDelete == 1
                && getBizInfoCursor().getCount() != 0
                && getSystemMainTid().equals(tid)){
            SQLiteDatabase wDB = getWritableDatabase();
            wDB.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put(SystemSetting.MAIN_TID, getSystemBizInfoFirstTid());
            long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
            if (updateCnt==1){
                wDB.setTransactionSuccessful();
                wDB.endTransaction();
                LOG.e(TAG,"주 사업자 단말기번호 변경 성공");
                wDB.close();
            }
            else{
                wDB.endTransaction();
                LOG.e(TAG,"주 사업자 단말기번호 변경 오류");
                wDB.close();
            }
        }
        else if (indertOrDelete == 1
                && getBizInfoCursor().getCount() == 0
                && getSystemMainTid().equals(tid)){
            SQLiteDatabase wDB = getWritableDatabase();
            wDB.beginTransaction();
            ContentValues cv = new ContentValues();
            cv.put(SystemSetting.MAIN_TID, "0");
            long updateCnt = wDB.update(SystemSetting.TABLE_NAME, cv, null, null);
            if (updateCnt==1){
                wDB.setTransactionSuccessful();
                wDB.endTransaction();
                LOG.e(TAG,"주 사업자 단말기번호 변경 성공");
                wDB.close();
            }
            else{
                wDB.endTransaction();
                LOG.e(TAG,"주 사업자 단말기번호 변경 오류");
                wDB.close();
            }
        }
    }
    /* 서버로부터 해당 사업자의 거래내역을 동기화한 시간을 업데이트*/
    public String setBizInfoTs(String ts){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(BizInfo.TS, ts);
        long updateCnt = wDB.update(BizInfo.TABLE_NAME, cv, null, null);
        if (updateCnt==1){
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
            LOG.e(TAG,"타임스탬프 변경 성공");
            wDB.close();
            return "success";
        }
        else{
            wDB.endTransaction();
            LOG.e(TAG,"타임스탬프 변경 오류");
            wDB.close();
            return "타임스탬프 변경 오류";
        }
    }
    /*사업자 정보를 등록하는 메소드*/
    public long addBizInfo(String... values){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        long id;
        cv.put(BizInfo.TID, values[0]);
        cv.put(BizInfo.BIZ_NUM, values[1]);
        cv.put(BizInfo.SHOP_NAME, values[2]);
        cv.put(BizInfo.SHOP_CEO, values[3]);
        cv.put(BizInfo.SHOP_ADDRESS, values[4]);
        cv.put(BizInfo.SHOP_TEL, values[5]);
        cv.put(BizInfo.SHOP_ID, values[6]);
        cv.put(BizInfo.AGENCY_NAME, values[7]);
        cv.put(BizInfo.AGENCY_TEL, values[8]);
        cv.put(BizInfo.USER_NAME, values[9]);
        cv.put(BizInfo.VAT_VAL_PER, values[10]);
        cv.put(BizInfo.SERVICE_VAL_PER, values[11]);
        cv.put(BizInfo.VAT_VAL_INCLUDE, values[12]);
        cv.put(BizInfo.USER_MSG, values[13]);
        cv.put(BizInfo.SERVER_SYNC, values[14]);
        cv.put(BizInfo.POSCODE, values[15]);
        cv.put(BizInfo.PIN, values[16]);
        cv.put(BizInfo.TS, values[17]); // 사업자 정보가 삽입될 당시의 시간을 ts로 삽입
        cv.put(BizInfo.IP_ADDRESS, values[18]);
        cv.put(BizInfo.PORT_NUMBER, values[19]);
        try {
            id = wDB.insert(BizInfo.TABLE_NAME, null, cv);
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
        }
        catch (Exception e){
            wDB.endTransaction();
            LOG.e(TAG,"사업자정보등록실패");
            wDB.close();
            return 0;

        }
        LOG.i(TAG, "사업자정보등록성공");
        wDB.close();
        return id;
    }
    /*현재 사업자 리스트중에서 가장 위에있는 사업자의 단말기 번호를 반환 (사업자가 없다면 0을반환)*/
    public String getSystemBizInfoFirstTid(){
        SQLiteDatabase rDB = getReadableDatabase();
        String query = "SELECT TID FROM BIN_INFO";
        Cursor c = rDB.rawQuery(query, null);
        c.moveToNext();
        String str = c.getString(0);
        c.close();
        rDB.close();
        return str;
    }
    /*사업자 정보가 담긴 corsor를 리턴해주는 메소드*/
    public Cursor getBizInfoCursor(){
        SQLiteDatabase rDB = getReadableDatabase();
        Cursor cursor;
        cursor = rDB.query(BizInfo.TABLE_NAME, new String[]{
                        BizInfo._ID,
                        BizInfo.TID,
                        BizInfo.BIZ_NUM,
                        BizInfo.SHOP_NAME,
                        BizInfo.SHOP_CEO,
                        BizInfo.SHOP_ADDRESS,
                        BizInfo.SHOP_TEL,
                        BizInfo.SHOP_ID,
                        BizInfo.AGENCY_NAME,
                        BizInfo.AGENCY_TEL,
                        BizInfo.USER_NAME,
                        BizInfo.VAT_VAL_PER,
                        BizInfo.SERVICE_VAL_PER,
                        BizInfo.VAT_VAL_INCLUDE,
                        BizInfo.USER_MSG,
                        BizInfo.SERVER_SYNC,
                        BizInfo.POSCODE,
                        BizInfo.PIN,
                        BizInfo.TS,
                        BizInfo.IP_ADDRESS,
                        BizInfo.PORT_NUMBER},
                null, null, null, null, null);
//        rDB.close();
        return cursor;
    }

    /*현재 사용중인 사업자에 해당하는 사업자정보가 담긴 레코드를 리턴하는 레코드*/
    public Cursor getBizInfoUseCursorOneRecord(){
        String tid = getSystemUseTid();
        SQLiteDatabase rDB = getReadableDatabase();
        Cursor cursor;
        cursor = rDB.query(BizInfo.TABLE_NAME, new String[]{
                        BizInfo._ID,
                        BizInfo.TID,
                        BizInfo.BIZ_NUM,
                        BizInfo.SHOP_NAME,
                        BizInfo.SHOP_CEO,
                        BizInfo.SHOP_ADDRESS,
                        BizInfo.SHOP_TEL,
                        BizInfo.SHOP_ID,
                        BizInfo.AGENCY_NAME,
                        BizInfo.AGENCY_TEL,
                        BizInfo.USER_NAME,
                        BizInfo.VAT_VAL_PER,
                        BizInfo.SERVICE_VAL_PER,
                        BizInfo.VAT_VAL_INCLUDE,
                        BizInfo.USER_MSG,
                        BizInfo.SERVER_SYNC,
                        BizInfo.POSCODE,
                        BizInfo.PIN,
                        BizInfo.TS,
                        BizInfo.IP_ADDRESS,
                        BizInfo.PORT_NUMBER},
                "TID = ?",
                new String[] {tid}, null, null, null);
//        rDB.close();
        return cursor;
    }
    /*tid에 해당하는 사업자정보가 담긴 레코드를 리턴하는 레코드*/
    public Cursor getBizInfoUseCursorOneRecordForTid(String tid){
        SQLiteDatabase rDB = getReadableDatabase();
        Cursor cursor;
        cursor = rDB.query(BizInfo.TABLE_NAME, new String[]{
                        BizInfo._ID,
                        BizInfo.TID,
                        BizInfo.BIZ_NUM,
                        BizInfo.SHOP_NAME,
                        BizInfo.SHOP_CEO,
                        BizInfo.SHOP_ADDRESS,
                        BizInfo.SHOP_TEL,
                        BizInfo.SHOP_ID,
                        BizInfo.AGENCY_NAME,
                        BizInfo.AGENCY_TEL,
                        BizInfo.USER_NAME,
                        BizInfo.VAT_VAL_PER,
                        BizInfo.SERVICE_VAL_PER,
                        BizInfo.VAT_VAL_INCLUDE,
                        BizInfo.USER_MSG,
                        BizInfo.SERVER_SYNC,
                        BizInfo.POSCODE,
                        BizInfo.PIN,
                        BizInfo.TS,
                        BizInfo.IP_ADDRESS,
                        BizInfo.PORT_NUMBER},
                "tid = ?",
                new String[] {tid}, null, null, null);
//        rDB.close();
        return cursor;
    }

    /*사업자 정보를 삭제하는 메소드*/
    public String deleteBizInfo(String _id){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        try{
            wDB.delete(BizInfo.TABLE_NAME, "_id=?", new String[]{_id});
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
        }
        catch (Exception e){
            wDB.endTransaction();
            LOG.i(TAG,"사업자정보삭제실패");
            wDB.close();
            return "사업자정보삭제실패";
        }
        if (getBizInfoCursor().getCount() == 0) {
            setSystemUseTid("0");
        }

        LOG.i(TAG,"사업자정보삭제성공");
        wDB.close();
        return "success";
    }
    /*사업자 설정을 수정하는 메소드*/
    public String setBizInfoSetting(String tid, String userName, String vatValPer, String serviceValPer, String serverSync, String vatValInclude, String userMsg){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(BizInfo.USER_NAME, userName);
        cv.put(BizInfo.VAT_VAL_PER, vatValPer);
        cv.put(BizInfo.SERVICE_VAL_PER, serviceValPer);
        cv.put(BizInfo.VAT_VAL_INCLUDE, vatValInclude);
        cv.put(BizInfo.USER_MSG, userMsg);
        cv.put(BizInfo.SERVER_SYNC, serverSync);
        if (serverSync.equals("1")) {
            cv.put(BizInfo.TS, mSdf.format(new Date())); // 서버동기화가 on이되면 현재시간을 ts에 삽입한다.
        }
        try{
            wDB.update(BizInfo.TABLE_NAME, cv, "tid='" + tid + "'", null);
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
        }
        catch (Exception e){
            wDB.endTransaction();
            LOG.i(TAG,"사업자설정수정실패");
            wDB.close();
            return "사업자정보설정실패";
        }
        LOG.i(TAG,"사업자정보설정성공");
        wDB.close();
        return "success";
    }
    /*거래내역을 등록하는 메소드*/
    public long addPaymentHistory(String... values){ // _id를 제외하고 총 30개
        LOG.i(TAG, "addPaymentHistory(String... values)");
        SQLiteDatabase wDB = getWritableDatabase();
        long id;
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(PaymentHistory.TRAN_TYPE, values[0]);
        cv.put(PaymentHistory.CARD_NUM, values[1]);
        cv.put(PaymentHistory.CARD_NAME, values[2]);
        cv.put(PaymentHistory.CARD_COMPANY, values[3]);
        cv.put(PaymentHistory.BALANCE, values[4]);
        cv.put(PaymentHistory.TOTAL_VAL, values[5]);
        cv.put(PaymentHistory.ORIGIN_VAL, values[6]);
        cv.put(PaymentHistory.VAT_VAL, values[7]);
        cv.put(PaymentHistory.SERVICE_VAL, values[8]);
        cv.put(PaymentHistory.MONTH_VAL, values[9]);
        cv.put(PaymentHistory.APPROVAL_NUM_ORG, values[10]);
        cv.put(PaymentHistory.APPROVAL_NUM, values[11]);
        cv.put(PaymentHistory.APPROVAL_DATE, values[12]);
        cv.put(PaymentHistory.IS_KEY_IN, values[13]);
        cv.put(PaymentHistory.RESULT_CODE, values[14]);
        cv.put(PaymentHistory.SHOP_TID, values[15]);
        cv.put(PaymentHistory.SHOP_BIZ_NUM, values[16]);
        cv.put(PaymentHistory.SHOP_NAME, values[17]);
        cv.put(PaymentHistory.SHOP_CEO, values[18]);
        cv.put(PaymentHistory.SHOP_ADDRRESS, values[19]);
        cv.put(PaymentHistory.SHOP_TEL, values[20]);
        cv.put(PaymentHistory.USER_MSG, values[21]);
        cv.put(PaymentHistory.CANCEL_REASON, values[22]);
        cv.put(PaymentHistory.CANCELED, values[23]);
        cv.put(PaymentHistory.SEND_DATA, values[24]);
        cv.put(PaymentHistory.SEND_FAILED_REASON, values[25]);
        cv.put(PaymentHistory.SHOP_ID, values[26]);
        cv.put(PaymentHistory.PIN, values[27]);
        cv.put(PaymentHistory.POSCODE, values[28]);
        cv.put(PaymentHistory.EULLYEON, values[29]);
        cv.put(PaymentHistory.VAT_VAL_PER, values[30]);
        cv.put(PaymentHistory.SERVICE_VAL_PER, values[31]);
        cv.put(PaymentHistory.VAT_VAL_INCLUDE, values[32]);
        cv.put(PaymentHistory.USER_NAME, values[33]);
        cv.put(PaymentHistory.IS_SERVER_DATA, values[34]);
        cv.put(PaymentHistory.PRINT_MESSAGE, values[35]);
        cv.put(PaymentHistory.IS_MASKED, values[36]);
        cv.put(PaymentHistory.APPROVAL_NUM_DEF, values[37]);

        LOG.d(TAG, "values[11]: " + values[11] );

        if (values[23].equals("1")) { // 취소데이터이고
            if (values[34].equals("1")) { // 서버로받은 데이터일경우
                ContentValues cv2 = new ContentValues(); // 기존승인건을 CANCELED컬럼을 0 -> 1로 변경
                cv2.put(PaymentHistory.CANCELED, PaymentHistory.CANCELED_ON_CANCEL);
                try {
                    wDB.update(PaymentHistory.TABLE_NAME, cv2, "APPROVAL_NUM='" + values[37] + "'", null);
                }
                catch (Exception e) {
                     LOG.e(TAG, e.getMessage());
                    //기존 승인 건이 없는 경우 아무것도 하지않음.
                }

            }
        }


        try {
            id = wDB.insert(PaymentHistory.TABLE_NAME, null, cv);
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
        }
        catch (Exception e){
            wDB.endTransaction();
            LOG.e(TAG,"거래내역등록실패");
            wDB.close();
            return -1;

        }
        LOG.i(TAG, "거래내역등록성공");
        wDB.close();
        return id;
    }
    /*거래내역 전체의 커서를 리턴해주는 메소드(현재는 사용하지 않음)*/
    public Cursor getPaymentHistoryCursor(){
        SQLiteDatabase rDB = getReadableDatabase();
        Cursor cursor;
        cursor = rDB.query(PaymentHistory.TABLE_NAME, new String[]{ // 총 30개의 컬럼이 담긴 Cursor 리턴
                        PaymentHistory._ID,
                        PaymentHistory.TRAN_TYPE,
                        PaymentHistory.CARD_NUM,
                        PaymentHistory.CARD_NAME,
                        PaymentHistory.CARD_COMPANY,
                        PaymentHistory.BALANCE,
                        PaymentHistory.TOTAL_VAL,
                        PaymentHistory.ORIGIN_VAL,
                        PaymentHistory.VAT_VAL,
                        PaymentHistory.SERVICE_VAL,
                        PaymentHistory.MONTH_VAL,
                        PaymentHistory.APPROVAL_NUM_ORG,
                        PaymentHistory.APPROVAL_NUM,
                        PaymentHistory.APPROVAL_DATE,
                        PaymentHistory.IS_KEY_IN,
                        PaymentHistory.RESULT_CODE,
                        PaymentHistory.SHOP_TID,
                        PaymentHistory.SHOP_BIZ_NUM,
                        PaymentHistory.SHOP_NAME,
                        PaymentHistory.SHOP_CEO,
                        PaymentHistory.SHOP_ADDRRESS,
                        PaymentHistory.SHOP_TEL,
                        PaymentHistory.USER_MSG,
                        PaymentHistory.CANCEL_REASON,
                        PaymentHistory.CANCELED,
                        PaymentHistory.SEND_DATA,
                        PaymentHistory.SEND_FAILED_REASON,
                        PaymentHistory.SHOP_ID,
                        PaymentHistory.PIN,
                        PaymentHistory.POSCODE,
                        PaymentHistory.EULLYEON,
                        PaymentHistory.VAT_VAL_PER,
                        PaymentHistory.SERVICE_VAL_PER,
                        PaymentHistory.VAT_VAL_INCLUDE,
                        PaymentHistory.USER_NAME,
                        PaymentHistory.IS_SERVER_DATA,
                        PaymentHistory.PRINT_MESSAGE,
                        PaymentHistory.IS_MASKED,
                        PaymentHistory.APPROVAL_NUM_DEF},
                null, null, null, null, null);
        return cursor;
    }
    /**
     * 서버로 전송실패한 결제내역이 담긴 커서를 리턴
     * 앱 로딩시 실행
     * @return
     */
    public Cursor getPaymentHistorySendDataFailedCursor(){
        SQLiteDatabase rDB = getReadableDatabase();
        Cursor cursor;
        String where = "(("
                + PaymentHistory.TRAN_TYPE + " IN ('1', '2', '3', '4', '5')) AND (" + PaymentHistory.CANCELED +" = '0') AND (" + PaymentHistory.SEND_DATA + " = '1'))"
                + " OR ((" + PaymentHistory.TRAN_TYPE + " IN ('6', '7', '8', '9', '10')) AND (" + PaymentHistory.CANCELED +" = '1') AND (" + PaymentHistory.SEND_DATA +" = '1'))";
        LOG.i(TAG, where);
        String query = String.format("SELECT * FROM '%s' WHERE (%s)", PaymentHistory.TABLE_NAME,where);
        cursor = rDB.rawQuery(query, null);
        return cursor;
    }
    /*Where절(조건절)을 매개변수로 받아 커서를 리턴해주는 메소드*/
    public Cursor getPaymentHistoryCursorWhere(String where){
        SQLiteDatabase rDB = getReadableDatabase();
        Cursor cursor;
        String query = String.format("SELECT * FROM '%s' WHERE (%s)", PaymentHistory.TABLE_NAME, where);
        cursor = rDB.rawQuery(query,null);
        return cursor;
    }
    /*거래를 취소(Update)하는 메소드*/
    public String setPaymentCancel(String id){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(PaymentHistory.CANCELED, PaymentHistory.CANCELED_ON_CANCEL);
        try{
            wDB.update(PaymentHistory.TABLE_NAME, cv, "_id='" + id + "'", null);
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
        }
        catch (Exception e){
            wDB.endTransaction();
            LOG.i(TAG,"거래내역수정(거래취소)실패");
            wDB.close();
            return "거래내역수정(거래취소)실패";
        }
        LOG.i(TAG,"거래내역수정(거래취소)성공");
        wDB.close();
        return "success";
    }
    /* 데이터 전송 성공여부를 성공으로 바꿔주는 메소드*/
    public String setPaymentHistorySendDataSuccess(String approvalNumOrg){
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(PaymentHistory.SEND_DATA, PaymentHistory.SEND_DATA_SUCCESS);
        try{
            wDB.update(PaymentHistory.TABLE_NAME, cv, "APPROVAL_NUM_ORG='" + approvalNumOrg + "'", null);
            wDB.setTransactionSuccessful();
            wDB.endTransaction();
        }
        catch (Exception e){
            wDB.endTransaction();
            LOG.i(TAG,"데이터 전송 성공으로 수정실패");
            wDB.close();
            return "데이터 전송 성공으로 수정실패";
        }
        LOG.i(TAG,"데이터 전송 성공으로 수정성공");
        wDB.close();
        return "success";
    }



    public void deletePaymentHistoryFor80days(){
        String temp= ""; // 오버라이트 함수넣기~!!
        for (int i = 0; i < 16; i ++) {
            temp += Util.getRandomString(1);
        }
        LOG.d(TAG, "80일경과 maked card number step1: " + temp);
        deletePaymentHistoryFor80days(temp, 1);
        temp = "FFFFFFFFFFFFFFFF";
        LOG.d(TAG, "80일경과 maked card number step2: " + temp);
        deletePaymentHistoryFor80days(temp, 2);
        temp = "****************";
        LOG.d(TAG, "80일경과 maked card number step3: " + temp);
        deletePaymentHistoryFor80days(temp, 3);
    }

    private void deletePaymentHistoryFor80days(String updateCardData, int stemp) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -80);
        String a80daysAgoDate = mSdf.format(calendar.getTime());
        SQLiteDatabase wDB = getWritableDatabase();
        wDB.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(PaymentHistory.CARD_NUM, updateCardData);
        if (stemp == 3) {
            cv.put(PaymentHistory.IS_MASKED, "1");
        }
        wDB.update(PaymentHistory.TABLE_NAME, cv, "(APPROVAL_DATE < '" + a80daysAgoDate + "')" + " AND " + "(IS_MASKED = '0')", null);
        wDB.setTransactionSuccessful();
        wDB.endTransaction();
        wDB.close();
    }


}


package kr.checkbill.cb2.activity.listview;

/**
 * Created by kwonhyeokjin on 2017. 1. 11..
 */

public class BizListItem {
    public String _id;
    public String tid;
    public String bizNum;
    public String shopName;
    public String shopCeo;
    public String shopAddress;
    public String shopTel;
    public String poscode;
    public String shopId;
}

package kr.checkbill.cb2.server;

/**
 * Created by kwonhyeokjin on 2017. 2. 20..
 */

public class SetOrdersData {
    public String approval_num_org;
    String tid;
    String biz_num;
    String shop_id;
    String pin;
    String poscode;
    String tran_type;
    String card_num;
    String card_name;
    String card_company;
    String balance;
    String total_val;
    String origin_val;
    String vat_val;
    String service_val;
    String month_val;
    String approval_num;
    String approval_date;
    String cancel_reason;
    String is_key_in;
    String canceled;
    String rate_service;
    String rate_vat;
    String flag_vat;
    String is_union_pay;
    String staff_nm;
    String approval_num_def;
    String print_message;

    public SetOrdersData(String approval_num_org, String tid, String biz_num, String tran_type,
                         String shop_id, String pin, String poscode,
                         String card_num, String card_name, String card_company, String balance,
                         String total_val, String origin_val, String vat_val, String service_val,
                         String month_val, String approval_num, String approval_date, String cancel_reason,
                         String is_key_in, String canceled, String rate_service, String rate_vat,
                         String flag_vat, String is_union_pay, String staff_nm, String approval_num_def, String print_message) {
        this.approval_num_org = approval_num_org;
        this.tid = tid;
        this.biz_num = biz_num;
        this.tran_type = tran_type;
        this.shop_id = shop_id;
        this.pin = pin;
        this.poscode = poscode;
        this.card_num = card_num;
        this.card_name = card_name;
        this.card_company = card_company;
        this.balance = balance;
        this.total_val = total_val;
        this.origin_val = origin_val;
        this.vat_val = vat_val;
        this.service_val = service_val;
        this.month_val = month_val;
        this.approval_num = approval_num;
        this.approval_date = approval_date;
        this.cancel_reason = cancel_reason;
        this.is_key_in = is_key_in;
        this.canceled = canceled;
        this.rate_service = rate_service;
        this.rate_vat = rate_vat;
        this.flag_vat = flag_vat;
        this.is_union_pay = is_union_pay;
        this.staff_nm = staff_nm;
        this.approval_num_def = approval_num_def;
        this.print_message = print_message;
    }
}

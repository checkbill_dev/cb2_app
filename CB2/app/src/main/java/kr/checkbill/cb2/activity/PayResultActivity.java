package kr.checkbill.cb2.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.checkbill.cbapi.ResultStartBusness;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.base.DaouStartBizActivity;
import kr.checkbill.cb2.activity.payment.CashPayCancelActivity;
import kr.checkbill.cb2.activity.payment.CardPayCancelActivity;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.util.SendReceipt;
import kr.checkbill.cb2.util.Util;
import kr.checkbill.cbkiccprtapi.CBKiccPrtApi;
import kr.checkbill.cbkiccprtapi.CBKiccPrtApiCallback;

public class PayResultActivity extends DaouStartBizActivity
        implements CBKiccPrtApiCallback, View.OnClickListener {
    private static final String TAG = "PayResultActivity";

    private String tranType;
    private String paymentId;
    private int whichIntent;
    private PaymentHistory paymentHistory;
    private String title = "";
    private String month;
    private String sortation;
    private String cardName;
    private String cardCompany;
    private String cancelReason;
    private String balance;
    private String canceled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_pay_result);

        DBHelper DB = new DBHelper(this);

        paymentId = getIntent().getStringExtra("paymentId");
        whichIntent = getIntent().getIntExtra("whichIntent", 1);

        registerOnClickListener();

        String where = "_id = '" + paymentId + "'";
        paymentHistory = new PaymentHistory();
        Cursor paymentHistoryCursor = DB.getPaymentHistoryCursorWhere(where);
        while (paymentHistoryCursor.moveToNext()){
            paymentHistory.fetch(paymentHistoryCursor);
        }
        tranType = paymentHistory.tranType;
        month = paymentHistory.monthVal;
        cardName = paymentHistory.cardName;
        cardCompany = paymentHistory.cardCompany;
        cancelReason = paymentHistory.cancelReason;
        balance = paymentHistory.balance;
        canceled = paymentHistory.canceled;
        sortation = "****-####-####-****";
        if (month == null || month.isEmpty() || month.equals("0") || month.equals("1")) {
            month = "일시불";
        }
        resultSetText();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_bt_print:
                CBKiccPrtApi mPrtApi;
                mPrtApi = new CBKiccPrtApi(this, this);
                if (!mPrtApi.KiccPrt_IsConnect()) {
                    mPrtApi.KiccPrt_Connect("");
                }
                break;
            case R.id.btn_send_receipt:
                if (isCard(paymentHistory.cardName)) {
                    SendReceipt.sendCashReceipt(this, title, paymentHistory.shopName, paymentHistory.shopCeo, paymentHistory.shopBizNum,
                            paymentHistory.shopTel, paymentHistory.shopAddress, sortation, paymentHistory.approvalDate,
                            paymentHistory.approvalNumOrg, paymentHistory.approvalNum, paymentHistory.shopTid, cancelReason,
                            Util.commaFormat(paymentHistory.originVal), Util.commaFormat(paymentHistory.vatVal),
                            Util.commaFormat(paymentHistory.serviceVal), Util.commaFormat(paymentHistory.totalVal),
                            paymentHistory.printMessage, paymentHistory.userName, paymentHistory.userMsg);
                }
                else {
                    SendReceipt.sendCardReceipt(this, title, paymentHistory.shopName, paymentHistory.shopCeo, paymentHistory.shopBizNum,
                            paymentHistory.shopTel, paymentHistory.shopAddress, "****-####-####-****", month,
                            paymentHistory.approvalDate, paymentHistory.approvalNumOrg, paymentHistory.approvalNum, paymentHistory.shopTid,
                            paymentHistory.cardName, paymentHistory.cardCompany, Util.commaFormat(balance),
                            Util.commaFormat(paymentHistory.originVal), Util.commaFormat(paymentHistory.vatVal),
                            Util.commaFormat(paymentHistory.serviceVal), Util.commaFormat(paymentHistory.totalVal),
                            paymentHistory.printMessage, paymentHistory.userName, paymentHistory.userMsg);
                }

                break;
            case R.id.btn_pay_cancel:
                DBHelper db = new DBHelper(this);
                if (BT_IsConnect()){
                    if (paymentHistory.shopTid.equals(db.getSystemUseTid())) { // 선택한 사업자와 개시거래된 사업자가 같은경우
                        startCancelActivity(tranType);
                    }
                    else { // 선택한 사업자와 개시거래된 사업자가 다른경우 : 개시거래 실시
                        showDialog("잠시만 기다려주세요..");
                        DAOU_StartBusness(Util.getBizInfo(this, paymentHistory.shopTid).ipAdress, Util.getBizInfo(this, paymentHistory.shopTid).portNumber, paymentHistory.shopBizNum,
                                paymentHistory.shopTid, paymentHistory.shopId, false);
                    }
                } else {
                    showAlertDialog("알림", new String[]{"앱을 리더기와 연결을 한 후에 이용해주세요."}, true,
                            true, false, false, "확인", null, null, null, null, null);
                }
                break;
        }
    }
    @Override
    public void DAOU_StartBusness_CB(final ResultStartBusness resultStartBusness) {
        super.DAOU_StartBusness_CB(resultStartBusness);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (resultStartBusness.resultCode == 0) {
                    DBHelper db = new DBHelper(PayResultActivity.this);
                    if (db.setSystemUseTid(paymentHistory.shopTid).equals("success")){
                        startCancelActivity(tranType);
                    }
                    else {
                        showToast("개시거래를 한후 다시 이용해주세요.");
                    }
                }
            }
        });
    }

    @Override
    public void BT_KiccPrt(final int resultCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (resultCode) {
                    case 0:
                        showToast("프린터 연결 실패 휴대폰설정에서 프린터를 연결을한 후에 이용해주세요.");
                        break;
                    case 1:
                        showToast("이미 연결되어있습니다.");
                        break;
                    case 2:
                        showToast("프린터에 연결되었습니다.");
                        CBKiccPrtApi mPrtApi;
                        mPrtApi = new CBKiccPrtApi(PayResultActivity.this, PayResultActivity.this);
                        if (paymentHistory.cardName == null || paymentHistory.cardName.isEmpty() || paymentHistory.cardName.equals("일반영수증")) {
                            mPrtApi.KiccPrt_CashPrint(title, paymentHistory.shopName, paymentHistory.shopCeo, paymentHistory.shopBizNum,
                                    paymentHistory.shopTel, paymentHistory.shopAddress, sortation, paymentHistory.approvalDate,
                                    paymentHistory.approvalNumOrg, paymentHistory.approvalNum, paymentHistory.shopTid, cancelReason,
                                    Util.commaFormat(paymentHistory.originVal), Util.commaFormat(paymentHistory.vatVal),
                                    Util.commaFormat(paymentHistory.serviceVal), Util.commaFormat(paymentHistory.totalVal),
                                    paymentHistory.printMessage, paymentHistory.userName, paymentHistory.userMsg);
                        }
                        else {
                            mPrtApi.KiccPrt_CardPrint(title, paymentHistory.shopName, paymentHistory.shopCeo, paymentHistory.shopBizNum,
                                    paymentHistory.shopTel, paymentHistory.shopAddress, "****-####-####-****", month,
                                    paymentHistory.approvalDate, paymentHistory.approvalNumOrg, paymentHistory.approvalNum, paymentHistory.shopTid,
                                    paymentHistory.cardName, paymentHistory.cardCompany, Util.commaFormat(balance),
                                    Util.commaFormat(paymentHistory.originVal), Util.commaFormat(paymentHistory.vatVal),
                                    Util.commaFormat(paymentHistory.serviceVal), Util.commaFormat(paymentHistory.totalVal),
                                    paymentHistory.printMessage, paymentHistory.userName, paymentHistory.userMsg);
                        }

                        break;
                }
            }
        });
    }

    @Override
    public void PrintResult_KiccPrt(final int resultCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CBKiccPrtApi mPrtApi;
                mPrtApi = new CBKiccPrtApi(PayResultActivity.this, PayResultActivity.this);
                switch (resultCode) {
                    case 0:
                        showToast("영수증이 출력되었습니다.");
                        mPrtApi.KiccPrt_DisConnect();
                        break;
                    case 1:
                        showToast("영수증이 출력에 실패했습니다. 다시 시도해주세요.");
                        mPrtApi.KiccPrt_DisConnect();
                        break;
                }
            }
        });
    }
    private boolean isCard(String cardName) {
        if (cardName == null || cardName.isEmpty() || cardName.equals("일반영수증")) {
            return false;
        }
        else {
            return true;
        }
    }
    private void startCancelActivity(String tranType) {
        Class c;
        if (tranType.equals("5")) {
            c = CardPayCancelActivity.class;
        }
        else {
            c = CashPayCancelActivity.class;
        }
        Intent intent = new Intent(this, c);
        intent.putExtra("paymentId", paymentId);
        startActivity(intent);
        finish();
    }
    private void registerOnClickListener() {
        (findViewById(R.id.btn_bt_print)).setOnClickListener(this);
        (findViewById(R.id.btn_send_receipt)).setOnClickListener(this);
        (findViewById(R.id.btn_pay_cancel)).setOnClickListener(this);
    }
    private void resultSetText() {
        if (canceled.equals("0")){
            if (whichIntent == 1) {
                (findViewById(R.id.btn_pay_cancel)).setVisibility(View.VISIBLE);
            }
            switch (tranType){
                case PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE: // 지출증빙
                    title = PaymentHistory.TITLE_CASH;
                    sortation += "(" + PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE_SHOW_OO + ")";
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CASH_VOLUNTARY: // 자진발급
                    title = PaymentHistory.TITLE_CASH;
                    sortation += "(" + PaymentHistory.TRANTYPE_CASH_VOLUNTARY_SHOW_OO + ")";
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION: // 소득공제
                    title = PaymentHistory.TITLE_CASH;
                    sortation += "(" + PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION_SHOW_OO + ")";
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CASH_DEFAULT: // 현금영수증없는 현금결제
                    title = PaymentHistory.TITLE_CASH;
                    sortation = PaymentHistory.TRANTYPE_CASH_DEFAULT_SHOW_OO;
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CREDIT_CARD: // 카드 결제
                    title = PaymentHistory.TITLE_CARD;
                    SetText(R.id.txt_card_num, "****-####-####-****");
                    SetText(R.id.txt_month, month);
                    (findViewById(R.id.ll_sortation)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;
            }
        }
        else if (canceled.equals("1")) {
            switch (tranType) {
                case PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE: // 지출증빙
                    title = PaymentHistory.TITLE_CASH;
                    sortation += "(" + PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE_SHOW_OO + ")";
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CASH_VOLUNTARY: // 자진발급
                    title = PaymentHistory.TITLE_CASH;
                    sortation += "(" + PaymentHistory.TRANTYPE_CASH_VOLUNTARY_SHOW_OO + ")";
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION: // 소득공제
                    title = PaymentHistory.TITLE_CASH;
                    sortation += "(" + PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION_SHOW_OO + ")";
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CASH_DEFAULT: // 현금영수증없는 현금결제
                    title = PaymentHistory.TITLE_CASH;
                    sortation = PaymentHistory.TRANTYPE_CASH_DEFAULT_SHOW_OO;
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CREDIT_CARD: // 카드
                    title = PaymentHistory.TITLE_CARD;
                    SetText(R.id.txt_card_num, "****-####-####-****");
                    (findViewById(R.id.ll_sortation)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;

                case PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE_CANCEL: // 지출증빙취소
                    title = PaymentHistory.TITLE_CASH_CANCEL;
                    sortation += "(" + PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE_CANCEL_SHOW_XX + ")";
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CASH_VOLUNTARY_CANCEL: // 자진발급취소
                    title = PaymentHistory.TITLE_CASH_CANCEL;
                    sortation += "(" + PaymentHistory.TRANTYPE_CASH_VOLUNTARY_CANCEL_SHOW_XX + ")";
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION_CANCEL: // 소득공제취소
                    title = PaymentHistory.TITLE_CASH_CANCEL;
                    sortation += "(" + PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION_CANCEL_SHOW_XX + ")";
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CASH_DEFAULT_CANCEL: // 현금영수증없는 현금결제취소
                    title = PaymentHistory.TITLE_CASH_CANCEL;
                    sortation += "(" + PaymentHistory.TRANTYPE_CASH_DEFAULT_CANCEL_SHOW_XX + ")";
                    sortation = PaymentHistory.TRANTYPE_CASH_DEFAULT_CANCEL_SHOW_XX;
                    SetText(R.id.txt_sortation, sortation);
                    (findViewById(R.id.ll_card_num)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_month)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
                    break;
                case PaymentHistory.TRANTYPE_CREDIT_CARD_CANCEL: // 카드취소
                    title = PaymentHistory.TITLE_CARD_CANCEL;
                    SetText(R.id.txt_card_num, "****-####-####-****");
                    SetText(R.id.txt_month, month);
                    (findViewById(R.id.ll_sortation)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
                    break;
            }
        }


        if (cardName == null || cardName.isEmpty()) {
            (findViewById(R.id.ll_card_name)).setVisibility(View.GONE);
        }

        if (cardCompany == null || cardCompany.isEmpty()) {
            (findViewById(R.id.ll_card_company)).setVisibility(View.GONE);

        }
        if (cancelReason == null || cancelReason.isEmpty()) {
            (findViewById(R.id.ll_cancel_reason)).setVisibility(View.GONE);
        }
        else if (cancelReason.equals("1")) {
            cancelReason = "거래취소";
            SetText(R.id.txt_cancel_reason, cancelReason);
        }
        else if (cancelReason.equals("2")) {
            cancelReason = "거래취소";
            SetText(R.id.txt_cancel_reason, cancelReason);
        }
        else if (cancelReason.equals("3")) {
            cancelReason = "거래취소";
        }
        if (balance == null || balance.isEmpty() || balance.equals("0")) {
            (findViewById(R.id.ll_balance)).setVisibility(View.GONE);
        }
        SetText(R.id.txt_title, title);
        SetText(R.id.txt_shop_name, paymentHistory.shopName);
        SetText(R.id.txt_shop_ceo, paymentHistory.shopCeo);
        SetText(R.id.txt_biz_num, paymentHistory.shopBizNum);
        SetText(R.id.txt_shop_tel, paymentHistory.shopTel);
        SetText(R.id.txt_shop_address, paymentHistory.shopAddress);
        SetText(R.id.txt_card_name, cardName);
        SetText(R.id.txt_card_company, cardCompany);
        SetText(R.id.txt_approval_date, paymentHistory.approvalDate);
        SetText(R.id.txt_approval_num_org, paymentHistory.approvalNumOrg);
        SetText(R.id.txt_approval_num, paymentHistory.approvalNum);
        SetText(R.id.txt_shop_tid, paymentHistory.shopTid);
        SetText(R.id.txt_balance, Util.commaFormat(balance) + "원");
        SetText(R.id.txt_origin_val, Util.commaFormat(paymentHistory.originVal) + "원");
        SetText(R.id.txt_vat_val, Util.commaFormat(paymentHistory.vatVal) + "원");
        SetText(R.id.txt_service_val, Util.commaFormat(paymentHistory.serviceVal) + "원");
        SetText(R.id.txt_total_val, Util.commaFormat(paymentHistory.totalVal) + "원");
        SetText(R.id.txt_user_name, paymentHistory.userMsg);
        SetText(R.id.txt_user_msg, paymentHistory.userMsg);
        SetText(R.id.txt_print_message, paymentHistory.printMessage);
    }
}

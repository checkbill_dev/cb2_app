package kr.checkbill.cb2.activity.base;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.checkbill.cbapi.CBApi;
import com.checkbill.cbapi.ResultPayment;
import com.checkbill.cbapi.ResultStartBusness;
import com.checkbill.cbdaouapi.CBDaouApi;
import com.checkbill.cbdaouapi.CBDaouApiCallback;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.FWUpdateActivity;
import kr.checkbill.cb2.activity.PayResultActivity;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.server.ServerAPI;
import kr.checkbill.cb2.util.ResultPrice;
import kr.checkbill.cb2.util.SingleTon;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.Util;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Administrator on 2017-03-27.
 */

public class DaouActivity extends BaseActivity implements CBDaouApiCallback {

    private static final String TAG = "DaouActivity";
    private int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_PAY_RESULT = 3;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DAOU_setCallback(this, this);
    }

    /******************************************************************************/
    /** 카드 초기화 함수                                                         **/
    /******************************************************************************/
    /**
     * CBDaouApi를 사용하기 위해서는 Activity resume시 아래를 설정해야 합니다.
     * @param context   Context
     * @param callback  Callback
     */
    public void DAOU_setCallback(Context context, CBDaouApiCallback callback) {
        LOG.d(TAG, "DAOU_setCallback");
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_setCallback(context, callback);
    }

    /******************************************************************************/
    /** 블루투스 함수                                                            **/
    /******************************************************************************/
    /**
     * Bluetooth 연결
     * @param bluetoothAddress 블루투스 디바이스 주소
     */
    public void BT_Connect(String bluetoothAddress) {
        LOG.d(TAG, "BT_Connect");
        showDialog(getString(R.string.a_base_connect_ing));
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.BT_Connect(bluetoothAddress);
    }

    /**
     * Bluetooth 연결 해제
     */
    public void BT_Disconnect() {
        LOG.d(TAG, "BT_Disconnect");
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.BT_Disconnect();

    }

    /**
     * Bluetooth 연결 여부 확인
     * @return Bluetooth 연결 여부
     */
    public boolean BT_IsConnect() {
        CBDaouApi mApi = CBDaouApi.getInstance();
        return mApi.BT_IsConnect();
    }

    /******************************************************************************/
    /** 다우데이터 함수                                                          **/
    /******************************************************************************/
    /**
     * 여신금융 소프트웨어 인증번호를 가져온다.
     * @return 여신금융 소프트웨어 인증번호
     */
    public String DAOU_GetSWNum() {
        CBDaouApi mApi = CBDaouApi.getInstance();
        return mApi.DAOU_GetSWNum();
    }

    /**
     * 여신금융 하드웨어 인증번호를 가져온다. DAOU_GetHWum()과 DAOU_GetVanSerial(int index)는 동시에 호출 되면 안됨.
     */
    public void DAOU_GetHWNum() {
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_GetHWNum();
    }

    /**
     * 생산일련번호 (밴시리얼 번호)를 가져온다. DAOU_GetHWum()과 DAOU_GetVanSerial(int index)는 동시에 호출 되면 안됨.
     * @param index 0~15 생산일련번호
     * @return 결과
     */
    public boolean DAOU_GetVanSerial(int index) {
        CBDaouApi mApi = CBDaouApi.getInstance();


        boolean result = mApi.DAOU_GetVanSerial(index);
        return result;
    }

    /**
     * 무결성 검사를 실행
     */
    public void DAOU_VerifyFW() {
        showDialog(getString(R.string.a_base_verify_fw_ing));
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_VerifyFW();

    }

    /**
     * 무결성 결과 리스트 화면 표시
     */
    public void DAOU_ShowVerifyList() {
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_ShowVerifyList();
    }

    /**
     * 단말기 개시거래를 진행합니다.
     * @param ip              IP 어드레스
     * @param port            Port 넘버
     * @param hwNum           여신금융 하드웨어 인증번호
     * @param regNum          사업자번호
     * @param termNum         단말기번호
     * @param vanSerial       밴시리얼
     * @param mainTermNum     (다중사업자) 주단말기번호
     */
    public void DAOU_StartBusness(String ip, String port, String hwNum, String regNum,
                                  String termNum, String vanSerial, String mainTermNum) {
        showDialog(getString(R.string.a_base_start_biz_ing));
        LOG.d(TAG, "termNum: " + termNum);
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_StartBusness(ip, port, hwNum, regNum, termNum, vanSerial, mainTermNum);
    }

    /**
     * 보안키를 갱신합니다
     * @param termNum 단말기번호
     * @param hwNum   하드웨어번호
     * @param vanSerial  여신금융인증번호
     */
    public void DAOU_KeyUpdate(String termNum, String hwNum, String vanSerial) {
        showDialog("보안키 갱신 중입니다.");
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_KeyUpdate(termNum, hwNum, vanSerial);
    }

    /**
     * 신용카드 결제
     * (IC 카드 삽입이나 MSR 긁은 뒤 호출해야 합니다.)
     *
     * @param hwNum        하드웨어 여신 인증번호
     * @param termNum      단말기번호
     * @param vanSerial    밴시리얼
     * @param subTermNum   부사업자 단말기번호
     * @param password     여섯자리 비밀번호 (은련카드 거래시 사용 // 그 외의 경우 password = null)
     * @param bmpSign      사인정보 (무서명 거래시 bmpSign = null)
     * @param totalPrice   결제금액
     * @param taxPrice     부가세
     * @param servicePrice 봉사료
     * @param taxFreePrice 물품가격
     * @param month        할부개월 (일시불 거래시 month = 0)
     * @param fallback     fallback 거래
     */
    public void DAOU_Credit(
                            String hwNum, String termNum, String vanSerial,
                            String subTermNum, String password, Bitmap bmpSign,
                            long totalPrice, long taxPrice, long servicePrice, long taxFreePrice,
                            String month, int fallback
    ) {
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_Credit(hwNum, termNum, vanSerial, subTermNum, password, bmpSign, totalPrice, taxPrice, servicePrice, taxFreePrice, month, fallback);
    }

    /**
     * 신용카드 거래 취소
     * @param hwNum             하드웨어 여신 인증번호
     * @param termNum           단말기번호
     * @param vanSerial         밴시리얼
     * @param subTermNum        부사업자 단말기번호
     * @param password          여섯자리 비밀번호 (은련카드 거래시 사용 // 그 외의 경우 password = null)
     * @param bmpSign           사인정보 (무서명 거래시 signObj = null)
     * @param totalPrice        결제금액
     * @param month             할부개월 (일시불 거래시 month = 0)
     * @param approvalDate      원승인날짜 (YYYYMMDD)
     * @param approvalNumber    원승인번호
     * @param fallback          fallback 값
     */
    public void DAOU_Credit_Cancel(
                                   String hwNum, String termNum, String vanSerial,
                                   String subTermNum, String password, Bitmap bmpSign,
                                   long totalPrice, String month,
                                   String approvalDate, String approvalNumber,
                                   int fallback
    ) {
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_Credit_Cancel(hwNum, termNum, vanSerial, subTermNum, password, bmpSign, totalPrice, month, approvalDate, approvalNumber, fallback);
    }

    /**
     * 현금 결제
     *
     * @param hwNum             하드웨어 여신 인증번호
     * @param termNum           단말기번호
     * @param vanSerial         밴시리얼
     * @param subTermNum        부사업자 단말기번호
     * @param totalPrice        결제금액
     * @param taxPrice          부가세
     * @param servicePrice      봉사료
     * @param taxFreePrice      물품가격
     * @param option            거래구분 "1":소득공제 "2":지출증빙 "3":자진발급
     * @param iskeyin           직접 입력시 true
     *                          현금 영수증 카드 이용시 false
     * @param cash_num          현금 영수증 번호
     */
    public void DAOU_Cash(
                          String hwNum, String termNum, String vanSerial,
                          String subTermNum,
                          long totalPrice, long taxPrice, long servicePrice, long taxFreePrice,
                          String option, boolean iskeyin, String cash_num
    ) {
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_Cash(hwNum, termNum, vanSerial, subTermNum, totalPrice, taxPrice, servicePrice, taxFreePrice, option, iskeyin, cash_num);
    }

    /**
     * 현금 취소
     *
     * @param hwNum             하드웨어 여신 인증번호
     * @param termNum           단말기번호
     * @param vanSerial         밴시리얼
     * @param subTermNum        부사업자 단말기번호
     * @param totalPrice        결제금액
     * @param option            거래취소 "A":소득공제 "B":지출증빙 "E":자진발급
     * @param cancelReason      취소사유코드 "1":거래취소 "2":오류발급 "3":기타
     * @param iskeyin           직접 입력시 true, 현금 영수증 카드 이용시 false
     * @param cash_num          현금 영수증 번호
     * @param approvalDate      원승인날짜 (YYYYMMDD)
     * @param approvalNumber    원승인번호
     */
    public void DAOU_Cash_Cancel(
                                 String hwNum, String termNum, String vanSerial,
                                 String subTermNum,
                                 long totalPrice, String option, boolean iskeyin,
                                 String cancelReason, String cash_num,
                                 String approvalDate, String approvalNumber
                                 ) {
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_Cash_Cancel(hwNum, termNum, vanSerial, subTermNum, totalPrice, option, cancelReason, iskeyin, cash_num, approvalDate, approvalNumber);
    }

    /**
     * 거짓 결제
     *
     * @param termNum           단말기번호
     * @param totalPrice        결제금액
     * @param taxPrice          부가세
     * @param servicePrice      봉사료
     * @param taxFreePrice      물품가격
     * @param approveNum        승인번호 (취소시)
     */
    public void DAOU_Fake(
            String termNum,
            long totalPrice, long taxPrice, long servicePrice, long taxFreePrice, String approveNum, boolean isCard, String cancelReason
    ) {
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_Fake(termNum, totalPrice, taxPrice, servicePrice, taxFreePrice, approveNum, isCard, cancelReason);
    }

    /**
     * 다우데이타 디폴트 아이피 주소를 가져온다.
     * @param isTest 테스트 서버 유무
     * @return 아이피 주소
     */
    public String DAOU_getDefaultIP(boolean isTest) {
        CBDaouApi mApi = CBDaouApi.getInstance();
        return mApi.DAOU_getDefaultIP(isTest);
    }

    /**
     * 다우데이타 디폴트 포트 번호를 가져온다.
     * @param isTest 테스트 서버 유무
     * @return 포트 번호
     */
    public String DAOU_getDefaultPort(boolean isTest) {
        CBDaouApi mApi = CBDaouApi.getInstance();
        return mApi.DAOU_getDefaultPort(isTest);
    }

    public void DAOU_EmvStart() {
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_EmvStart();
    }

    public void DAOU_IsFwUpdate() {
        CBDaouApi mApi = CBDaouApi.getInstance();
        mApi.DAOU_IsFwUpdate();
    }

    private void singleTon_setHWNum(String hwNum) {
        SingleTon inst = SingleTon.getInstance();
        inst.setHWNum(hwNum);
    }

    /**
     * 결제 내역을 전송
     * @param paymentId
     * @param price
     * @param resultPayment
     * @param maskingCardData
     * @param isUnionPay
     */
    public void sendPaymentHistory(String tranType, String paymentId, ResultPrice price, ResultPayment resultPayment, String maskingCardData, String isUnionPay, boolean isKeyIn) {
        ServerAPI.sendPaymentHistory(this, tranType, paymentId, price, resultPayment, maskingCardData, isUnionPay, isKeyIn, this);
    }

    public void receiveServerSync() {
        final BizInfo bizInfo = new BizInfo();
        final DBHelper db = new DBHelper(this);
        Cursor bizInfoCursor = db.getBizInfoCursor();
        int bizInfoSize = bizInfoCursor.getCount();
        if (bizInfoSize != 0) {
            while (bizInfoCursor.moveToNext()) {
                bizInfo.fetch(bizInfoCursor);
                if (bizInfo.serverSync.equals("1")) {
                    showDialog(getString(R.string.a_base_server_ing));
                    ServerAPI.receivePaymentHistory(bizInfo.shopId, bizInfo.poscode, bizInfo.ts, this);
                }
            }
        }
    }

    /**
     * 결제 내역을 재전송
     * @param paymentHistory
     */
    public void reSendPaymentHistory(PaymentHistory paymentHistory) {
        showDialog(getString(R.string.a_base_server_ing));
        ResultPrice price = new ResultPrice();
        ResultPayment resultPayment = new ResultPayment();

        price.totalPrice = Long.parseLong(paymentHistory.totalVal);
        price.taxFreePrice = Long.parseLong(paymentHistory.originVal);
        price.taxPrice = Long.parseLong(paymentHistory.vatVal);
        price.servicePrice = Long.parseLong(paymentHistory.serviceVal);

        boolean isApproval;
        if (paymentHistory.canceled.equals(PaymentHistory.CANCELED_NOT_CANCEL)) {
            isApproval = true;
        }
        else {
            isApproval = false;
        }
        resultPayment.uniqueNum = paymentHistory.approvalNumOrg;
        resultPayment.cardName = paymentHistory.cardName;
        resultPayment.balance = paymentHistory.cardCompany;
        resultPayment.option = paymentHistory.monthVal;
        resultPayment.approveNum = paymentHistory.approvalNum;
        resultPayment.date = paymentHistory.approvalDate;
        resultPayment.cancelReason = paymentHistory.cancelReason;
        resultPayment.isApproval =  isApproval;
        ServerAPI.sendPaymentHistory(this, paymentHistory.tranType, paymentHistory._id, price, resultPayment, paymentHistory.cardNum, paymentHistory.eullyeon, Boolean.valueOf(paymentHistory.isKeyIn).booleanValue(), this);
    }

    /**
     * 단말기 및 사업자 정보 등록
     * @param shopId
     * @param bizNum
     * @param resultStartBusness
     * @param saveFlag             Y로 보내면 무조건 사업자번호 중복 상관없이 무조건 insert됨
     */
    public void registerBizInfo(String shopId, String bizNum, ResultStartBusness resultStartBusness, String saveFlag) {
        showDialog(getString(R.string.a_base_start_biz_ing));
        ServerAPI.registerBizInfo(shopId, bizNum, resultStartBusness.name,
                resultStartBusness.tel, resultStartBusness.ceo, resultStartBusness.addr, resultStartBusness.termNum, saveFlag, this);
    }


    @Override
    public void BT_CB(final int result, final String value) {  //공통
        LOG.d(TAG, "BT_CB resultCode: " + result);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissDialog();

                switch(result) {
                    case CBApi.BT_CONNECTED:
                        LOG.i(TAG, "블루투스가 연결되었습니다.");
                        mBTAddress = value;
                        showToast(getResources().getString(R.string.a_base_connect));
                        break;

                    case CBApi.BT_DISCONNECTED:
                        singleTon_setHWNum("");
                        LOG.i(TAG, "블루투스가 해제되었습니다.");
                        showToast(getString(R.string.a_base_disconnect));
                        break;

                    case CBApi.BT_SERVICE_INIT_FAIL:
                        LOG.i(TAG, "블루투스 초기화를 실패하였습니다.");
                        showToast(getString(R.string.a_base_init_fail));
                        break;

                    case CBApi.BT_INIT_SETTING_OFF: // 블루투스가 OFF일때
                        showToast(getString(R.string.a_base_setting_off));
                        Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                        break;

                    case CBApi.BT_INIT_ADAPTER_FAIL:
                        LOG.i(TAG, "블루투스를 사용할 수 없습니다.");
                        showToast(getString(R.string.a_base_adpater_fail));
                        break;

                    case CBApi.BT_NOT_UART:
                        LOG.i(TAG, "블루투스를 지원하지 않습니다.");
                        showToast(getString(R.string.a_base_not_uart));
                        break;

                    case CBApi.BT_INIT_GPS_PERMISSION:
                        LOG.i(TAG, "블루투스를 사용하기 위해 GPS 권한이 필요합니다.");
                        showToast(getString(R.string.a_base_gps_permission));
                        ActivityCompat.requestPermissions(DaouActivity.this,
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                        break;

                    case CBApi.BT_INIT_GPS_IS_NOT_ON:
                        LOG.i(TAG, "블루투스를 사용하기 위해 GPS 설정을 ON으로 변경해야 합니다.");
                        showToast(getString(R.string.a_base_gps_is_not_on));
                        break;

                    case CBApi.BT_CANCELED:
                        LOG.i(TAG, "블루투스가 취소되었습니다.");
                        showToast(getString(R.string.a_base_canceled));
                        break;

                    case CBApi.BT_GATT_ERROR:
                        LOG.i(TAG, "블루투스 연결 중 에러가 발생하였습니다.");
                        showToast(getString(R.string.a_base_gatt_error));
                        break;
                }
            }
        });

    }

    @Override
    public void Wakeup_CB(byte b) {  //메인
        LOG.d(TAG, "Wakeup_CB()");
        LOG.i("Wakeup_CB", "리더기가 작동되었습니다.");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showToast(getString(R.string.a_base_wake_up));
                String SWNum = DAOU_GetSWNum();
                if (SWNum == null || SWNum.isEmpty()){
                    showToast(getString(R.string.a_base_sw_num_gatt_fail));
                    dismissDialog();
                    BT_Disconnect();
                }
                else {
                    DAOU_GetHWNum();
                }
            }
        });

    }

    @Override
    public void DAOU_GetHWNum_CB(final String hwNum) {  //메인
        LOG.d(TAG, "DAOU_GetHWNum_CB() HWNum : " + hwNum);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (hwNum == null || hwNum.isEmpty()){
                    showToast(getString(R.string.a_base_hw_num_gatt_fail));
                    dismissDialog();
                    BT_Disconnect();
                }
                else {
                    singleTon_setHWNum(hwNum);
                    LOG.i("하드웨어 여신인증번호", hwNum);
                }
                DAOU_GetVanSerial(0);
            }
        });

    }

    @Override
    public void DAOU_Read_Serial_CB(byte b, byte b1, final String van_serial) {  //메인
        LOG.d(TAG, "DAOU_Read_Serial_CB() van_serial : " + van_serial);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (van_serial == null || van_serial.isEmpty()){
                    showToast(getString(R.string.a_base_van_serial_gatt_fail));
                }
                else {
                    SingleTon.getInstance().setVanSerial(van_serial);
                    LOG.i("생산일련번호", van_serial);
                    showToast(getString(R.string.a_base_reader_connect));

                    DAOU_IsFwUpdate();
                }
            }
        });

    }

    @Override
    public void DAOU_StartBusness_CB(ResultStartBusness resultStartBusness) {   //단말기 개시거래
        LOG.d(TAG, "DAOU_StartBusness_CB() resultCode : " + resultStartBusness.resultCode);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissDialog();
            }
        });

    }

    @Override
    public void DAOU_KeyUpdate_CB(final int resultCode, final String msg) {   //단말기 개시거래
        LOG.d(TAG, "DAOU_KeyUpdate_CB() resultCode : " + resultCode);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissDialog();
                if (resultCode == 0){
                    showToast(getString(R.string.a_base_key_update));

                }
                else {
                    showAlertDialog(getString(R.string.a_base_key_result), new String[]{msg + "\n" + getString(R.string.a_base_error_code) + resultCode}, true,
                            true, false, false, getString(R.string.pb_ok), null, null, null, null, null);
                }
            }
        });

    }

    @Override
    public void DAOU_Payment_CB(ResultPayment resultPayment) {  //결제
        LOG.d(TAG, "DAOU_Payment_CB() resultCode : " + resultPayment.resultCode);
    }

    @Override
    public void DAOU_OnICInsert() { //결제
        LOG.d(TAG, "DAOU_OnICInsert()");
    }

    @Override
    public void DAOU_OnICRemove() { //결제
        LOG.d(TAG, "DAOU_OnICRemove()");
        dismissDialog();
    }

    @Override
    public void DAOU_OnMSRRead() { //결제
        LOG.d(TAG, "DAOU_OnMSRRead()");
    }

    @Override
    public void DAOU_OnFallback(int i) { //결제
        LOG.d(TAG, "DAOU_OnFallback()");
    }

    @Override
    public void DAOU_OnCardData(String s) { //결제
        LOG.d(TAG, "DAOU_OnCardData()");
    }

    @Override
    public void DAOU_Payment_Incomplete(int i, String s) { //결제
        LOG.d(TAG, "DAOU_Payment_Incomplete()");
    }

    @Override
    public void DAOU_Error_CB(final int resultCode) { //공통

        runOnUiThread(new Runnable() {
            public void run() {
                dismissDialog();
                String msg = "";
                switch(resultCode) {
                    case CBDaouApi.CODE_NETWORK_CANNOT_ACCESS_ERROR: {
                        msg = getString(R.string.a_base_network_cannot_access_error);
                    }
                    break;

                    case CBDaouApi.CODE_NETWORK_NO_RESPONSE_ERROR: {
                        msg = getString(R.string.a_base_network_no_response_error);
                    }
                    break;

                    case CBDaouApi.CODE_BT_NOT_CONNECT: {
                        msg = getString(R.string.a_base_network_bt_not_connect);
                    }
                    break;
                    case CBDaouApi.CODE_PARAM_INPUT: {
                        msg = getString(R.string.a_base_network_param_input);
                    }
                    break;

                    case CBDaouApi.CODE_PARAM_PORT_NUMBER: {
                        msg = getString(R.string.a_base_network_param_port_number);
                    }
                    break;

                    case CBDaouApi.CODE_READER_ERROR: {
                        msg = getString(R.string.a_base_network_reader_error);
                    }
                    break;

                    case CBDaouApi.CODE_ROOTING_ERROR: {
                        msg = getString(R.string.a_base_network_rooting_error);
                    }
                    break;

                    case CBDaouApi.CODE_PROGRAM_ERROR: {
                        msg = getString(R.string.a_base_network_program_error);
                    }
                    break;
                }
                LOG.d(TAG, "msg: " + msg);
                showAlertDialog(getString(R.string.pb_notice), new String[]{msg}, true,
                        true, false, false, getString(R.string.pb_ok), null, null, null, null, null);

            }
        });

    }



    @Override
    public void DAOU_VerifyFW_CB(boolean b, final String msg, final int resultCode) { //공통
        LOG.d(TAG, "DAOU_VerifyFW_CB() resultCode : " + resultCode);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissDialog();

                if (resultCode == 1){
                    showToast(getString(R.string.a_base_verifi_fw));
                }
                else if (resultCode == 5 || resultCode == 6 || resultCode == 7) {
                    final BizInfo info = Util.getBizInfo(DaouActivity.this);
                    View.OnClickListener event = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DAOU_KeyUpdate(info.tid, getHwNum(), getVanSerial());
                        }
                    };
                    showAlertDialog(getString(R.string.a_base_verifi_fw_result), new String[]{msg + "\n" + getString(R.string.a_base_error_code) + resultCode + "\n" + getString(R.string.a_base_is_key_update)}, true,
                            true, false, false, getString(R.string.pb_ok), null, null, event, null, null);
                }
                else {
                    showAlertDialog(getString(R.string.a_base_verifi_fw_result), new String[]{msg + "\n" + getString(R.string.a_base_error_code) + resultCode + "\n" + getString(R.string.a_base_ask_company)}, true,
                            true, false, false, getString(R.string.pb_ok), null, null, null, null, null);
                }
            }
        });


    }

    @Override
    public void DAOU_CardError_CB(int errorCode) {
        LOG.d(TAG, "DAOU_CardError_CB() errorCode: " + errorCode);
    }

    @Override
    public void DAOU_FwVersionCheckCB(String version) {

    }

    @Override
    public void DAOU_Write_HASH_CB(final int code) {

    }

    @Override
    public void DAOU_Sleep_Ctrl_CB(final byte code) {

    }

    String mBTAddress;
    @Override
    public void DAOU_IsFwUpdateCB(final boolean result, String currentVersion, String Update) {
        LOG.d(TAG, "DAOU_IsFwUpdateCB result:"+result);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(result == true){
                    Intent intent = new Intent(DaouActivity.this, FWUpdateActivity.class);
                    intent.putExtra("btaddress", mBTAddress);
                    startActivityForResult(intent, 30000);
                }
            }
        });


    }

    @Override
    public void DAOU_Need_StartBusness() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showToast(getString(R.string.a_base_neek_startbiz));
            }
        });
    }

    @Override
    public void onResponse(Call<Object> call, Response<Object> response) {
        super.onResponse(call, response);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissDialog();
            }
        });
    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {
        super.onFailure(call, t);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissDialog();
            }
        });
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        LOG.d(TAG, "onActivityResult"+requestCode+" "+resultCode);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (requestCode) {
                    case REQUEST_ENABLE_BT:
                        dismissDialog();
                        break;
                }
            }
        });
    }
    protected void startPayResult(int whichIntent, long paymentId) {
        Intent intent = new Intent(this, PayResultActivity.class);
        intent.putExtra("whichIntent", whichIntent);
        intent.putExtra("paymentId", String.valueOf(paymentId));
        startActivity(intent);
        if (whichIntent == 0) {
            finish();
        }
    }
}

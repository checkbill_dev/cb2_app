package kr.checkbill.cb2.server;

import java.util.ArrayList;

/**
 * Created by kwonhyeokjin on 2017. 3. 22..
 */

public class GetOrdersResponse {
    public String rowcnt;
    public String ts;
    public ArrayList<GetOrdersResponseData> data = new ArrayList<GetOrdersResponseData>();

    public GetOrdersResponse(String rowcnt, String ts, ArrayList<GetOrdersResponseData> data) {
        this.rowcnt = rowcnt;
        this.ts = ts;
        this.data = data;
    }
}

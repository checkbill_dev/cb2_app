package kr.checkbill.cb2.server;

/**
 * Created by kwonhyeokjin on 2017. 2. 21..
 */

public class ReaderIdResponse {
    public String result_val;
    public String poscode;
    public String pin;
    public String result;


    public void setResult_val(String result_val) {
        this.result_val = result_val;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public void setPoscode(String poscode) {
        this.poscode = poscode;
    }

    public void setResult(String result) {
        this.result = result;
    }

}

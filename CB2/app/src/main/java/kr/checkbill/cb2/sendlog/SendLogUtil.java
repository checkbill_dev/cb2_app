package kr.checkbill.cb2.sendlog;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kr.checkbill.cb2.util.LOG;

public class SendLogUtil {
	
	private final static String TAG = "SendLogUtil";

	private final static String MAIL_ADDRESS = "system@checkbill.kr";
	private final static String MAIL_PASSWORD = "checkbillsystem";
	private final static String LINE_SEPARATOR = System.getProperty("line.separator");
	
	
	public static String getMailAddress() {
		return MAIL_ADDRESS;
	}
	
	public static String getMailPassword() {
		return MAIL_PASSWORD;
	}
	
	public static String getFilePath(Context context) {
		
		String filepath = "";
		
		if(context != null) {
			filepath = context.getApplicationContext().getFilesDir().getAbsolutePath()+"/debug_log.txt";
		}
		return filepath;
	}
	
	public static String getZipFilePath(Context context) {
		
		String filepath = "";
		
		if(context != null) {
			filepath = context.getApplicationContext().getFilesDir().getAbsolutePath()+"/debug_log.zip";
		}
		return filepath;
	}
	
	public static String getFormattedKernelVersion()
	{
	    String procVersionStr;

	    try {
	        BufferedReader reader = new BufferedReader(new FileReader("/proc/version"), 256);
	        try {
	            procVersionStr = reader.readLine();
	        } finally {
	            reader.close();
	        }

	        final String PROC_VERSION_REGEX =
	            "\\w+\\s+" + /* ignore: Linux */
	            "\\w+\\s+" + /* ignore: version */
	            "([^\\s]+)\\s+" + /* group 1: 2.6.22-omap1 */
	            "\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+" + /* group 2: (xxxxxx@xxxxx.constant) */
	            "\\([^)]+\\)\\s+" + /* ignore: (gcc ..) */
	            "([^\\s]+)\\s+" + /* group 3: #26 */
	            "(?:PREEMPT\\s+)?" + /* ignore: PREEMPT (optional) */
	            "(.+)"; /* group 4: date */
	        
	        Pattern p = Pattern.compile(PROC_VERSION_REGEX);
	        Matcher m = p.matcher(procVersionStr);

	        if (!m.matches()) {
				LOG.e(TAG, "Regex did not match on /proc/version: " + procVersionStr);
				return "Unavailable";
	        } else if (m.groupCount() < 4) {
				LOG.e(TAG, "Regex match on /proc/version only returned " + m.groupCount()
						+ " groups");
				return "Unavailable";
	        } else {
				return (new StringBuilder(m.group(1)).append("\n").append(
	                   m.group(2)).append(" ").append(m.group(3)).append("\n")
	                    .append(m.group(4))).toString();
	        }
	    } catch (IOException e) {
			LOG.e(TAG,"IO Exception when getting kernel version for Device Info screen",e);

	        return "Unavailable";
	    }
	    
	    
	}
	
	public static String getVersionNumber(Context context)
	{
	    String version = "?";
	    try 
	    {
	        PackageInfo packagInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
	        version = packagInfo.versionName;
	    } 
	    catch (PackageManager.NameNotFoundException e){};
	        
	    return version;
	}
	
	public static String getPackageName(Context context)
	{
	    String version = "?";
	    try 
	    {
	        PackageInfo packagInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
	        version = packagInfo.packageName;
	    } 
	    catch (PackageManager.NameNotFoundException e){};
	        
	    return version;
	}
	
	public static String getCurrentID(Context context)
	{
		SharedPreferences sp = context.getSharedPreferences("loginPrefName", 0);
		String currentId = sp.getString("id", "");
	        
	    return currentId;
	}
	
	public static String getSerialNumber(Context context)
	{
		SharedPreferences sp = context.getSharedPreferences("loginPrefName", 0);
		String serialnumber = sp.getString("serialnumber", "");
	        
	    return serialnumber;
	}
	
	public static String getAndroidVersion() {
		String version = "?";
	    String release = Build.VERSION.RELEASE;
	    int sdkVersion = Build.VERSION.SDK_INT;
	    version = "Android SDK: " + sdkVersion + " (" + release +")";
	    return version;
	}
	
	public static String getBuildNumber() {
		String version = "?";
//		mString.concat("\nFINGERPRINT {" + Build.FINGERPRINT + "}")
		String buildNumber = Build.FINGERPRINT;
		return buildNumber;
	}
	
	
	
	public static void writeToFile(Context context, String log) {
		
		File logFile;
		String filePath;
		
		FileWriter file_writer = null;
		BufferedWriter buff_writer = null;
		PrintWriter print_writer = null;
		
		try {
			filePath = getFilePath(context);
			
			if(filePath.isEmpty()==true) {
				return;
			}
			
			logFile = new File(filePath);
			if(logFile.exists() == true) {
				logFile.delete();
			}
			
			logFile.createNewFile();
			
			file_writer = new FileWriter(logFile, true);
			buff_writer= new BufferedWriter(file_writer);
			print_writer = new PrintWriter(buff_writer, true);
			print_writer.println(log);
			
			if(print_writer.checkError()){
				LOG.d(TAG, "print_write error");	
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try	{
				file_writer.close();
				buff_writer.close();
				print_writer.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void saveLog(Context context) {
		Process process;
		String strLog = "";
		
		LOG.d(TAG, "saveLog filepath:"+SendLogUtil.getFilePath(context));	
		try {
			process = Runtime.getRuntime().exec("logcat -v time -f"+SendLogUtil.getFilePath(context));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void zip(Context context) {
		LOG.d(TAG, "zip(Context context)");
		ZipUtils.zip(SendLogUtil.getFilePath(context), SendLogUtil.getZipFilePath(context));
	}
	
	public static void sendMail(Context context, Activity act, String mail_subject, String mail_body) {
		// Network Check 루닡을 넣자 
		LOG.d(TAG, "sendMail -start-");
		if(isAbleIp(context) == false) {
			Toast.makeText(context, "네트워크 연결이 필요합니다.", Toast.LENGTH_SHORT).show();
			LOG.d(TAG, "isAbleIp(context) is fail");
			return;
		}
		SendMailAsyncTask async = new SendMailAsyncTask();
		async.setContext(context);
		async.setActivity(act);
		async.setProgress("메일 보내는 중");
		async.setMail(mail_subject, mail_body);
		async.execute();
		LOG.d(TAG, "sendMail -end-");
	}
	
	public static void sendMailNoUI(Context context, String mail_subject, String mail_body) {
		// Network Check 루닡을 넣자 
		LOG.d(TAG, "sendMailNoUI -start-");
		if(isAbleIp(context) == false) {
			LOG.d(TAG, "isAbleIp(context) is fail");
			return;
		}
		SendMailThread sendMail = new SendMailThread();
		sendMail.setContext(context);
		sendMail.setMail(mail_subject, mail_body);
		sendMail.start();
		LOG.d(TAG, "sendMailNoUI -end-");
	}
	private static boolean isAbleIp(Context context){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo!=null && netInfo.isConnectedOrConnecting();
	}
}

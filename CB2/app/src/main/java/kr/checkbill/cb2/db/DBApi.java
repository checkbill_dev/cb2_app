package kr.checkbill.cb2.db;

import android.content.Context;
import android.database.Cursor;

import com.checkbill.cbapi.ResultPayment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.ResultPrice;
import kr.checkbill.cb2.util.Util;

/**
 * Created by jinreol on 2017. 4. 10..
 */

public class DBApi {

    private final static String TAG = "DBApi";
    private static String mPaymentId = "";

    public static long addPaymentHistory(Context context, ResultPrice price, ResultPayment resultPayment, String paymentHistoryTranType, String isSendData, String sendFailedReason,
                                           String isUnionPay, String maskingCardData, boolean isKeyIn) {

        BizInfo bizInfo = Util.getBizInfo(context);
        DBHelper mDB = new DBHelper(context);

        price = Util.minusFormat(resultPayment.isApproval, price);

        if (!resultPayment.isApproval) { // 취소 기존 승인건을 취소상태로변경
            mDB.setPaymentCancel(mPaymentId);
            LOG.d(TAG, "isApproval is false");
        }
        else {
            LOG.d(TAG, "isApproval is true");
        }
        String tempCardData;
        if (paymentHistoryTranType.equals("5")
                || paymentHistoryTranType.equals("10")) {
            tempCardData = maskingCardData;
        }
        else {
            tempCardData = Util.cashNumMasking(maskingCardData);
        }
        LOG.d(TAG, "DBApi mPaymentHistoryTranType: " + paymentHistoryTranType);
        LOG.d(TAG, "DBApi resultPayment.option: " + resultPayment.option);
        LOG.d(TAG, "DBApi mPaymentHistoryTranType: " + paymentHistoryTranType);

        long id = mDB.addPaymentHistory (// id 를 제외하고 총 24개
                paymentHistoryTranType,
                tempCardData,
                resultPayment.cardName, //카드이름 //
                resultPayment.issueName, // 카드발급사 //발급
                resultPayment.balance, // 선불카드일시에 잔액
                price.totalPrice + "", //총금액
                price.taxFreePrice + "", // 물품가액
                price.taxPrice + "", // 부가세
                price.servicePrice + "", // 봉사료
                resultPayment.option, // 할부개월
                resultPayment.uniqueNum, // 거래고유번호
                resultPayment.approveNum, //승인번호
                Util.getDateString(resultPayment.date), // 날짜
                isKeyIn==true? "true":"false",
                resultPayment.resultCode + "", //응답코드
                bizInfo.tid,
                bizInfo.bizNum,
                bizInfo.shopName,
                bizInfo.shopCeo,
                bizInfo.shopAddress,
                bizInfo.shopTel,
                bizInfo.userMsg,
                resultPayment.cancelReason,
                resultPayment.isApproval ? PaymentHistory.CANCELED_NOT_CANCEL : PaymentHistory.CANCELED_ON_CANCEL,
//                isApproval ? PaymentHistory.CANCELED_NOT_CANCEL : PaymentHistory.CANCELED_ON_CANCEL,
                isSendData,
                sendFailedReason,
                bizInfo.shopId,
                bizInfo.pin,
                bizInfo.poscode,
                isUnionPay,
                bizInfo.vatValPer,
                bizInfo.serviceValPer,
                bizInfo.vatValInclude,
                bizInfo.userName,
                "0",
                resultPayment.printMessage,
                "0",
                "");
        LOG.d(TAG, "기존 아이디" + mPaymentId);
        LOG.d(TAG, "현재 아이디" + id);
        if (bizInfo.serverSync.equals("0")) {
            Util.clearString(maskingCardData);
        }
        return id;
    }

    public static ResultPrice getApprovePrice(Context context, String paymentId) {
        mPaymentId = paymentId;
        DBHelper mDB = new DBHelper(context);
        ResultPrice price = new ResultPrice();
        PaymentHistory paymentHistory = new PaymentHistory();

        String where = "_id = '" + paymentId + "'";
        Cursor paymentHistoryCursor = mDB.getPaymentHistoryCursorWhere(where);
        while (paymentHistoryCursor.moveToNext()){
            paymentHistory.fetch(paymentHistoryCursor);
        }

        // query
        try {
            price.servicePrice = Long.parseLong(paymentHistory.serviceVal);
            price.taxPrice = Long.parseLong(paymentHistory.vatVal);
            price.taxFreePrice = Long.parseLong(paymentHistory.originVal);
            price.totalPrice = Long.parseLong(paymentHistory.totalVal);
        }
        catch (Exception e) {
            price.servicePrice = 0;
            price.taxPrice = 0;
            price.taxFreePrice = 0;
            price.totalPrice = 0;
        }
        return price;
    }

}

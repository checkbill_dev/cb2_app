package kr.checkbill.cb2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.base.BaseActivity;
import kr.checkbill.cb2.util.LOG;

public class PasswordActivity extends BaseActivity
        implements View.OnClickListener{
    private static final String TAG = "PasswordActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_password);

        registerOnClickListener();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                if (GetText(R.id.edit_password).length() == 6){
                    Intent intent = new Intent();
                    intent.putExtra("Password", GetText(R.id.edit_password));
                    setResult(RESULT_OK, intent);
                    finish();
                    LOG.d(TAG, "GetText(R.id.edit_password): " + GetText(R.id.edit_password));
                }
                else {
                    showToast("비밀번호를 여섯자리로 입력 해주세요.");
                }
                break;
        }
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
    private void registerOnClickListener() {
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }
}

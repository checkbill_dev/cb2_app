package kr.checkbill.cb2.sendlog;

import kr.checkbill.cb2.util.LOG;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class ZipUtils {

	private static final int COMPRESSION_LEVEL = 8;
	private static final int BUFFER_SIZE = 1024 * 2; 
	
	/**
	 * 파일을 압축한다.
	 * @param sourcePath 소스파일패스
	 * @param outputPath 결과파일패스
	 */
	public static void zip(String sourcePath, String outputPath) {
		
		LOG.d("zip","sourcePath:"+sourcePath+" outputPath:"+outputPath);
		
		File sourceFile =  new File(sourcePath); // 소스파일
		if(!sourceFile.isFile() && !sourceFile.isDirectory()) {
			return;
		}
		
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		ZipOutputStream zos = null;
		
		try {
			LOG.d("zip","#1");
			fos = new FileOutputStream(outputPath);
			bos = new BufferedOutputStream(fos);
			zos = new ZipOutputStream(bos);
			zos.setLevel(COMPRESSION_LEVEL); // 압축 레벨 - 최대 압축률은 9, 디폴트 8
			
			zipEntry(sourceFile, sourcePath, zos);  // Zip 파일 생성
			zos.finish();
			LOG.d("zip","#2");
		}
		catch(Exception e) {
			
		}
		finally {
			if(zos != null) {
				try {
					LOG.d("zip","#3");
					zos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if(bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if(fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}
	
	/**
	 * 압축 파일 생성 
	 * @param sourceFile 소스의 File 객체
	 * @param sourcePath 파일 경로
	 * @param zos         ZipOutputStream 객체 
	 * @throws Exception 오동작시
	 */
	private static void zipEntry(File sourceFile, String sourcePath, ZipOutputStream zos) throws Exception {
		
		if(sourceFile.isDirectory()) {
			if(sourceFile.getName().equalsIgnoreCase(".metadata")) { // .metadata 디렉토리 return
				return;
			}
			
			File[] fileArray = sourceFile.listFiles(); // 재귀 호출
			for(int i=0; i<fileArray.length; i++) {
				zipEntry(fileArray[i], sourcePath, zos);
			}
		}
		else {
			BufferedInputStream bis = null;
			
			try {
				String sFilePath = sourceFile.getPath();
				StringTokenizer tok = new StringTokenizer(sFilePath, "/");
				
				int tok_len = tok.countTokens();
				String zipEntryName = tok.toString();
				
				while(tok_len !=0) {
					tok_len--;
					zipEntryName = tok.nextToken();
				}
				bis = new BufferedInputStream(new FileInputStream(sourceFile));
				
				ZipEntry zentry = new ZipEntry(zipEntryName);
				zentry.setTime(sourceFile.lastModified());
				zos.putNextEntry(zentry);
				
				byte[] buffer = new byte[BUFFER_SIZE];
				int cnt = 0;
				
				while ((cnt = bis.read(buffer, 0, BUFFER_SIZE)) != -1) {  
                    zos.write(buffer, 0, cnt);  
                }  
                zos.closeEntry();  
			} finally {
				if(bis != null) {
					bis.close();
				}
			}
		}
	}
}

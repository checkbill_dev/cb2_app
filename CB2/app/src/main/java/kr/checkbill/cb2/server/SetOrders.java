package kr.checkbill.cb2.server;

import java.util.ArrayList;

/**
 * Created by kwonhyeokjin on 2017. 2. 20..
 */

public class SetOrders {
    String order_result_num;
    ArrayList<SetOrdersData> data;
    public SetOrders(String order_result_num, ArrayList<SetOrdersData> data){
        this.order_result_num = order_result_num;
        this.data = data;
    }
}

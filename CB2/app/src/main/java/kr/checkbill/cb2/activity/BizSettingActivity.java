package kr.checkbill.cb2.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.base.BaseActivity;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.util.Util;

public class BizSettingActivity extends BaseActivity
        implements View.OnClickListener, View.OnFocusChangeListener{
    private static final String TAG = "BizSettingActivity";
    private EditText edit_user_name, edit_vat_val_per, edit_service_val_per, edit_memo;
    private Switch sw_server_sync, sw_vat_val_include;

    private DBHelper mDB;
    private BizInfo bizInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biz_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        edit_user_name = (EditText) findViewById(R.id.edit_user_name);
        edit_vat_val_per = (EditText) findViewById(R.id.edit_vat_val_per);
        edit_service_val_per = (EditText) findViewById(R.id.edit_service_val_per);
        edit_memo = (EditText) findViewById(R.id.edit_memo);
        sw_server_sync = (Switch) findViewById(R.id.sw_server_sync);
        sw_vat_val_include = (Switch) findViewById(R.id.sw_vat_val_include);

        registerOnClickListener();
        registerOnFocusChangeListener();

        mDB = new DBHelper(this);
        bizInfo = new BizInfo();

        String tid = getIntent().getStringExtra("tid");
        bizInfo = Util.getBizInfo(this, tid);

        SetText(R.id.edit_user_name, bizInfo.userName);
        SetText(R.id.edit_vat_val_per, bizInfo.vatValPer);
        SetText(R.id.edit_service_val_per, bizInfo.serviceValPer);

        if (bizInfo.serverSync.equals("0")){
            sw_server_sync.setChecked(false);
        }
        else if (bizInfo.serverSync.equals("1")) {
            sw_server_sync.setChecked(true);
        }


        if (bizInfo.vatValInclude.equals("0")){
            sw_vat_val_include.setChecked(false);
        }
        else if (bizInfo.vatValInclude.equals("1")) {
            sw_vat_val_include.setChecked(true);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_user_name_clear:
                SetText(R.id.edit_user_name, "");
                break;
            case R.id.btn_vat_val_per_clear:
                SetText(R.id.edit_vat_val_per, "");
                break;
            case R.id.btn_service_val_per_clear:
                SetText(R.id.edit_service_val_per, "");
                break;
            case R.id.btn_memo_clear:
                SetText(R.id.edit_memo, "");
                break;
            case R.id.btn_ok:
                String result = mDB.setBizInfoSetting(bizInfo.tid, GetText(R.id.edit_user_name), GetText(R.id.edit_vat_val_per),
                        GetText(R.id.edit_service_val_per), sw_server_sync.isChecked()? "1" : "0", sw_vat_val_include.isChecked()? "1" : "0", edit_memo.getText().toString());
                if (result.equals("success")){
                    showToast("저장되었습니다.");
                    finish();
                }
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.edit_user_name:
                if(hasFocus){//포커스를 얻을때
                    edit_user_name.setBackgroundResource(R.drawable.blue_radius);
                }
                else{
                    edit_user_name.setBackgroundResource(R.drawable.gray_radius);
                }
                break;
            case R.id.edit_vat_val_per:
                if(hasFocus){//포커스를 얻을때
                    edit_vat_val_per.setBackgroundResource(R.drawable.blue_radius);
                }
                else{
                    edit_vat_val_per.setBackgroundResource(R.drawable.gray_radius);
                }
                break;
            case R.id.edit_service_val_per:
                if(hasFocus){//포커스를 얻을때
                    edit_service_val_per.setBackgroundResource(R.drawable.blue_radius);
                }
                else{
                    edit_service_val_per.setBackgroundResource(R.drawable.gray_radius);
                }
                break;
            case R.id.edit_memo:
                if(hasFocus){//포커스를 얻을때
                    edit_memo.setBackgroundResource(R.drawable.blue_radius);
                }
                else{
                    edit_memo.setBackgroundResource(R.drawable.gray_radius);
                }
                break;
        }
    }
    private void registerOnClickListener() {
        (findViewById(R.id.img_back)).setOnClickListener(this);
        (findViewById(R.id.btn_user_name_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_vat_val_per_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_service_val_per_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_memo_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }
    private void registerOnFocusChangeListener() {
        (findViewById(R.id.edit_user_name)).setOnFocusChangeListener(this);
        (findViewById(R.id.edit_vat_val_per)).setOnFocusChangeListener(this);
        (findViewById(R.id.edit_service_val_per)).setOnFocusChangeListener(this);
        (findViewById(R.id.edit_memo)).setOnFocusChangeListener(this);
    }
}

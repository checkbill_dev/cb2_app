package kr.checkbill.cb2.db.table;

import android.database.Cursor;

import java.io.Serializable;

/**
 * Created by kwonhyeokjin on 2017. 1. 12..
 */

/*
CREATE TABLE PAYMENT_HISTORY(
    _id                 integer PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
    TRAN_TYPE           integer,
    CARD_NUM            text,
    CARD_NAME           text,
    CARD_COMPANY        text,
    BALANCE             integer,
    TOTAL_VAL           integer,
    ORIGIN_VAL          integer,
    VAT_VAL             integer,
    SERVICE_VAL         integer,
    MONTH_VAL           integer,
    APPROVAL_NUM_ORG    text,
    APPROVAL_NUM        text,
    APPROVAL_DATE       text,
    IS_KEY_IN           text,
    RESULT_CODE         text,
    SHOP_TID            text,
    SHOP_BIZ_NUM        text,
    SHOP_NAME           text,
    SHOP_CEO            text,
    SHOP_ADDRESS        text,
    SHOP_TEL            text,
    USER_MSG            text,
    CANCEL_REASON       integer,
    CANCELED            integer
);
*/
public class PaymentHistory implements Serializable {
    public static final String TABLE_NAME="PAYMENT_HISTORY";

    public static final String _ID="_id";
    public static final String TRAN_TYPE="TRAN_TYPE"; //1:카드 2:현금영수증없는 현금결제 3:자진발급 4:소득공제 5:지출증빙
    public static final String CARD_NUM="CARD_NUM"; // 카드번호 또는 현금영수증번호
    public static final String CARD_NAME="CARD_NAME"; // 카드이름 , 현금영수증일경우에는 null
    public static final String CARD_COMPANY = "CARD_COMPANY"; // 카드사 , 현금영수증일경우에는 null
    public static final String BALANCE = "BALANCE"; // 선불카드일시 잔액을 저장
    public static final String TOTAL_VAL="TOTAL_VAL"; // 총금액
    public static final String ORIGIN_VAL="ORIGIN_VAL"; // 물품가액
    public static final String VAT_VAL="VAT_VAL"; // 부가세
    public static final String SERVICE_VAL="SERVICE_VAL"; // 봉사료
    public static final String MONTH_VAL="MONTH_VAL"; // 할부개월, 현금일경우 0개월
    public static final String APPROVAL_NUM_ORG="APPROVAL_NUM_ORG"; // 거래고유번호
    public static final String APPROVAL_NUM="APPROVAL_NUM"; // 승인번호
    public static final String APPROVAL_DATE="APPROVAL_DATE"; // 승인날짜 yyyy-mm-dd hh:mm:ss
    public static final String IS_KEY_IN ="IS_KEY_IN"; //현금 영수증 카드 이용시 0 , 현금영수증 번호 직접 입력시 1
    public static final String RESULT_CODE="RESULT_CODE"; // 응답코드
    public static final String SHOP_TID="SHOP_TID"; // 단말기번호
    public static final String SHOP_BIZ_NUM="SHOP_BIZ_NUM"; // 사업자등록번호
    public static final String SHOP_NAME="SHOP_NAME"; // 가맹점명
    public static final String SHOP_CEO="SHOP_CEO"; // 대표자명
    public static final String SHOP_ADDRRESS="SHOP_ADDRESS"; // 가맹점주소
    public static final String SHOP_TEL="SHOP_TEL"; // 가맹점 전화번호
    public static final String USER_MSG="USER_MSG"; // 영수증에 찍히는 메세지
    public static final String CANCEL_REASON="CANCEL_REASON"; //취소사유코드 "1":거래취소 "2":오류발급 "3":기타
    public static final String CANCELED="CANCELED"; //취소여부 defaulf:0 취소됫을경우:1
    public static final String SEND_DATA="SEND_DATA"; // 결제내역 서버에 전송성공여부 0:결제내역 전송됨 1:결제내역 전송안됨 -1:서버에 전송안함
    public static final String SEND_FAILED_REASON = "SEND_FAILED_REASON"; // 결제내역 서버에 전송 실패시 실패이유
    public static final String SHOP_ID = "SHOP_ID"; // 결제내역을 서버에 전송한 후 응답받은 코드
    public static final String PIN = "PIN"; // 결제내역을 서버에 전송한 후 응답받은 코드
    public static final String POSCODE = "POSCODE"; // 결제내역을 서버에 전송한 후 응답받은 코드
    public static final String EULLYEON = "EULLYEON"; // 은련카드인제 여부 0:은련카드아님 1:은련카드임
    public static final String VAT_VAL_PER="VAT_VAL_PER"; // 부가세 세율
    public static final String SERVICE_VAL_PER="SERVICE_VAL_PER"; // 봉사료 세율
    public static final String VAT_VAL_INCLUDE="VAT_VAL_INCLUDE"; // 입력금액에 부가세 포함여부 0:부가세포함 1:부가세 별도
    public static final String USER_NAME="USER_NAME"; // 사용자이름
    public static final String IS_SERVER_DATA = "IS_SERVER_DATA"; //서버에서 받은 데이터 인지 0:로컬데이터임 1:서버에서 받은데이터임
    public static final String PRINT_MESSAGE = "PRINT_MESSAGE"; //프린트메세지
    public static final String IS_MASKED = "IS_MASKED"; // 80일이 지나서 마스킹이 되었는지 0:마스킹안됨 1:마스킹됨
    public static final String APPROVAL_NUM_DEF = "APPROVAL_NUM_DEF"; // 결제시와 취소시 승인번호가 다른경우가 있어서 취소시에 결제의 승인번호를 저장

    public static final String TRANTYPE_CASH_EXPENSE_EVIDENCE ="2"; // 지출증빙
    public static final String TRANTYPE_CASH_VOLUNTARY ="3"; // 자진발급
    public static final String TRANTYPE_CASH_INCOME_DEDUCTION ="1"; // 소득공제
    public static final String TRANTYPE_CASH_DEFAULT ="4"; // 현금영수증없는 현금결제
    public static final String TRANTYPE_CREDIT_CARD ="5"; // 카드
    public static final String TRANTYPE_CASH_EXPENSE_EVIDENCE_CANCEL ="7"; // 지출증빙 취소
    public static final String TRANTYPE_CASH_VOLUNTARY_CANCEL ="8"; // 자진발급 취소
    public static final String TRANTYPE_CASH_INCOME_DEDUCTION_CANCEL ="6"; // 소득공제 취소
    public static final String TRANTYPE_CASH_DEFAULT_CANCEL ="9"; // 현금영수증없는 현금결제 취소
    public static final String TRANTYPE_CREDIT_CARD_CANCEL ="10"; // 카드 취소

    //승인된 레코드
    public static final String TRANTYPE_CASH_EXPENSE_EVIDENCE_SHOW_OO ="지출증빙 승인"; // 지출증빙
    public static final String TRANTYPE_CASH_VOLUNTARY_SHOW_OO ="자진발급 승인"; // 자진발급
    public static final String TRANTYPE_CASH_INCOME_DEDUCTION_SHOW_OO ="소득공제 승인"; // 소득공제
    public static final String TRANTYPE_CASH_DEFAULT_SHOW_OO ="일반영수증 승인"; // 현금영수증없는 현금결제
    public static final String TRANTYPE_CREDIT_CARD_SHOW_OO ="신용 승인"; // 카드

    //승인후 취소된 레코드
    public static final String TRANTYPE_CASH_EXPENSE_EVIDENCE_CANCEL_SHOW_OX ="지출증빙 승인(취소)";
    public static final String TRANTYPE_CASH_VOLUNTARY_CANCEL_SHOW_OX ="자진발급 승인(취소)";
    public static final String TRANTYPE_CASH_INCOME_DEDUCTION_CANCEL_SHOW_OX ="소득공제 승인(취소)";
    public static final String TRANTYPE_CASH_DEFAULT_CANCEL_SHOW_OX ="일반영수증 승인(취소)";
    public static final String TRANTYPE_CREDIT_CARD_CANCEL_SHOW_OX ="신용 승인(취소)";

    //취소되어 새롭게 생긴 레코드
    public static final String TRANTYPE_CASH_EXPENSE_EVIDENCE_CANCEL_SHOW_XX ="지출증빙 취소";
    public static final String TRANTYPE_CASH_VOLUNTARY_CANCEL_SHOW_XX ="자진발급 취소";
    public static final String TRANTYPE_CASH_INCOME_DEDUCTION_CANCEL_SHOW_XX ="소득공제 취소";
    public static final String TRANTYPE_CASH_DEFAULT_CANCEL_SHOW_XX ="일반영수증 취소";
    public static final String TRANTYPE_CREDIT_CARD_CANCEL_SHOW_XX ="신용 취소";

    public static final String TITLE_CARD = "매출전표(신용 승인)";
    public static final String TITLE_CARD_CANCEL = "매출전표(신용 취소)";
    public static final String TITLE_CASH = "매출전표(현금 승인)";
    public static final String TITLE_CASH_CANCEL = "매출전표(현금 취소)";


    /*취소여부 디폴트값*/
    public static final String CANCELED_NOT_CANCEL = "0"; // 0 : 취소안됨
    public static final String CANCELED_ON_CANCEL = "1"; // 1= 취소됨

    /*결제내역 전송여부 디폴트값*/
    public static final String SEND_DATA_SUCCESS = "0"; // 0 : 결제내역 전송됨
    public static final String SEND_DATA_FAILED = "1"; // 1 : 결제내역 전송안됨

    /*결제내역 전송여부 디폴트값*/
    public static final String EULLYEON_NOT = "0"; // 0 : 은련카드아님
    public static final String EULLYEON_ON = "1"; // 1 : 은련카드임


    public String _id;
    public String tranType;
    public String cardNum;
    public String cardName;
    public String cardCompany;
    public String balance;
    public String totalVal;
    public String originVal;
    public String vatVal;
    public String serviceVal;
    public String monthVal;
    public String approvalNumOrg;
    public String approvalNum;
    public String approvalDate;
    public String isKeyIn;
    public String resultCode;
    public String shopTid;
    public String shopBizNum;
    public String shopName;
    public String shopCeo;
    public String shopAddress;
    public String shopTel;
    public String userMsg;
    public String cancelReason;
    public String canceled;
    public String sendData;
    public String sendFailedReason;
    public String shopId;
    public String pin;
    public String poscode;
    public String eullyeon;
    public String vatValPer;
    public String serviceValPer;
    public String vatValInclude;
    public String userName;
    public String isServerData;
    public String printMessage;
    public String isMasked;
    public String approvalNumDef;

    /**
     * 전달받은 커서를 기반으로 결제내역을 추출하는 메소드
     */
    public void fetch(Cursor c){

        int idx_id = c.getColumnIndex(PaymentHistory._ID);
        int idxTranType = c.getColumnIndex(PaymentHistory.TRAN_TYPE);
        int idxCardNum= c.getColumnIndex(PaymentHistory.CARD_NUM);
        int idxCardName = c.getColumnIndex(PaymentHistory.CARD_NAME);
        int idxCardCompany = c.getColumnIndex(PaymentHistory.CARD_COMPANY);
        int idxBalance = c.getColumnIndex(PaymentHistory.BALANCE);
        int idxTotalVal = c.getColumnIndex(PaymentHistory.TOTAL_VAL);
        int idxOriginVal = c.getColumnIndex(PaymentHistory.ORIGIN_VAL);
        int idxVatVal = c.getColumnIndex(PaymentHistory.VAT_VAL);
        int idxServiceVal = c.getColumnIndex(PaymentHistory.SERVICE_VAL);
        int idxMonthVal = c.getColumnIndex(PaymentHistory.MONTH_VAL);
        int idxApprovalNumOrg = c.getColumnIndex(PaymentHistory.APPROVAL_NUM_ORG);
        int idxApprovalNum = c.getColumnIndex(PaymentHistory.APPROVAL_NUM);
        int idxApprovalDate = c.getColumnIndex(PaymentHistory.APPROVAL_DATE);
        int idxIsKeyIn = c.getColumnIndex(PaymentHistory.IS_KEY_IN);
        int idxResultCode = c.getColumnIndex(PaymentHistory.RESULT_CODE);
        int idxShopTid = c.getColumnIndex(PaymentHistory.SHOP_TID);
        int idxShopBizNum = c.getColumnIndex(PaymentHistory.SHOP_BIZ_NUM);
        int idxShopName = c.getColumnIndex(PaymentHistory.SHOP_NAME);
        int idxShopCeo = c.getColumnIndex(PaymentHistory.SHOP_CEO);
        int idxShopAddress = c.getColumnIndex(PaymentHistory.SHOP_ADDRRESS);
        int idxShopTel = c.getColumnIndex(PaymentHistory.SHOP_TEL);
        int idxUserMsg = c.getColumnIndex(PaymentHistory.USER_MSG);
        int idxCancelReason = c.getColumnIndex(PaymentHistory.CANCEL_REASON);
        int idxCanceled = c.getColumnIndex(PaymentHistory.CANCELED);
        int idxSendData = c.getColumnIndex(PaymentHistory.SEND_DATA);
        int idxSendFailedReason = c.getColumnIndex(PaymentHistory.SEND_FAILED_REASON);
        int idxShopId = c.getColumnIndex(PaymentHistory.SHOP_ID);
        int idxPin = c.getColumnIndex(PaymentHistory.PIN);
        int idxPoscode = c.getColumnIndex(PaymentHistory.POSCODE);
        int idxEullyeon = c.getColumnIndex(PaymentHistory.EULLYEON);
        int idxVatValPer = c.getColumnIndex(PaymentHistory.VAT_VAL_PER);
        int idxServiceValPer = c.getColumnIndex(PaymentHistory.SERVICE_VAL_PER);
        int idxVatValInclude = c.getColumnIndex(PaymentHistory.VAT_VAL_INCLUDE);
        int idxUserName = c.getColumnIndex(PaymentHistory.USER_NAME);
        int idxIsServerData = c.getColumnIndex(PaymentHistory.IS_SERVER_DATA);
        int idxPrintMessage = c.getColumnIndex(PaymentHistory.PRINT_MESSAGE);
        int idxIsMasked = c.getColumnIndex(PaymentHistory.IS_MASKED);
        int idxApprovalNumDef = c.getColumnIndex(PaymentHistory.APPROVAL_NUM_DEF);

        _id = c.getString(idx_id);
        tranType = c.getString(idxTranType);
        cardNum = c.getString(idxCardNum);
        cardName = c.getString(idxCardName);
        cardCompany = c.getString(idxCardCompany);
        balance = c.getString(idxBalance);
        totalVal = c.getString(idxTotalVal);
        originVal = c.getString(idxOriginVal);
        vatVal = c.getString(idxVatVal);
        serviceVal = c.getString(idxServiceVal);
        monthVal = c.getString(idxMonthVal);
        approvalNumOrg = c.getString(idxApprovalNumOrg);
        approvalNum = c.getString(idxApprovalNum);
        approvalDate = c.getString(idxApprovalDate);
        isKeyIn = c.getString(idxIsKeyIn);
        resultCode = c.getString(idxResultCode);
        shopTid = c.getString(idxShopTid);
        shopBizNum = c.getString(idxShopBizNum);
        shopName = c.getString(idxShopName);
        shopCeo = c.getString(idxShopCeo);
        shopAddress = c.getString(idxShopAddress);
        shopTel = c.getString(idxShopTel);
        userMsg = c.getString(idxUserMsg);
        cancelReason = c.getString(idxCancelReason);
        canceled = c.getString(idxCanceled);
        sendData = c.getString(idxSendData);
        sendFailedReason = c.getString(idxSendFailedReason);
        shopId = c.getString(idxShopId);
        pin = c.getString(idxPin);
        poscode = c.getString(idxPoscode);
        eullyeon = c.getString(idxEullyeon);
        vatValPer = c.getString(idxVatValPer);
        serviceValPer = c.getString(idxServiceValPer);
        vatValInclude = c.getString(idxVatValInclude);
        userName = c.getString(idxUserName);
        isServerData = c.getString(idxIsServerData);
        printMessage = c.getString(idxPrintMessage);
        isMasked = c.getString(idxIsMasked);
        approvalNumDef = c.getString(idxApprovalNumDef);
    }
}
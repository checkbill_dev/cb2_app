package kr.checkbill.cb2.server;

/**
 * Created by kwonhyeokjin on 2017. 3. 22..
 */

public class GetOrdersResponseData {
    public String approval_num_org;
    public String tid;
    public String biz_num;
    public String shop_id;
    public String pin;
    public String poscode;
    public String tran_type;
    public String card_num;
    public String card_name;
    public String card_company;
    public String balance;
    public String total_val;
    public String origin_val;
    public String vat_val;
    public String service_val;
    public String month_val;
    public String approval_num;
    public String approval_date;
    public String cancel_reason;
    public String is_key_in;
    public String canceled;
    public String rate_service;
    public String rate_vat;
    public String flag_vat;
    public String is_union_pay;
    public String staff_nm;
    public String approval_num_def;
    public String print_message;
}

package kr.checkbill.cb2.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.base.BaseActivity;
import kr.checkbill.cb2.db.DBHelper;

public class PasswordChangeActivity extends BaseActivity
        implements View.OnClickListener, TextWatcher, View.OnFocusChangeListener{
    private EditText edit_now_password, edit_new_password, edit_new_password_confirm;
    private Button btn_now_password_clear, btn_now_password_clear_tint;
    private Button btn_ok, btn_ok_tint;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        edit_now_password = (EditText) findViewById(R.id.edit_now_password);
        edit_new_password = (EditText) findViewById(R.id.edit_new_password);
        edit_new_password_confirm = (EditText) findViewById(R.id.edit_new_password_confirm);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok_tint = (Button) findViewById(R.id.btn_ok_tint);

        edit_now_password.requestFocus();
        edit_now_password.addTextChangedListener(this);
        edit_new_password.setFocusableInTouchMode(false);
        edit_new_password_confirm.setFocusableInTouchMode(false);

        registerOnClickListener();
        registerTextWatcher();
        registerOnFocusChangeListener();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_now_password_clear:
                SetText(R.id.edit_now_password, "");
                break;
            case R.id.btn_new_password_clear:
                SetText(R.id.edit_new_password, "");
                break;
            case R.id.btn_new_password_confirm_clear:
                SetText(R.id.edit_new_password_confirm, "");
                break;
            case R.id.btn_ok:
                DBHelper mDB = new DBHelper(this);
                if(edit_now_password.length()>=1){// edit_now_password 가 포커스가 가능할때 즉, 현재비밀번호 입력가능상태일때
                    if(mDB.getSystemPassword().equals(GetText(R.id.edit_now_password))){//현재 비밀번호일치
                        /*현재비밀번호는 비활성화로 새비밀번호 두개는 활성화 상태로 변경*/
                        edit_now_password.setFocusableInTouchMode(false);
                        edit_new_password.setFocusableInTouchMode(true);
                        edit_new_password_confirm.setFocusableInTouchMode(true);
                        SetText(R.id.edit_now_password, "");
                        edit_now_password.setHint("-");
                        btn_now_password_clear.setVisibility(View.GONE);
                        btn_now_password_clear_tint.setVisibility(View.VISIBLE);
                        btn_ok.setVisibility(View.GONE);
                        btn_ok_tint.setVisibility(View.VISIBLE);
                        edit_new_password.requestFocus();
                        edit_new_password.setHint("새 비밀번호를 입력 해주세요.");
                        edit_new_password_confirm.setHint("새 비밀번호를 재입력 해주세요.");
                        SetText(R.id.btn_ok, "비밀번호 변경");
                        SetText(R.id.btn_ok_tint, "비밀번호 변경");
                    }
                    else {
                        showAlertDialog("알림", new String[]{"현재 비밀번호가 일치하지 않습니다."}, true,
                                true, false, false, "확인", null, null, null, null, null);
                    }
                }
                else if(edit_now_password.getHint().toString().equals("-")){
                    //새 비밀번호 두개가 활성화상태일때,, 즉, 현재비밀번호가 완료된 상태이고 새로운 비밀번호를 변경하려할때
                    if(GetText(R.id.edit_new_password).equals(GetText(R.id.edit_new_password_confirm))){
                        String result = mDB.setSystemPassword(GetText(R.id.edit_new_password));
                        View.OnClickListener event = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        };
                        if(result.equals("success")){
                            showAlertDialog("알림", new String[]{"비밀번호가 변경되었습니다."}, true,
                                    true, false, false, "확인", null, null, event, null, null);
                        }
                        else {
                            showAlertDialog("알림", new String[]{"비밀번호에 실패하였습니다."}, true,
                                    true, false, false, "확인", null, null, event, null, null);
                        }
                    }
                    else {
                        showAlertDialog("알림", new String[]{"비밀번호에 실패하였습니다."}, true,
                                true, false, false, "확인", null, null, null, null, null);
                    }
                }
                break;
        }
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (edit_now_password.hasFocus()) {
            if(GetText(R.id.edit_now_password).length()>=1){
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }
        }
        else if (edit_new_password.hasFocus()) {
            if(GetText(R.id.edit_new_password).length()>=1
                    && GetText(R.id.edit_new_password_confirm).toString().length()>=1){
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }
        }
        else if (edit_new_password_confirm.hasFocus()) {
            if(GetText(R.id.edit_new_password).length()>=1
                    && GetText(R.id.edit_new_password_confirm).length()>=1){
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }
        }
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            v.setBackgroundResource(R.drawable.blue_radius);
        }
        else {
            v.setBackgroundResource(R.drawable.gray_radius);
        }
    }
    private void registerOnClickListener(){
        (findViewById(R.id.img_back)).setOnClickListener(this);
        (findViewById(R.id.btn_now_password_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_new_password_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_new_password_confirm_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }
    private void registerTextWatcher() {
        edit_now_password.addTextChangedListener(this);
        edit_new_password.addTextChangedListener(this);
        edit_new_password_confirm.addTextChangedListener(this);
    }
    private void registerOnFocusChangeListener() {
        edit_now_password.addTextChangedListener(this);
        edit_new_password.addTextChangedListener(this);
        edit_new_password_confirm.addTextChangedListener(this);
    }

    /**
     * 안쓰는 메소드들
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void afterTextChanged(Editable s) {
    }
}

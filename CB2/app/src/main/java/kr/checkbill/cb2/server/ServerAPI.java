package kr.checkbill.cb2.server;

import android.content.Context;
import android.database.Cursor;

import com.checkbill.cbapi.ResultPayment;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.ResultPrice;
import kr.checkbill.cb2.util.Util;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by jinreol on 2017. 4. 10..
 */

public class ServerAPI {

    private final static String TAG = "ServerAPI";

    public static void sendPaymentHistory(Context context, String tranType,  String paymentId, ResultPrice price, ResultPayment resultPayment, String maskingCardData, String isUnionPay, boolean isKeyIn, Callback callback) {
        BizInfo bizInfo = Util.getBizInfo(context);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(RetrofitApiService.API_URL).build();
        RetrofitApiService apiService = retrofit.create(RetrofitApiService.class);

        Gson gson = new Gson();
        ArrayList<SetOrdersData> data = new ArrayList<SetOrdersData>();
        data.add(new SetOrdersData(resultPayment.uniqueNum, bizInfo.tid, bizInfo.bizNum, tranType, // 트랜타입 수정해야함
                bizInfo.shopId, bizInfo.pin, bizInfo.poscode,
                isKeyIn ? Util.cashNumMasking(maskingCardData) : maskingCardData, resultPayment.cardName, resultPayment.issueName, resultPayment.balance,
                String.valueOf(price.totalPrice), String.valueOf(price.taxFreePrice), String.valueOf(price.taxPrice), String.valueOf(price.servicePrice),
                resultPayment.option, resultPayment.approveNum, Util.getDateString(resultPayment.date), resultPayment.cancelReason,
                isKeyIn ? "Y" : "N", resultPayment.isApproval ? PaymentHistory.CANCELED_NOT_CANCEL : PaymentHistory.CANCELED_ON_CANCEL,
                bizInfo.serviceValPer, bizInfo.vatValPer,
                bizInfo.vatValInclude.equals("0") ? "Y" : "N", isUnionPay.equals("1") ? "Y" : "N", bizInfo.userName,
                resultPayment.isApproval ? "" : getApprovalNum(context, paymentId), resultPayment.printMessage)
        );

        String body = gson.toJson(new SetOrders("1", data));

        LOG.i(TAG, "JSON/loginPos --- " + body);

        Call<ResponseBody> call = apiService.setOrdersApi(body);
        call.enqueue(callback);

        Util.clearString(maskingCardData);
    }

    public static void receivePaymentHistory(String shopId, String poscode, String ts, Callback callback) {
        Gson gson = new Gson();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(RetrofitApiService.API_URL).build();
        RetrofitApiService apiService = retrofit.create(RetrofitApiService.class);

        String body;
        body = gson.toJson(new GetOrders(shopId, poscode, ts));

        LOG.i(TAG, "JSON /loginPos --- " + body);

        Call<GetOrdersResponse> call = apiService.getOrdersApi(body);

        call.enqueue(callback);

    }

    public static void registerBizInfo(String shopId, String bizNum, String shopName,
                                       String shopTel, String shopCeo, String shopAddress, String tid, String saveFlag, Callback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(RetrofitApiService.API_URL).build();
        RetrofitApiService apiService = retrofit.create(RetrofitApiService.class);
        Gson gson = new Gson();

        String body;
        body = gson.toJson(new ReaderId(shopId, bizNum, shopName,
                shopTel, shopCeo, shopAddress, tid, saveFlag));
        LOG.d(TAG,body);
        Call<ReaderIdResponse> call = apiService.setReaderIdApi(body);
        call.enqueue(callback);

    }
    private static String getApprovalNum(Context context, String paymentId) {
        DBHelper mDB = new DBHelper(context);
        PaymentHistory paymentHistory = new PaymentHistory();

        String where = "_id = '" + paymentId + "'";
        Cursor paymentHistoryCursor = mDB.getPaymentHistoryCursorWhere(where);
        while (paymentHistoryCursor.moveToNext()){
            paymentHistory.fetch(paymentHistoryCursor);
        }
        LOG.d(TAG, "_id: " + paymentId);
        LOG.d(TAG, "approvalDate: " + paymentHistory.approvalDate);
        LOG.d(TAG, "approvalNum: " + paymentHistory.approvalNum);
        return paymentHistory.approvalNum;
    }
}

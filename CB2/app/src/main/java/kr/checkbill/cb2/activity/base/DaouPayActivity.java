package kr.checkbill.cb2.activity.base;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;

import com.checkbill.cbapi.ResultPayment;
import com.checkbill.cbdaouapi.CBDaouApi;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import kr.checkbill.cb2.db.DBApi;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.server.GetOrdersResponse;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.ResultPrice;
import kr.checkbill.cb2.util.Util;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * 다우데이타 결제용 Activity
 * 부가세 등등 계산
 */
public class DaouPayActivity extends DaouActivity {

    private static final String TAG = "DaouPayActivity";

    private int fallback = 0;             //
    private String maskingCardData = "";  //
    private ResultPayment mResultPayment = null; //
    private String mPaymentHistoryTranType = ""; //
    private boolean mIsExternal = false; // 초기화
    private ResultPrice mPrice = null;
    private String mIsUnionPay = "0";
    private boolean mIsKeyIn = false;
    private String mPaymentId = "";
    private boolean mIsReSend;
    private String mApprovalNumOrg;
    private boolean mIsPaymentListActivity = false;
    private boolean mIsSendServerState = true; // true: 서버전송상태 false:로부터 전송받는 상태이다. 서버의 대한 응답이 어떤응답인지 분기하기위해서 사용한다..

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * 내부용 카드결제 함수
     * @param password        패스워드(은련카드 결제시)
     * @param bmpSign         싸인데이터(5만원 초과 결제시)
     * @param strInputPrice   입력 결제 금액
     * @param strServicePrice 봉사료
     * @param month           할부개월수(0:일시불 최대 11개월까지만 가능)
     * @param fallback        fallback 코드
     */
    public void DAOU_Credit(String paymentType, String password, Bitmap bmpSign, String strInputPrice, String strServicePrice, String month, int fallback) {
        mIsExternal = false;
        mPrice = Util.calculatePrice(this, strInputPrice, strServicePrice, true);
        mPaymentHistoryTranType = paymentType;
        payment(paymentType, false, password, bmpSign, mPrice, month, fallback, null, null, null, null, null);
    }

    /**
     *
     * @param isExternal      true:외부호출  false:내부호출
     * @param paymentType     결제타입
     * @param password        패스워드(은련카드 결제시)
     * @param bmpSign         싸인데이터(5만원 초과 결제시)
     * @param strTotalPrice   외부호출시 취소금액
     * @param paymentId       내부호출시 ID
     * @param month           할부개월수(0:일시불 최대 11개월까지만 가능)
     * @param approvalDate    승인번호
     * @param approvalNumber  승인날짜(yyyyMMdd)
     * @param fallback        fallback 코드
     */
    public void DAOU_Credit_Cancel(boolean isExternal, String paymentType, String password, Bitmap bmpSign, String strTotalPrice, String paymentId, String month, String approvalDate, String approvalNumber, int fallback) {
        mIsExternal = isExternal;
        mPaymentId = paymentId;


        if (isExternal) {
            mPrice = new ResultPrice();
            try {
                mPrice.totalPrice = Long.parseLong(strTotalPrice);
            }
            catch (Exception e) {
                mPrice.servicePrice = 0;
            }
        }
        else {
            mPrice = DBApi.getApprovePrice(this, paymentId);
        }
        mPaymentHistoryTranType = paymentType;
        payment(paymentType, false, password, bmpSign, mPrice, month, fallback, approvalNumber, approvalDate, null, null, null);
    }

    /**
     * 내부용 현금 결제 함수
     * @param strInputPrice    입력 결제 금액
     * @param strServicePrice  봉사료
     * @param option           거래구분 "1":소득공제 "2":지출증빙 "3":자진발급
     * @param isKeyIn          true:직접입력시 false:카드사용시
     * @param cashNum          전화번호 또는 카드번호
     */
    public void DAOU_Cash(String paymentType, String strInputPrice, String strServicePrice, String option, boolean isKeyIn, String cashNum) {
        mIsExternal = false;
        mPrice = Util.calculatePrice(this, strInputPrice, strServicePrice, true);
        mPaymentHistoryTranType = paymentType;
        maskingCardData = cashNum;
        payment(paymentType, isKeyIn, null, null, mPrice, null, fallback, null, null, cashNum, null, option);
    }

    /**
     *
     * @param isExternal       true:외부호출  false:내부호출
     * @param paymentType      결제타입
     * @param strTotalPrice    외부호출시 취소금액
     * @param paymentId        내부호출시 ID
     * @param option           거래취소 "A":소득공제 "B":지출증빙 "E":자진발급
     * @param isKeyIn          직접 입력시 true, 현금 영수증 카드 이용시 false
     * @param cancelReason     취소사유코드 "1":거래취소 "2":오류발급 "3":기타
     * @param cashNum          현금 영수증 번호
     * @param approvalDate     원승인날짜 (YYYYMMDD)
     * @param approvalNumber   원승인번호
     */
    public void DAOU_Cash_Cancel(boolean isExternal, String paymentType, String strTotalPrice, String paymentId, String option, boolean isKeyIn, String cancelReason, String cashNum, String approvalDate, String approvalNumber) {
        mIsExternal = isExternal;
        mPaymentId = paymentId;

        LOG.d(TAG, "strTotalPrice:"+strTotalPrice);
        if (isExternal) {
            mPrice = new ResultPrice();
            try {
                mPrice.totalPrice = Long.parseLong(strTotalPrice);
            }
            catch (NumberFormatException e) {
                mPrice.totalPrice = 0;

            }
        }
        else {
            mPrice = DBApi.getApprovePrice(this, paymentId);
        }
        maskingCardData = cashNum;
        mPaymentHistoryTranType = paymentType;
        payment(paymentType, isKeyIn, null, null, mPrice, null, fallback, approvalNumber, approvalDate, cashNum, cancelReason, option);
    }

    /**
     * 외부용 카드결제 함수
     * @param password        패스워드(은련카드 결제시)
     * @param bmpSign         싸인데이터(5만원 초과 결제시)
     * @param strTotalPrice   총 결제 금액
     * @param strOriginPrice  물품가액
     * @param strVatPrice     부가세
     * @param strServicePrice 봉사료
     * @param month           할부개월수(0:일시불 최대 11개월까지만 가능)
     * @param fallback        fallback 코드
     */
    public void DAOU_Credit(String paymentType, String password, Bitmap bmpSign, String strTotalPrice, String strOriginPrice, String strVatPrice, String strServicePrice, String month, int fallback) {
        mIsExternal = true;
        ResultPrice price = new ResultPrice();
        try {
            price.totalPrice = Long.parseLong(strTotalPrice);
            price.taxFreePrice = Long.parseLong(strOriginPrice);
            price.taxPrice = Long.parseLong(strVatPrice);
            price.servicePrice = Long.parseLong(strServicePrice);
        }
        catch (Exception e) {
            price.servicePrice = 0;
            price.taxPrice = 0;
            price.taxFreePrice = 0;
            price.totalPrice = 0;
        }
        mPaymentHistoryTranType = paymentType;
        payment(paymentType, false, password, bmpSign, price, month, fallback, null, null, null, null, null);
    }



    /**
     * 외부용 현금 결제 함수
     * @param strTotalPrice   총 결제 금액
     * @param strOriginPrice  물품가액
     * @param strVatPrice     부가세
     * @param strServicePrice 봉사료
     * @param option          거래구분 "1":소득공제 "2":지출증빙 "3":자진발급
     * @param isKeyIn         true:직접입력시 false:카드사용시
     * @param cashNum         전화번호 또는 카드번호
     */
    public void DAOU_Cash(String paymentType, String strTotalPrice, String strOriginPrice, String strVatPrice, String strServicePrice, String option, boolean isKeyIn, String cashNum) {
        mIsExternal = true;
        ResultPrice price = new ResultPrice();
        try {
            price.totalPrice = Long.parseLong(strTotalPrice);
            price.taxFreePrice = Long.parseLong(strOriginPrice);
            price.taxPrice = Long.parseLong(strVatPrice);
            price.servicePrice = Long.parseLong(strServicePrice);
        }
        catch (Exception e) {
            price.servicePrice = 0;
            price.taxPrice = 0;
            price.taxFreePrice = 0;
            price.totalPrice = 0;
        }
        mPaymentHistoryTranType = paymentType;
        payment(paymentType, isKeyIn, null, null, price, null, fallback, null, null, cashNum, null, option);
    }

    /**
     * 내부용 일반영수증 함수
     * @param paymentType
     * @param strInputPrice    입력 결제 금액
     * @param strServicePrice  봉사료
     * @param approvalNumber   승인번호 (취소시)
     */
    public void DAOU_Fake(String paymentType, String paymentId, String strInputPrice, String strServicePrice, String approvalNumber, String cancelReason) {
        mPrice = Util.calculatePrice(this, strInputPrice, strServicePrice, false);
        mPaymentHistoryTranType = paymentType;
        mPaymentId = paymentId;
        DBApi.getApprovePrice(this, paymentId);
        payment(paymentType, false, null, null, mPrice, null, fallback, approvalNumber, null, null, cancelReason, null);
    }
    public String getSystemIcAutoPay() {
        return new DBHelper(this).getSystemIcAutoPay();
    }





    @Override
    public void DAOU_Payment_CB(final ResultPayment resultPayment) {
        super.DAOU_Payment_CB(resultPayment);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissDialog();

                if (resultPayment == null){
                    LOG.e(TAG, "resultPayment is NULL");
                    return;
                }

                mResultPayment = resultPayment;

                BizInfo info = Util.getBizInfo(DaouPayActivity.this);

                if (resultPayment.resultCode == 0) {
                    if (!mIsExternal) {
                        if(info.serverSync.equals("1")) {
                            mIsReSend = false;
                            sendPaymentHistory(mPaymentHistoryTranType, mPaymentId, mPrice, resultPayment, maskingCardData, mIsUnionPay, mIsKeyIn);
                        }
                        else {
                            long id = DBApi.addPaymentHistory(DaouPayActivity.this, mPrice, mResultPayment, mPaymentHistoryTranType, "-1", "-1", mIsUnionPay, maskingCardData, mIsKeyIn);
                            if (id != -1) {
                                startPayResult(0, id);
                            }
                        }
                    }
                }
                else {
                    showAlertDialog("알림", new String[]{"(" + resultPayment.resultCode + ") " + resultPayment.screenMessage}, true,
                            true, false, false, "확인", null, null, null, null, null);
                }

            }
        });
    }

    @Override
    public void DAOU_OnICInsert() {
        super.DAOU_OnICInsert();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        DAOU_EmvStart();
                    }
                }, 100);
                showDialog("IC카드 정보를 읽는 중입니다..");
            }
        });

    }

    @Override
    public void DAOU_OnICRemove() {
        super.DAOU_OnICRemove();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showToast("IC카드가 제거 되었습니다.");
            }
        });
    }

    @Override
    public void DAOU_OnMSRRead() {
        super.DAOU_OnMSRRead();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showDialog("MSR카드 정보를 읽는 중입니다..");
            }
        });

    }

    @Override
    public void DAOU_OnFallback(final int fallback) {
        super.DAOU_OnFallback(fallback);
        LOG.d(TAG, "DAOU_OnFallback() fallbackCode" + String.valueOf(fallback));
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String title = "";
                String msg = "";
                dismissDialog();
                if(fallback == 2 || fallback == 30 || fallback == 31 || fallback == 32) {
                    title = "거래 불가능 카드";
                    msg = "사용 불가능한 카드입니다." + "\n에러코드 : " + fallback;
                }
                else if (fallback == 20) {
                    title = "TimeOut 발생";
                    msg = "IC카드를 제거 후 다시 삽입해주세요." + "\n에러코드 : " + fallback;
                }
                else {
                    title = "FallBack 거래 발생";
                    msg = "IC를 읽을 수 없습니다.\n마그네틱으로 결제해 주세요." + "\n에러코드 : " + fallback;
                }
                showAlertDialog(title, new String[]{msg}, true,
                        true, false, false, "확인", null, null, null, null, null);
            }
        });
    }

    @Override
    public void DAOU_OnCardData(final String s) {
        super.DAOU_OnCardData(s);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissDialog();
                maskingCardData = s;
                LOG.d(TAG, s);
            }
        });
    }

    @Override
    public void DAOU_Payment_Incomplete(final int resultCode, String msg) {
        super.DAOU_Payment_Incomplete(resultCode, msg);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String msg = "";
                if(resultCode == 0) {
                    msg = "미완료거래가 발생이 되었습니다.";
                }
                else {
                    msg = "미완료거래 중 오류가 발생하였습니다. 카드사에 확인 부탁 드립니다.";
                }
                showAlertDialog("알림", new String[]{msg}, true,
                        true, false, false, "확인", null, null, null, null, null);

            }
        });
    }

    @Override
    public void DAOU_Error_CB(int resultCode) {
        super.DAOU_Error_CB(resultCode);
    }

    @Override
    public void DAOU_CardError_CB(final int errorCode) {
        super.DAOU_CardError_CB(errorCode);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String msg = "";
                dismissDialog();
                switch(errorCode) {
                    case CBDaouApi.CODE_EMV_ERROR: {
                        msg = "EMV 처리 중 오류가 발생하였습니다.\nIC카드를 다시 재삽입하시고 시도해주세요.";
                    }
                    break;

                    case CBDaouApi.CODE_IC_READING_ERROR: {
                        msg = "IC 리딩 에러";
                    }
                    break;

                    case CBDaouApi.CODE_MSR_READING_ERROR:  {
                        msg = "MSR 리딩 에러";
                    }
                    break;

                    case CBDaouApi.CODE_MSR_PROTECTED_ERROR: {
                        msg = "IC로 결제해야합니다.\nIC슬롯에 카드를 삽입하여 결제하십시오." + "\n에러코드 : " + CBDaouApi.CODE_MSR_PROTECTED_ERROR;
                    }
                    break;
                }
                showAlertDialog("알림", new String[]{msg}, true,
                        true, false, false, "확인", null, null, null, null, null);

            }
        });

    }
    /**
     * 서버 통신 성공시 콜백
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call<Object> call, Response<Object> response) { //결제내역 전송 결과 콜백
        super.onResponse(call, response);
        DBHelper db = new DBHelper(this);
        BizInfo bizInfo;
        String isSendData, sendFailedReason;
        if (mIsSendServerState) {
            try {
                LOG.d(TAG, "RESPONSE: " + ((ResponseBody) response.body()).string());
                isSendData = PaymentHistory.SEND_DATA_SUCCESS;
                sendFailedReason = "success";
            } catch (IOException e) {
                e.printStackTrace();
                isSendData = PaymentHistory.SEND_DATA_FAILED;
                sendFailedReason = "onResponse failed";
            }
            LOG.d(TAG, "paymentType" + mPaymentHistoryTranType);
            if (mIsPaymentListActivity) { // 재전송인 경우
                // 서버로 전송이끝난후 결제내역을 전송 받는다
                receiveServerSync();
                if (isSendData.equals(PaymentHistory.SEND_DATA_SUCCESS)) {
                    // 재전송이고 전송이 성공한경우애는 플래그를 바꿔준다.
                    // 전송이 실패한경우에는 아무것도 하지않는다.
                    db.setPaymentHistorySendDataSuccess(mApprovalNumOrg);
                }
            }
            else { // 결제시 전송하는 경우
                long id = DBApi.addPaymentHistory(DaouPayActivity.this, mPrice, mResultPayment, mPaymentHistoryTranType, isSendData, sendFailedReason, mIsUnionPay, maskingCardData, mIsKeyIn);
                if (id != -1) {
                    startPayResult(0, id);
                }
            }
        }
        else {
            try {
                LOG.d(TAG, "RESPONSE: " + ((ResponseBody) response.body()).string());
                isSendData = PaymentHistory.SEND_DATA_SUCCESS;
            } catch (IOException e) {
                e.printStackTrace();
                isSendData = PaymentHistory.SEND_DATA_FAILED;
            }
            if (isSendData.equals(PaymentHistory.SEND_DATA_SUCCESS)) {
                int rowcnt = Integer.parseInt(((GetOrdersResponse)response.body()).rowcnt);
                String ts = ((GetOrdersResponse) response.body()).ts;
                LOG.d(TAG, "타임스탬프~" + ts);
                db.setBizInfoTs(ts); // 타임스탬프를 업그레이드 // 단말기 개시거래할때 초기화 되어있음

                for (int i = 0; i < rowcnt; i ++) {
                    String approvalNumOrg = ((GetOrdersResponse)response.body()).data.get(i).approval_num_org;
                    String tid = ((GetOrdersResponse)response.body()).data.get(i).tid;
                    String bizNum = ((GetOrdersResponse)response.body()).data.get(i).biz_num;
                    String shopId = ((GetOrdersResponse)response.body()).data.get(i).shop_id;
                    String pin = ((GetOrdersResponse)response.body()).data.get(i).pin;
                    String poscode = ((GetOrdersResponse)response.body()).data.get(i).poscode;
                    String tranType = ((GetOrdersResponse)response.body()).data.get(i).tran_type;
                    String cardNum = ((GetOrdersResponse)response.body()).data.get(i).card_num;
                    String cardName = ((GetOrdersResponse)response.body()).data.get(i).card_name;
                    String cardCompany = ((GetOrdersResponse)response.body()).data.get(i).card_company;
                    String balance = ((GetOrdersResponse)response.body()).data.get(i).balance;
                    String totalVal = ((GetOrdersResponse)response.body()).data.get(i).total_val;
                    String originVal = ((GetOrdersResponse)response.body()).data.get(i).origin_val;
                    String vatVal = ((GetOrdersResponse)response.body()).data.get(i).vat_val;
                    String serviceVal = ((GetOrdersResponse)response.body()).data.get(i).service_val;
                    String monthVal = ((GetOrdersResponse)response.body()).data.get(i).month_val;
                    String approvalNum = ((GetOrdersResponse)response.body()).data.get(i).approval_num;
                    String approvalDate = ((GetOrdersResponse)response.body()).data.get(i).approval_date;
                    String cancelReason = ((GetOrdersResponse)response.body()).data.get(i).cancel_reason;
                    String isKeyIn = ((GetOrdersResponse)response.body()).data.get(i).is_key_in;
                    String canceled = ((GetOrdersResponse)response.body()).data.get(i).canceled;
                    String serviceValPer = ((GetOrdersResponse)response.body()).data.get(i).rate_service;
                    String vatValPer = ((GetOrdersResponse)response.body()).data.get(i).rate_vat;
                    String vatValInclude = (((GetOrdersResponse)response.body()).data.get(i).flag_vat).equals("Y") ? "0" : "1"; // Y:부가세포함 N:부가세미포함
                    String isUnionPay = (((GetOrdersResponse)response.body()).data.get(i).is_union_pay).equals("Y") ? "1" : "0";
                    String userName = ((GetOrdersResponse)response.body()).data.get(i).staff_nm;
                    String approvalNumDef = ((GetOrdersResponse)response.body()).data.get(i).approval_num_def;
                    String printMessage = ((GetOrdersResponse)response.body()).data.get(i).print_message;
                    LOG.d(TAG, "전송받은 결제데이터 사업자번호: " + bizNum + "   고유번호: " + approvalNumOrg + "   승인번호: " + approvalNum + "   취소여부: " + canceled);

                    bizInfo = Util.getBizInfo(this, tid);
                    db.addPaymentHistory(
                            tranType,
                            cardNum,
                            cardName,
                            cardCompany,
                            balance,
                            totalVal,
                            originVal,
                            vatVal,
                            serviceVal,
                            monthVal,
                            approvalNumOrg,
                            approvalNum,
                            approvalDate,
                            isKeyIn,
                            "0", // 응답코드
                            tid,
                            bizNum,
                            bizInfo.shopName,
                            bizInfo.shopCeo,
                            bizInfo.shopAddress,
                            bizInfo.shopTel,
                            bizInfo.userMsg,
                            cancelReason,
                            canceled,
                            PaymentHistory.SEND_DATA_SUCCESS,
                            "success",
                            shopId,
                            pin,
                            poscode,
                            isUnionPay,
                            cardNum,
                            vatValPer,
                            serviceValPer,
                            vatValInclude,
                            userName,
                            "1",
                            printMessage,
                            "0",
                            approvalNumDef);
                }
            }
        }
    }

    /**
     * 서버 통신 실패시 콜백
     * @param call
     * @param t
     */
    @Override
    public void onFailure(Call<Object> call, Throwable t) {
        super.onFailure(call, t);
        if (mIsSendServerState) {
            String isSendData, sendFailedReason;
            isSendData = PaymentHistory.SEND_DATA_FAILED;
            sendFailedReason = t.getMessage();
            if (mIsPaymentListActivity) {
                // 서버로부터 전송이끝난후 결제내역을 전송 받는다
                receiveServerSync();
            }
            else {
                long id = DBApi.addPaymentHistory(DaouPayActivity.this, mPrice, mResultPayment, mPaymentHistoryTranType, isSendData, sendFailedReason, mIsUnionPay, maskingCardData, mIsKeyIn);
                if (id != -1) {
                    startPayResult(0, id);
                }
            }
        }
        else {
            // 서버로부터 데이터를 전송받는게 실패하면 아무것도 하지않는다.
        }

    }
    @Override
    protected void onStart() {
        super.onStart();
        if (mIsPaymentListActivity) {
            LOG.d(TAG, "isPaymentListActivity is true");
            sendServerSync();
        }
        else {
            LOG.d(TAG, "isPaymentListActivity is false");
        }
    }

    private void payment(String paymentType, boolean isKeyIn, String password, Bitmap bmpSign, ResultPrice price, String month, int fallback,
                         String approvalNumber, String approvalDate, String cashNum, String cancelReason, String option) {

        LOG.d(TAG, "payment paymentType:"+paymentType+" isKeyIn:"+isKeyIn+" password:"+password+" total:"+price.totalPrice+" month:"+month
            +" fallback:"+fallback+" approvalNumber:"+approvalNumber+ " approvalDate:"+approvalDate+ "cashNum:"+cashNum+" cancelReason:"+cancelReason+" option:"+option);

        String loadmsg = "";
        BizInfo bizInfo = Util.getBizInfo(this);

        //하드웨어 넘버
        String hwnum;
        //터미널 넘버
        String termNum;
        //생산일련번호
        String vanSerial;

        mPaymentHistoryTranType = paymentType;
        mIsKeyIn = isKeyIn;

        if (password == null || password.isEmpty()) {
            mIsUnionPay = PaymentHistory.EULLYEON_NOT;
        }
        else {
            mIsUnionPay = PaymentHistory.EULLYEON_ON;
        }

//        if(Const.isTest) {
//            termNum = Const.TEST_TERMINAL_NUMBER;
//            vanSerial = Const.TEST_VAN_NUMBER;
//            hwnum = SingleTon.getInstance().getHWNum();
//        }
//        else {
//            termNum = bizInfo.tid;
//            vanSerial = Const.VAN_NUMBER;
//        }

        termNum = bizInfo.tid;
        vanSerial = getVanSerial();
        hwnum = getHwNum();

        if(approvalDate == null || approvalDate.isEmpty()) {
            loadmsg = "결제 요청중입니다...";
        }
        else {
            loadmsg = "취소 요청중입니다...";
        }
        showDialog(loadmsg);
        if(paymentType.equals(PaymentHistory.TRANTYPE_CREDIT_CARD)) { // 카드결제
            DAOU_Credit(hwnum, termNum, vanSerial, "", password, bmpSign, price.totalPrice, price.taxPrice, price.servicePrice, price.taxFreePrice, month, fallback);
        }
        else if (paymentType.equals(PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION)
                || paymentType.equals(PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE)
                || paymentType.equals(PaymentHistory.TRANTYPE_CASH_VOLUNTARY)) { // 현금결제
            DAOU_Cash(hwnum, termNum, vanSerial, "", price.totalPrice, price.taxPrice, price.servicePrice, price.taxFreePrice, option, isKeyIn, cashNum);
        }
        else if (paymentType.equals(PaymentHistory.TRANTYPE_CREDIT_CARD_CANCEL)) { // 카드취소
            DAOU_Credit_Cancel(hwnum, termNum, vanSerial, "", password, bmpSign, price.totalPrice, month, approvalDate, approvalNumber, fallback);
        }
        else if (paymentType.equals(PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION_CANCEL)
                || paymentType.equals(PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE_CANCEL)
                || paymentType.equals(PaymentHistory.TRANTYPE_CASH_VOLUNTARY_CANCEL)) { // 현금취소
            DAOU_Cash_Cancel(hwnum, termNum, vanSerial, "", price.totalPrice, option, isKeyIn, cancelReason, cashNum , approvalDate, approvalNumber);
        }
        else if (paymentType.equals(PaymentHistory.TRANTYPE_CASH_DEFAULT)) { // 일반영수증
            DAOU_Fake(termNum, price.totalPrice, price.taxPrice, price.servicePrice, price.taxFreePrice, null, false, cancelReason);
        }
        else if (paymentType.equals(PaymentHistory.TRANTYPE_CASH_DEFAULT_CANCEL)) { // 일반영수증 취소
            DAOU_Fake(termNum, price.totalPrice, price.taxPrice, price.servicePrice, price.taxFreePrice, approvalNumber, false, cancelReason);
        }


    }
    /**
     * 현재액티비티가 결제리스트 액티비티인지 검사
     * 롤리팝 이상부터는 getRunningTasks가 작동
     * @return
     */
    public void setIsPaymentListActivity(boolean isPaymentListActivity){
        mIsPaymentListActivity = isPaymentListActivity;
    }

    /**
     * 전송 실패한 결제내역을 서버로 재전송 하는 코드
     * 전송실패한 결제 레코드의 개수를 추출해서 0이아니라면 서버에 전송한다.
     * 그리고 전송에 성공하면 전송 성공여부의 플래그를 바꿔준다.;
     */
    private void sendServerSync() {
        PaymentHistory paymentHistory = new PaymentHistory();
        DBHelper db = new DBHelper(this);
        Cursor paymentHistorySendDataFailedCursor = db.getPaymentHistorySendDataFailedCursor();
        int recordCount = paymentHistorySendDataFailedCursor.getCount();
        LOG.d(TAG, "not send recordCount: " + recordCount +"");

        if (recordCount != 0){
            LOG.d(TAG, "서버로 전송할 결제 내역이 있습니다.");
            while (paymentHistorySendDataFailedCursor.moveToNext()){
                paymentHistory.fetch(paymentHistorySendDataFailedCursor);
                reSendPaymentHistory(paymentHistory);
                mApprovalNumOrg = paymentHistory.approvalNumOrg;
            }
        }
        else {
            LOG.d(TAG, "서버로 전송할 결제 내역이 없습니다.");
            receiveServerSync();
        }
    }
}

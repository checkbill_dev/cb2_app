package kr.checkbill.cb2.util;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Administrator on 2017-03-07.
 */

public class SendReceipt {

    /**
     * 카드결제용 공유하기 기능
     * 잔액표시 따로안해주고 프린트스크린으로 대체함
     */
    public static void sendCardReceipt(Context context,
                                       String title, String shopName, String shopCeo, String shopBizNum, String shopTel, String shopAddress,
                                       String cardNum, String month, String approvalDate, String approvalNumOrg, String approvalNum, String shopTid,
                                       String cardName, String cardCompany, String balance, String originVal, String vatVal, String serviceVal, String totalVal,
                                       String printMessage, String userName, String userMsg
    ){
        StringBuilder sb = new StringBuilder();
        sb.append("***");
        sb.append(title +""); // 타이틀
        sb.append("\n");
        sb.append("***");
        sb.append("\n");
        sb.append("상호: " + shopName);
        sb.append("\n");
        sb.append("대표자: " + shopCeo);
        sb.append("\n");
        sb.append("사업자번호: " + shopBizNum);
        sb.append("\n");
        sb.append("전화번호: " + shopTel);
        sb.append("\n");
        sb.append("주소: " + shopAddress);
        sb.append("\n");
        sb.append("***");
        sb.append("\n");
        sb.append("카드번호: " + cardNum);
        sb.append("\n");
        sb.append("할부개월: " + month);
        sb.append("\n");
        sb.append("거래일시: " + approvalDate);
        sb.append("\n");
        sb.append("거래고유번호: " + approvalNumOrg);
        sb.append("\n");
        sb.append("승인번호: " + approvalNum);
        sb.append("\n");
        sb.append("단말기번호: " + shopTid);
        sb.append("\n");
        sb.append("카드명: " + cardName);
        sb.append("\n");
        sb.append("매입사명: " + cardCompany);
        sb.append("\n");
        sb.append("***");
        sb.append("\n");
        if (balance != null && !balance.equals("") && !balance.equals("0")) {
            sb.append("잔액: " + balance + "원");
            sb.append("\n");
        }
        sb.append("물품가액: " + originVal + "원");
        sb.append("\n");
        sb.append("부가세액: " + vatVal + "원");
        sb.append("\n");
        sb.append("봉사료: " + serviceVal + "원");
        sb.append("\n");
        sb.append("승인금액: " + totalVal + "원");
        sb.append("\n");
        sb.append("***");
        sb.append("\n");
        sb.append("담당자: " + userName);
        sb.append("\n");
        sb.append(userMsg +""); // 한줄메모
        sb.append("\n");
        sb.append(printMessage +""); // 프린트 메세지
        sb.append("\n");

        Intent msg = new Intent(Intent.ACTION_SEND);
        msg.addCategory(Intent.CATEGORY_DEFAULT);
        msg.putExtra(Intent.EXTRA_SUBJECT, title);
        msg.putExtra(Intent.EXTRA_TEXT, sb.toString());
        msg.setType("text/plain");
        context.startActivity(Intent.createChooser(msg, "영수증 전송"));
    }
    public static void sendCashReceipt(Context context,
                                       String title, String shopName, String shopCeo, String shopBizNum, String shopTel, String shopAddress,
                                       String sortation, String approvalDate, String approvalNumOrg, String approvalNum, String shopTid, String cancelReason,
                                       String originVal, String vatVal, String serviceVal, String totalVal, String printMessage, String userName, String userMsg)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("***");
        sb.append(title +""); // 타이틀
        sb.append("\n");
        sb.append("***");
        sb.append("\n");
        sb.append("구분: " + sortation);
        sb.append("\n");
        sb.append("상호: " + shopName);
        sb.append("\n");
        sb.append("대표자: " + shopCeo);
        sb.append("\n");
        sb.append("사업자번호: " + shopBizNum);
        sb.append("\n");
        sb.append("전화번호: " + shopTel);
        sb.append("\n");
        sb.append("주소: " + shopAddress);
        sb.append("\n");
        sb.append("***");
        sb.append("\n");
        sb.append("거래일시: " + approvalDate);
        sb.append("\n");
        sb.append("거래고유번호: " + approvalNumOrg);
        sb.append("\n");
        sb.append("승인번호: " + approvalNum);
        sb.append("\n");
        sb.append("단말기번호: " + shopTid);
        sb.append("\n");
        if (!(cancelReason == null || cancelReason.isEmpty())) {
            sb.append("취소사유: " + cancelReason);
            sb.append("\n");
        }

        sb.append("***");
        sb.append("\n");
        sb.append("물품가액: " + originVal + "원");
        sb.append("\n");
        sb.append("부가세액: " + vatVal + "원");
        sb.append("\n");
        sb.append("봉사료: " + serviceVal + "원");
        sb.append("\n");
        sb.append("승인금액: " + totalVal + "원");
        sb.append("\n");
        sb.append("***");
        sb.append("\n");
        sb.append("담당자: " + userName);
        sb.append("\n");
        sb.append(userMsg +""); // 한줄메모
        sb.append("\n");
        sb.append(printMessage +""); // 프린트 메세지
        sb.append("\n");


        Intent msg = new Intent(Intent.ACTION_SEND);
        msg.addCategory(Intent.CATEGORY_DEFAULT);
        msg.putExtra(Intent.EXTRA_SUBJECT, title);
        msg.putExtra(Intent.EXTRA_TEXT, sb.toString());
        msg.setType("text/plain");
        context.startActivity(Intent.createChooser(msg, "영수증 전송"));
    }

}

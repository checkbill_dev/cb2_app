package kr.checkbill.cb2.activity.payment;

import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.checkbill.cbapi.ResultPayment;

import kr.checkbill.cb2.activity.base.DaouPayActivity;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.Util;

public class CashPayCancelActivity extends DaouPayActivity
        implements View.OnClickListener, TextWatcher, View.OnFocusChangeListener {
    private final static String TAG = "CashReceiptPublishCancelActivity";
    private EditText edit_cash_receipt_number;
    private Button btn_ok, btn_ok_tint;
    private RadioButton rbtn_payment_cancel, rbtn_error_issue, rbtn_etc;
    private View v_center_space, v_underline6, v_underline7, v_underline8;
    private LinearLayout ll_bottom_space, ll_cash_number_title_group, ll_cash_number_group;
    private DBHelper mDB;
    private PaymentHistory paymentHistory;
    private String tranType;
    private String paymenyHistory_id;
    private String option; // 결제모듈 입력용 TranType "A":소득공제 "B":지출증빙 "E":자진발급

    boolean isKeyIn = true; // 직접 입력시 true, 현금 영수증 카드 이용시 false
    private String cardData;
    /**
     * 카드를 긁은것인지 직접 현금영수증 번호를 입력한것인지 정확히 판단하기 위해서 사용
     * 카드데이터가 들어오면 true가되고 EditText에 포커스가오면 false가 된다.
     */
    private boolean isOnCardLeading = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_pay_cancel);

        edit_cash_receipt_number = (EditText) findViewById(R.id.edit_cash_receipt_number);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok_tint = (Button) findViewById(R.id.btn_ok_tint);
        rbtn_payment_cancel = (RadioButton) findViewById(R.id.rbtn_payment_cancel);
        rbtn_error_issue = (RadioButton) findViewById(R.id.rbtn_error_issue);
        rbtn_etc = (RadioButton) findViewById(R.id.rbtn_etc);
        v_center_space = findViewById(R.id.v_center_space);
        v_underline6 = findViewById(R.id.v_underline6);
        v_underline7 = findViewById(R.id.v_underline7);
        v_underline8 = findViewById(R.id.v_underline8);
        ll_bottom_space = (LinearLayout) findViewById(R.id.ll_bottom_space);
        ll_cash_number_title_group = (LinearLayout) findViewById(R.id.ll_cash_number_title_group);
        ll_cash_number_group = (LinearLayout) findViewById(R.id.ll_cash_number_group);

        registerOnClickListener();
        registerTextWatcher();
        registerOnFocusChangeListener();

        mDB = new DBHelper(this);
        paymentHistory = new PaymentHistory();


        paymenyHistory_id = getIntent().getStringExtra("paymentId");
        String where = "_id = '" + paymenyHistory_id + "'";
        Cursor paymentHistoryCursor = mDB.getPaymentHistoryCursorWhere(where);
        while (paymentHistoryCursor.moveToNext()){
            paymentHistory.fetch(paymentHistoryCursor);
        }

        SetText(R.id.txt_approval_date, paymentHistory.approvalDate);
        SetText(R.id.txt_amount, Util.commaFormat(paymentHistory.totalVal));
        if ((paymentHistory.tranType).equals(PaymentHistory.TRANTYPE_CASH_DEFAULT)) { // 일반영수증인경우 번호 입력란을 가린다.
            v_center_space.setVisibility(View.GONE);
            ll_cash_number_title_group.setVisibility(View.GONE);
            ll_cash_number_group.setVisibility(View.GONE);
            ll_bottom_space.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, (float) 3.3));
        }


        if ((paymentHistory.tranType).equals(PaymentHistory.TRANTYPE_CASH_DEFAULT)){ // 일반영수증
            SetText(R.id.txt_type, "일반영수증");

            tranType = PaymentHistory.TRANTYPE_CASH_DEFAULT_CANCEL;

            edit_cash_receipt_number.setClickable(false);
            edit_cash_receipt_number.setHint("-");
            edit_cash_receipt_number.setFocusable(false);
        }
        else if ((paymentHistory.tranType).equals(PaymentHistory.TRANTYPE_CASH_VOLUNTARY)){ // 자진발급
            SetText(R.id.txt_type, "자진발급");

            tranType = PaymentHistory.TRANTYPE_CASH_VOLUNTARY_CANCEL;
            option = "E";
        }
        else if ((paymentHistory.tranType).equals(PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION)){ // 소득공제
            SetText(R.id.txt_type, "소득공제");

            tranType = PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION_CANCEL;
            option = "A";
        }
        else if ((paymentHistory.tranType).equals(PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE)){ // 지출증빙
            SetText(R.id.txt_type, "지출증빙");

            tranType = PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE_CANCEL;
            option = "B";
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.edit_cash_receipt_number:
                isOnCardLeading = false;
                break;
            case R.id.rbtn_payment_cancel:
                v_underline6.setVisibility(View.VISIBLE);
                v_underline7.setVisibility(View.INVISIBLE);
                v_underline8.setVisibility(View.INVISIBLE);

                cardData = edit_cash_receipt_number.getText().toString();
                if(isKeyIn == true) {
                    cardData = edit_cash_receipt_number.getText().toString();

                    StringBuilder sb = new StringBuilder();
                    for(int i=0; i<cardData.length(); i++) {
                        if(i>=3 && i<7 ) {
                            sb.append("*");
                        }
                        else {
                            sb.append(cardData.charAt(i));
                        }
                    }
                    SetText(R.id.edit_cash_receipt_number, sb.toString());
                }

                if ((paymentHistory.tranType).equals(PaymentHistory.TRANTYPE_CASH_DEFAULT)){ // 일반영수증 취소인 경우
                    btn_ok.setVisibility(View.VISIBLE);
                    btn_ok_tint.setVisibility(View.GONE);
                }
                else {
                    if(edit_cash_receipt_number.getText().toString().length()>=1){
                        btn_ok.setVisibility(View.VISIBLE);
                        btn_ok_tint.setVisibility(View.GONE);
                    }
                    else{
                        btn_ok.setVisibility(View.GONE);
                        btn_ok_tint.setVisibility(View.VISIBLE);
                    }
                }
                paymentHistory.cancelReason = "1";
                break;
            case R.id.rbtn_error_issue:
                v_underline6.setVisibility(View.INVISIBLE);
                v_underline7.setVisibility(View.VISIBLE);
                v_underline8.setVisibility(View.INVISIBLE);
                if ((paymentHistory.tranType).equals(PaymentHistory.TRANTYPE_CASH_DEFAULT)){
                    btn_ok.setVisibility(View.VISIBLE);
                    btn_ok_tint.setVisibility(View.GONE);
                }
                else {
                    if(edit_cash_receipt_number.getText().toString().length()>=1){
                        btn_ok.setVisibility(View.VISIBLE);
                        btn_ok_tint.setVisibility(View.GONE);
                    }
                    else{
                        btn_ok.setVisibility(View.GONE);
                        btn_ok_tint.setVisibility(View.VISIBLE);
                    }
                }
                paymentHistory.cancelReason = "2";
                break;
            case R.id.rbtn_etc:
                v_underline6.setVisibility(View.INVISIBLE);
                v_underline7.setVisibility(View.INVISIBLE);
                v_underline8.setVisibility(View.VISIBLE);
                if ((paymentHistory.tranType).equals(PaymentHistory.TRANTYPE_CASH_DEFAULT)){
                    btn_ok.setVisibility(View.VISIBLE);
                    btn_ok_tint.setVisibility(View.GONE);
                }
                else {
                    if(edit_cash_receipt_number.getText().toString().length()>=1){
                        btn_ok.setVisibility(View.VISIBLE);
                        btn_ok_tint.setVisibility(View.GONE);
                    }
                    else{
                        btn_ok.setVisibility(View.GONE);
                        btn_ok_tint.setVisibility(View.VISIBLE);
                    }
                }
                paymentHistory.cancelReason = "3";
                break;
            case R.id.btn_ok:
                // yyyy-MM-dd HH:mm:ss yyyyMMdd형식으로 바꿔준다.
                String[] splitSpace = (paymentHistory.approvalDate).split(" ");
                String date = splitSpace[0];
                final String resultDate = date.replace("-", "");
                if (cardData == null) {
                    cardData = edit_cash_receipt_number.getText().toString();
                }

                if ((paymentHistory.tranType).equals(PaymentHistory.TRANTYPE_CASH_DEFAULT)){ //일반영수증 - 부가세없음, 결제내용을 DB에만 저장한다.
                    DAOU_Fake(tranType, paymenyHistory_id, paymentHistory.totalVal, paymentHistory.serviceVal, paymentHistory.approvalNum, paymentHistory.cancelReason);
                }
                else {
                    LOG.d(TAG, "기존 isKeyIn: " + Boolean.valueOf(paymentHistory.IS_KEY_IN).booleanValue());
                    LOG.d(TAG, "현재 isKeyIn: " + Boolean.valueOf(isKeyIn).booleanValue());
                    if (Boolean.valueOf(paymentHistory.isKeyIn).booleanValue() == isKeyIn) { // 결제할때와 취소할때의 iskeyin이 일치하는 경우
                        DAOU_Cash_Cancel(false, tranType, null, paymenyHistory_id, option,
                                isKeyIn, paymentHistory.cancelReason, cardData,
                                resultDate, paymentHistory.approvalNum);
                    }
                    else { // 결제할때와 취소할때의 iskeyin이 일치하지 않는 경우
                        if (Boolean.valueOf(paymentHistory.isKeyIn).booleanValue()) {
                            showAlertDialog("번호 입력 오류", new String[]{"현금 영수증 번호를 직접 입력해서 결제해주세요."}, true,
                                    true, false, false, "확인", null, null, null, null, null);
                        }
                        else {
                            showAlertDialog("번호 입력 오류", new String[]{"현금 영수증 카드를 리딩해서 결제해주세요."}, true,
                                    true, false, false, "확인", null, null, null, null, null);
                        }
                    }

                }
                break;

        }
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (edit_cash_receipt_number.hasFocus()) {
            if (isOnCardLeading) {
                // MSR이 긁힌 경우에 텍스트가 변경되었을때는 isKeyIn을 false로 변경
                isKeyIn = false;
            }
            else {
                // EditText에 포커스가 간다음에 텍스트가 변경되었을때는 isKeyIn을 true로 변경
                isKeyIn = true;
                if (isKeyIn) {
                    edit_cash_receipt_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
                }
            }

            if ((rbtn_payment_cancel.isChecked()
                    || rbtn_error_issue.isChecked()
                    || rbtn_etc.isChecked()) && (edit_cash_receipt_number.length() >= 1)){
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }
        }
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            v.setBackgroundResource(R.drawable.blue_radius);
        }
        else {
            v.setBackgroundResource(R.drawable.gray_radius);
        }
        switch (v.getId()) {
            case R.id.edit_cash_receipt_number:
                if(hasFocus){//포커스를 얻을때
                    isOnCardLeading = false;
                    if(isKeyIn == true) {
                        SetText(R.id.edit_cash_receipt_number, "");
                    }
                }
                else{
                    if(isKeyIn == true) {
                        cardData = edit_cash_receipt_number.getText().toString();

                        StringBuilder sb = new StringBuilder();
                        for(int i=0; i<cardData.length(); i++) {
                            if(i>=3 && i<7 ) {
                                sb.append("*");
                            }
                            else {
                                sb.append(cardData.charAt(i));
                            }
                        }
                        SetText(R.id.edit_cash_receipt_number, sb.toString());
                    }
                }
                break;
        }
    }
    @Override
    public void BT_CB(final int resultCode, String BTAdress) {
        super.BT_CB(resultCode, BTAdress);
    }

    @Override
    public void DAOU_Payment_CB(final ResultPayment resultPayment) {
        super.DAOU_Payment_CB(resultPayment);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (resultPayment.resultCode != 0) {
                    SetText(R.id.edit_cash_receipt_number, "");
                }
            }
        });
    }

    @Override
    public void DAOU_OnMSRRead() {
        super.DAOU_OnMSRRead();
    }

    @Override
    public void DAOU_OnCardData(final String maskedCarData) {
        super.DAOU_OnCardData(maskedCarData);
        runOnUiThread(new Runnable() {
            public void run() {
                isOnCardLeading = true;
                cardData = maskedCarData;
                edit_cash_receipt_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                SetText(R.id.edit_cash_receipt_number, "****-####-####-****");
                isKeyIn = false;
            }
        });

    }

    @Override
    public void DAOU_Error_CB(final int code) {
        super.DAOU_Error_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.edit_cash_receipt_number, "");
            }
        });
    }
    @Override
    public void DAOU_CardError_CB(int code){
        super.DAOU_CardError_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.edit_cash_receipt_number, "");
            }
        });
    }
	@Override
    public void DAOU_Payment_Incomplete(int resultCode, String message) {
        super.DAOU_Payment_Incomplete(resultCode, message);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetText(R.id.edit_cash_receipt_number, "");
            }
        });
    }
    private void registerOnClickListener() {
        (findViewById(R.id.img_back)).setOnClickListener(this);
        (findViewById(R.id.edit_cash_receipt_number)).setOnClickListener(this);
        (findViewById(R.id.rbtn_payment_cancel)).setOnClickListener(this);
        (findViewById(R.id.rbtn_error_issue)).setOnClickListener(this);
        (findViewById(R.id.rbtn_etc)).setOnClickListener(this);
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }
    private void registerTextWatcher() {
        ((EditText) findViewById(R.id.edit_cash_receipt_number)).addTextChangedListener(this);
    }
    private void registerOnFocusChangeListener() {
        (findViewById(R.id.edit_cash_receipt_number)).setOnFocusChangeListener(this);
    }

    /**
     * 안쓰는 메소드들
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void afterTextChanged(Editable s) {
    }

}

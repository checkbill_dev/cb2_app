package kr.checkbill.cb2.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.checkbill.cbapi.CBApi;
import com.checkbill.cbapi.CBMsg;
import com.checkbill.cbapi.LOG;
import com.checkbill.cbapi.ResultPayment;
import com.checkbill.cbapi.ResultStartBusness;
import com.checkbill.cbapi.VanCode;
import com.checkbill.cbdaouapi.CBDaouApi;
import com.checkbill.cbdaouapi.CBDaouApiCallback;

import java.util.Timer;
import java.util.TimerTask;

import checkbill.kr.cbbootapi.CBBootApi;
import checkbill.kr.cbbootapi.CBBootApiCallback;
import kr.checkbill.cb2.R;


public class FWUpdateActivity extends AppCompatActivity {

    private final static String TAG = "FWUpdateActivity";

    private final static int FW_SUCCESS = 0;   /* 펌웨어 업데이트 성공 */
    private final static int FW_ERROR_SLEEPCTR = 1; /*  다우데이타 슬립방지 요청 실패시 */
    private final static int FW_ERROR_2 = 2;
    private final static int FW_ERROR_3 = 3;
    private final static int FW_ERROR_4 = 4;
    private final static int FW_ERROR_5 = 5;

    private final static int STATE_01_DAOU_BT_CONNECT = 1;
    private final static int STATE_02_SLEEP_CTRL = 2;
    private final static int STATE_03_BOOT_BT_CONNECT = 3;
    private final static int STATE_04_BOOTAPI_START_DFU = 4;
    private final static int STATE_05_BOOTAPI_BACKUP = 5;
    private final static int STATE_06_BOOTAPI_DFU_DATA = 6;
    private final static int STATE_07_BOOTAPI_DFU_RESTORE = 7;
    private final static int STATE_08_BOOTAPI_BT_DISCONNECT = 8;
    private final static int STATE_09_DAOUAPI_BT_CONNECT = 9;
    private final static int STATE_10_DAOUAPI_HASH_WRITE = 10;

    private final static int BT_CONNECT_SEC = 3000;

    private String mBTAddress = "";
    private int mState = 0;
    CBDaouApi mDaouApi;
    CBBootApi mBootApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fwupdate);

        Intent receiveIntent = getIntent();
        mBTAddress = receiveIntent.getStringExtra("btaddress");

        mDaouApi = CBDaouApi.getInstance();
        mDaouApi.DAOU_setCallback(this, daouapi_callback);

        mBootApi = CBBootApi.getInstance();
        mBootApi.BOOT_setCallback(this, bootapi_callback);

    }

    // [#1. Daou CB 연결체크]
    // STATE_01_DAOU_BT_CONNECT
    private void state_01_daou_bt_connect() {

        Log.d(TAG, "state_01_daou_bt_connect");
        mState = STATE_01_DAOU_BT_CONNECT;

        if(mDaouApi.BT_IsConnect() == true) {
            state_02_sleep_ctrl();
        }
        else
        {
            Log.d(TAG, "state_01_daou_bt_connect mDaouApi.BT_Connect()");
            mDaouApi.BT_Connect();
        }
    }

    // 다우데이타 펌웨어가 슬립에 못 들어가도록 한다.
    //STATE_02_SLEEP_CTRL
    private void state_02_sleep_ctrl() {

        mState = STATE_02_SLEEP_CTRL;
        Log.d(TAG, "state_02_sleep_ctrl");
        mDaouApi.DAOU_Sleep_Ctrl(false);
    }

    //3초 후 Boot Api를 통해 BT 연결함
    //STATE_03_BOOT_BT_CONNECT
    private void state_03_boot_bt_connect() {

        mState = STATE_03_BOOT_BT_CONNECT;
        Log.d(TAG, "state_03_boot_bt_connect");

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                mBootApi.BT_Connect();
            }
        }, BT_CONNECT_SEC);
    }

    //DFU를 시작한다.
    //STATE_04_BOOTAPI_START_DFU
    private void state_04_bootapi_start_dfu() {
        mState = STATE_04_BOOTAPI_START_DFU;
        Log.d(TAG, "state_04_BOOTAPI_START_DFU");
        mBootApi.BOOT_DFU_Start(VanCode.VAN_DAOUDATA, CBApi.FW_VER_DAOUDATA.getBytes());
    }

    // DFU 백업 요청
    //STATE_05_BOOTAPI_BACKUP
    private void state_05_bootapi_backup(){
        mState = STATE_05_BOOTAPI_BACKUP;
        Log.d(TAG, "state_05_BOOTAPI_BACKUP");
        mBootApi.BOOT_Backup(VanCode.VAN_DAOUDATA);
    }

    // dfu data update
    //STATE_06_BOOTAPI_DFU_DATA
    private void state_06_bootapi_dfu_data() {
        mState = STATE_06_BOOTAPI_DFU_DATA;
        Log.d(TAG, "state_06_bootapi_dfu_data");
        mBootApi.BOOT_DFU_Data(false, "van1daoudata.bin");
    }

    // dfu data 복원
    //STATE_07_BOOTAPI_DFU_RESTORE
    private void state_07_bootapi_dfu_restore() {
        mState = STATE_07_BOOTAPI_DFU_RESTORE;
        Log.d(TAG, "state_07_bootapi_dfu_restore");
        mBootApi.BOOT_DFU_Restore(VanCode.VAN_DAOUDATA);
    }

    //STATE_08_BOOTAPI_BT_DISCONNECT
    private void state_08_bootapi_bt_disconnect() {
        mState = STATE_08_BOOTAPI_BT_DISCONNECT;
        Log.d(TAG, "state_08_bootapi_bt_disconnect");
        mBootApi.BT_Disconnect();
    }

    // STATE_09_DAOUAPI_BT_CONNECT
    private void state_09_daouapi_bt_connect() {

        mState = STATE_09_DAOUAPI_BT_CONNECT;
        Log.d(TAG, "state_09_daouapi_bt_connect");
        mDaouApi.BT_Connect();
    }

    //STATE_10_DAOUAPI_HASH_WRITE
    private void state_10_daouapi_hash_write() {
        mState = STATE_10_DAOUAPI_HASH_WRITE;
        Log.d(TAG, "state_10_daouapi_hash_write");
        mDaouApi.DAOU_sendHashData(false);
    }

    /**
     * 오류 발생시 다시 시도 하는 함수
     */
    private void retry(){
        final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();

        alertDialog.setMessage("업데이트 중 오류가 발생하였습니다. \n" +
                "펌웨어는 이전 상태 입니다. \n" +
                "다시 시도 하시겠습니까?");

        alertDialog.show();

    }

    private void finish(int result) {

        Log.d(TAG, "finish result:"+result);
        Intent intent = new Intent();
        intent.putExtra("result", 1);
        setResult(result, intent);

        finish();
    }

    CBBootApiCallback bootapi_callback = new CBBootApiCallback() {
        @Override
        public void BT_CB(final int result, final String value) {
            runOnUiThread(new Runnable() {
                public void run() {

                    switch (result) {

                        case CBApi.BT_CONNECTED:
                            Log.d(TAG, "BT_CONNECTED bt address:"+value);
                            mBTAddress = value;


                            break;

                        case CBApi.BT_DISCONNECTED:
                            Log.d(TAG, "BT_DISCONNECTED");

                            if(mState == STATE_08_BOOTAPI_BT_DISCONNECT) {
                                new Timer().schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        state_09_daouapi_bt_connect();
                                    }
                                }, BT_CONNECT_SEC);
                            }
                            //    Toast.makeText(FWUpdateActivity.this, "블루투스가 해제되었습니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_SERVICE_INIT_FAIL:
                            Toast.makeText(FWUpdateActivity.this, "블루투스 초기화를 실패하였습니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_INIT_ADAPTER_FAIL:
                            Log.d(TAG, "CBApi.BT_INIT_ADAPTER_FAIL 블루투스를 사용할 수 없습니다.");
                            Toast.makeText(FWUpdateActivity.this, "블루투스를 사용할 수 없습니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_INIT_SETTING_OFF:
                            Toast.makeText(FWUpdateActivity.this, "블루투스 설정을 On으로 변경해 주세요.", Toast.LENGTH_SHORT).show();
                            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivity(enableIntent);
                            break;

                        case CBApi.BT_NOT_UART:
                            Toast.makeText(FWUpdateActivity.this, "블루투스를 지원하지 않습니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_INIT_GPS_PERMISSION:
                            Toast.makeText(FWUpdateActivity.this, "블루투스를 사용하기 위해 GPS 권한이 필요합니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_INIT_GPS_IS_NOT_ON:
                            Toast.makeText(FWUpdateActivity.this, "블루투스를 사용하기 위해 GPS 설정을 ON으로 변경해야 합니다.", Toast.LENGTH_SHORT).show();
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                            break;

                        case CBApi.BT_CANCELED:
                            Toast.makeText(FWUpdateActivity.this, "블루투스가 취소되었습니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_RECONNECTING:
                            Toast.makeText(FWUpdateActivity.this, "블루투스가 재연결 중입니다.", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            });
        }

        @Override
        public void Wakeup_CB(byte result) {
            if(mState == STATE_04_BOOTAPI_START_DFU) {
                state_04_bootapi_start_dfu();
            }
        }

        @Override
        public void Boot_Error_CB(int code) {

        }

        @Override
        public void BOOT_DFU_Start_CB(byte responseCode, byte DFUresultCode) {
            if(responseCode == CBMsg.BT_MSG_RES_OK) {
                //#6
                state_06_bootapi_dfu_data();
            }
            else {
                if(DFUresultCode == CBMsg.CB_DFU_RSLT_NO_BACKUP_IMG) {
                    //#5
                    state_05_bootapi_backup();
                }
                else {
                    Log.d(TAG, "DFU start fail");
                }
            }
        }

        @Override
        public void BOOT_Backup_CB(byte responseCode, byte DFUresultCode) {
            if(responseCode == CBMsg.BT_MSG_RES_OK){
                //#4
                state_04_bootapi_start_dfu();
            }
            else{
                Log.d("TAG", "backup fail");
            }
        }

        @Override
        public void BOOT_DFU_Data_CB(byte responseCode, byte DFUresultCode) {

            if(responseCode == CBMsg.BT_MSG_RES_OK) {
                state_08_bootapi_bt_disconnect();
            }
        }

        @Override
        public void BOOT_DFU_Restore_CB(byte responseCode, byte DFUresultCode) {

            if(responseCode == CBMsg.BT_MSG_RES_OK) {
                //finish
            }
        }

        @Override
        public void BOOT_DFU_BackupClearForTestCB(byte responseCode) {

        }
    };

    CBDaouApiCallback daouapi_callback = new CBDaouApiCallback() {

        @Override
        public void BT_CB(final int result, final String value) {

            runOnUiThread(new Runnable() {
                public void run() {

                    switch (result) {

                        case CBApi.BT_CONNECTED:
                            Log.d(TAG, "BT_CONNECTED bt address:"+value);
                            mBTAddress = value;


                            break;

                        case CBApi.BT_DISCONNECTED:
                            Log.d(TAG, "BT_DISCONNECTED");
                            //    Toast.makeText(FWUpdateActivity.this, "블루투스가 해제되었습니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_SERVICE_INIT_FAIL:
                            Toast.makeText(FWUpdateActivity.this, "블루투스 초기화를 실패하였습니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_INIT_ADAPTER_FAIL:
                            Log.d(TAG, "CBApi.BT_INIT_ADAPTER_FAIL 블루투스를 사용할 수 없습니다.");
                            Toast.makeText(FWUpdateActivity.this, "블루투스를 사용할 수 없습니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_INIT_SETTING_OFF:
                            Toast.makeText(FWUpdateActivity.this, "블루투스 설정을 On으로 변경해 주세요.", Toast.LENGTH_SHORT).show();
                            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivity(enableIntent);
                            break;

                        case CBApi.BT_NOT_UART:
                            Toast.makeText(FWUpdateActivity.this, "블루투스를 지원하지 않습니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_INIT_GPS_PERMISSION:
                            Toast.makeText(FWUpdateActivity.this, "블루투스를 사용하기 위해 GPS 권한이 필요합니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_INIT_GPS_IS_NOT_ON:
                            Toast.makeText(FWUpdateActivity.this, "블루투스를 사용하기 위해 GPS 설정을 ON으로 변경해야 합니다.", Toast.LENGTH_SHORT).show();
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                            break;

                        case CBApi.BT_CANCELED:
                            Toast.makeText(FWUpdateActivity.this, "블루투스가 취소되었습니다.", Toast.LENGTH_SHORT).show();
                            break;

                        case CBApi.BT_RECONNECTING:
                            Toast.makeText(FWUpdateActivity.this, "블루투스가 재연결 중입니다.", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            });




        }

        @Override
        public void Wakeup_CB(final byte result) {
            //Log.d(TAG, "Wakeup_CB");
            //mApi.DAOU_GetHWum();

            runOnUiThread(new Runnable() {
                public void run() {

                    if(mState == STATE_01_DAOU_BT_CONNECT) {
                        state_02_sleep_ctrl();
                    }
                    if(mState == STATE_09_DAOUAPI_BT_CONNECT) {
                        state_10_daouapi_hash_write();
                    }

                }
            });
        }

        @Override
        public void DAOU_GetHWNum_CB(final String hwNum) {


        }

        @Override
        public void DAOU_Read_Serial_CB(final byte result, final byte index, final String van_serial) {


        }

        @Override
        public void DAOU_StartBusness_CB(final ResultStartBusness start) {

        }

        @Override
        public void DAOU_KeyUpdate_CB(final int resultCode, final String msg) {

        }

        @Override
        public void DAOU_Payment_CB(final ResultPayment result) {


        }

        @Override
        public void DAOU_Payment_Incomplete(int resultCode, String msg) {

        }

        @Override
        public void DAOU_OnICInsert() {
        }

        @Override
        public void DAOU_OnICRemove() {
        }

        @Override
        public void DAOU_OnMSRRead() {
        }

        @Override
        public void DAOU_OnFallback(int fallback) {
        }

        @Override
        public void DAOU_OnCardData(final String maskedCardData) {
        }

        @Override
        public void DAOU_Error_CB(final int code) {

            runOnUiThread(new Runnable() {
                public void run() {

                    switch (code) {

                        case CBDaouApi.CODE_NETWORK_CANNOT_ACCESS_ERROR: {
                            final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();

                            alertDialog.setMessage("접속 불가");
                            alertDialog.show();

                            break;
                        }

                        case CBDaouApi.CODE_NETWORK_NO_RESPONSE_ERROR: {
                            final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();

                            alertDialog.setMessage("통신 에러");
                            alertDialog.show();
                        }
                        break;

                        case CBDaouApi.CODE_BT_NOT_CONNECT: {
                            final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();

                            alertDialog.setMessage("블루투스를 연결해 주세요.");
                            alertDialog.show();
                        }
                        break;

                        case CBDaouApi.CODE_PARAM_INPUT: {
                            final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();

                            alertDialog.setMessage("입력 파라미터 오류");
                            alertDialog.show();
                        }
                        break;

                        case CBDaouApi.CODE_PARAM_PORT_NUMBER: {
                            final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();

                            alertDialog.setMessage("유효하지 않은 포트 번호 입니다. ");
                            alertDialog.show();
                        }
                        break;

                        case CBDaouApi.CODE_PROGRAM_ERROR: {
                            final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();

                            alertDialog.setMessage("프로그램 에러");
                            alertDialog.show();
                        }
                        break;

                        case CBDaouApi.CODE_READER_ERROR: {
                            final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();

                            alertDialog.setMessage("리더기 에러");
                            alertDialog.show();
                        }
                        break;

                        case CBDaouApi.CODE_MSR_PROTECTED_ERROR: {
                            final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();

                            alertDialog.setMessage("IC 우선 거래해 주세요.");
                            alertDialog.show();
                        }
                        break;



                        case CBDaouApi.CODE_ROOTING_ERROR: {
                            final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();

                            alertDialog.setMessage("루팅된 단말기는 사용할 수 없습니다.");
                            alertDialog.show();
                        }

//                        case CBDaouApi.CODE_NETWORK_UNCOMPLETE_ERROR: {
//                            final AlertDialog alertDialog = new AlertDialog.Builder(FWUpdateActivity.this).create();
//
//                            alertDialog.setMessage("미완료거래가 되었습니다.");
//                            alertDialog.show();
//                        }
                    }
                }
            });
        }

        @Override
        public void DAOU_CardError_CB(int code) {
        }

        @Override
        public void DAOU_FwVersionCheckCB(String version) {
        }

        @Override
        public void DAOU_Write_HASH_CB(int code) {
            if(code == CBMsg.BT_MSG_RES_OK) {
                finish(0);
            }
        }

        @Override
        public void DAOU_Sleep_Ctrl_CB(byte code) {
            Log.d(TAG, "[02. 1-1 ] DAOU_Sleep_Ctrl_CB code:"+code);
            if(code == 0x00) {
                //#3
            }
            else {
                //#0
            }
        }

        @Override
        public void DAOU_IsFwUpdateCB(boolean result, String currentVersion, String Update) {

        }

        @Override
        public void DAOU_VerifyFW_CB(boolean result, final String msg, int result_code) {
        }

        @Override
        public void DAOU_Need_StartBusness() {
        }

    };
    public void mOnClick(View view){
        switch(view.getId()){
            case R.id.btn_fwupdate_bt_connect:
                    mDaouApi.BT_Connect();
                break;

            case R.id.btn_fwupdate_bt_disconnect:
                    mDaouApi.BT_Disconnect();
                break;
            case R.id.btn_fwupdate_start:
                // 버튼 눌렸을 때
                if(mDaouApi.BT_IsConnect() == true) {
                    DFU_BTCoonectCheck();
                }
                else
                {
                    mDaouApi.BT_Connect(mBTAddress);
                }

                break;
        }
    }
    public void DFU_BTCoonectCheck(){
        LOG.d(TAG,"1. Daou BT check");
        if(mDaouApi.BT_IsConnect() == true)
        {
            mDaouApi.DAOU_Sleep_Ctrl(false);
        }
        else{
            LOG.d(TAG, "1. daou bt not connect");
        }
    }
}

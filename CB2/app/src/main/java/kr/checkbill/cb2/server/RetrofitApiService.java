package kr.checkbill.cb2.server;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by kwonhyeokjin on 2017. 2. 20..
 */

public interface RetrofitApiService {
    String API_URL = "http://cms.checkbill.xyz/";

    /**
     * 결제내역을 저장하는 API
     * @param body JSON값
     * @return
     */
    @PUT("api/set_reader_id")
    Call<ReaderIdResponse> setReaderIdApi(@Body String body);

    @PUT("api/set_orders")
    Call<ResponseBody> setOrdersApi(@Body String body);

    @PUT("api/get_Orders")
    Call<GetOrdersResponse> getOrdersApi(@Body String body);
}

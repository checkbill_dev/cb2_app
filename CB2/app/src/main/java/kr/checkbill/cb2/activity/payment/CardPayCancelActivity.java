package kr.checkbill.cb2.activity.payment;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.checkbill.cbapi.ResultPayment;

import kr.checkbill.cb2.activity.base.DaouPayActivity;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.SignPadActivity;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.Util;

public class CardPayCancelActivity extends DaouPayActivity
        implements View.OnClickListener, TextWatcher {
    private final static String TAG = "CreditCardPaymentCancelActivity";
    private Button btn_ok, btn_ok_tint;
    private CheckBox cb_eullyeon;

    private String monthVal;
    private PaymentHistory paymentHistory;
    private String paymenyHistory_id;
    private String totalVal;
    private final int REQUEST_CODE = 0;
    private String resultDate;
    private boolean isIC; // IC카드일때 자동취소
    //////////////////////////////////////
    //                 싸인       비밀번호  //
    //은련O, 5만초과       O          O    //
    //은련O, 5만이하       O          O    //
    //                                  //
    //은련X, 5만초과       O          X    //
    //은련X, 5만이하       X          X    //
    //////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_pay_cancel);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok_tint = (Button) findViewById(R.id.btn_ok_tint);
        cb_eullyeon = (CheckBox) findViewById(R.id.cb_eullyeon);

        registerOnClickListener();
        registerTextWatcher();

        DBHelper mDB = new DBHelper(this);
        paymentHistory = new PaymentHistory();
        paymenyHistory_id = getIntent().getStringExtra("paymentId");

        String where = "_id = '" + paymenyHistory_id + "'";
        Cursor paymentHistoryCursor = mDB.getPaymentHistoryCursorWhere(where);
        while (paymentHistoryCursor.moveToNext()){
            paymentHistory.fetch(paymentHistoryCursor);
        }


        String month; // 화면에 표시용 변수

        //할부개월이 공백 or 0 or 1 일때 일시불로바꾼다.
        if (paymentHistory.monthVal == null
                || (paymentHistory.monthVal).isEmpty()
                || (paymentHistory.monthVal.equals("0"))
                || (paymentHistory.monthVal.equals("1"))){
            month = "일시불";
            monthVal = "0";
        }
        else {
            month = paymentHistory.monthVal;
            monthVal = paymentHistory.monthVal;
        }
        // yyyy-MM-dd HH:mm:ss yyyyMMdd형식으로 바꿔준다.
        String[] splitSpace = (paymentHistory.approvalDate).split(" ");
        String date = splitSpace[0];
        resultDate = date.replace("-", ""); // 이거는 결제취소 매개변수로 사용


        SetText(R.id.txt_approval_date, paymentHistory.approvalDate);
        SetText(R.id.txt_amount, Util.commaFormat(paymentHistory.totalVal));
        SetText(R.id.txt_month, month);


        if (paymentHistory.eullyeon.equals(PaymentHistory.EULLYEON_ON)){
            cb_eullyeon.setVisibility(View.VISIBLE);
            cb_eullyeon.setChecked(true);
        }
        else if (paymentHistory.eullyeon.equals(PaymentHistory.EULLYEON_NOT)) {
            cb_eullyeon.setVisibility(View.GONE);
            cb_eullyeon.setChecked(false);
        }




    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_card_number_clear:
                SetText(R.id.txt_card_number, "");
                break;
            case R.id.btn_ok:
                totalVal = (paymentHistory.totalVal).replace(",", "");


                if (cb_eullyeon.isChecked()) { // 은련카드일때는 50,000 원 초과 검사를 하지않는다.(무조건 싸인을 받음)
                    Intent intent = new Intent(this, SignPadActivity.class);
                    intent.putExtra("Amount", GetText(R.id.edit_input_val));
                    intent.putExtra("IsEullyeon", "eullyeon_yes");
                    startActivityForResult(intent, REQUEST_CODE);
                }
                else {
                    if (Util.parseLong(totalVal, 0) > 50000) {
                        Intent intent = new Intent(this, SignPadActivity.class);
                        intent.putExtra("Amount", GetText(R.id.edit_input_val));
                        intent.putExtra("IsEullyeon", "eullyeon_no");
                        startActivityForResult(intent, REQUEST_CODE);
                    }
                    else {
                        DAOU_Credit_Cancel(false, PaymentHistory.TRANTYPE_CREDIT_CARD_CANCEL, null, null, totalVal, paymenyHistory_id,
                                monthVal, resultDate, paymentHistory.approvalNum, 0);
                    }
                }

                break;

        }
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(GetText(R.id.txt_card_number).length()>=1){
            btn_ok.setVisibility(View.VISIBLE);
            btn_ok_tint.setVisibility(View.GONE);
        }
        else{
            btn_ok.setVisibility(View.GONE);
            btn_ok_tint.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void BT_CB(final int resultCode, String BTAdress) {
        super.BT_CB(resultCode, BTAdress);
    }

    @Override
    public void DAOU_Payment_CB(final ResultPayment resultPayment) {
        super.DAOU_Payment_CB(resultPayment);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (resultPayment.resultCode != 0) {
                    SetText(R.id.txt_card_number, "");
                }
            }
        });
    }

    @Override
    public void DAOU_OnICInsert() {
        super.DAOU_OnICInsert();
        isIC = true;
    }

    @Override
    public void DAOU_OnMSRRead() {
        super.DAOU_OnMSRRead();
        isIC = false;
    }

    @Override
    public void DAOU_OnICRemove() {
        super.DAOU_OnICRemove();
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void DAOU_OnFallback(final int fallback) {
        super.DAOU_OnFallback(fallback);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });

    }

    @Override
    public void DAOU_OnCardData(final String maskedCarData) {
        super.DAOU_OnCardData(maskedCarData);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "****-####-####-****");
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.INVISIBLE);

                if (getSystemIcAutoPay().equals("1")) {
                    if (isIC) {
                        DAOU_Credit_Cancel(false, PaymentHistory.TRANTYPE_CREDIT_CARD_CANCEL, null, null, totalVal, paymenyHistory_id,
                                monthVal, resultDate, paymentHistory.approvalNum, 0);
                    }
                }
            }
        });
    }

    @Override
    public void DAOU_Error_CB(final int code) {
        super.DAOU_Error_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }
    @Override
    public void DAOU_Payment_Incomplete(int resultCode, String message) {
        super.DAOU_Payment_Incomplete(resultCode, message);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }
		
    @Override
    public void DAOU_CardError_CB(int code){
        super.DAOU_CardError_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        switch (requestCode){
            case REQUEST_CODE:
                if (resultCode == RESULT_OK){
                    byte[] signByteArray = data.getByteArrayExtra("SignData");
                    String eullyeon = data.getStringExtra("IsEullyeon");
                    String password = data.getStringExtra("Password");
                    LOG.d(TAG, "password: " + password);

                    Bitmap bitmap = BitmapFactory.decodeByteArray(signByteArray, 0, signByteArray.length);

                    if (eullyeon.equals("eullyeon_yes")) {
                        DAOU_Credit_Cancel(false, PaymentHistory.TRANTYPE_CREDIT_CARD_CANCEL, password, bitmap, totalVal, paymenyHistory_id,
                                monthVal, resultDate, paymentHistory.approvalNum, 0);
                    }
                    else {
                        DAOU_Credit_Cancel(false, PaymentHistory.TRANTYPE_CREDIT_CARD_CANCEL, null, bitmap, totalVal, paymenyHistory_id,
                                monthVal, resultDate, paymentHistory.approvalNum, 0);
                    }
                }
                else if (resultCode == RESULT_CANCELED){
                    finish();
                    showToast("결제가 취소 되었습니다.");
                }
                break;
        }
    }
    private void registerOnClickListener() {
        (findViewById(R.id.img_back)).setOnClickListener(this);
        (findViewById(R.id.btn_card_number_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }
    private void registerTextWatcher() {
        ((TextView) findViewById(R.id.txt_card_number)).addTextChangedListener(this);
    }

    /**
     * 안쓰는 메소드들
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void afterTextChanged(Editable s) {
    }
}

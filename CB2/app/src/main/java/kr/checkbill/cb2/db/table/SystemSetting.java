package kr.checkbill.cb2.db.table;

/**
 * Created by kwonhyeokjin on 2017. 1. 10..
 */

/*
CREATE TABLE SYSTEM_SETTING(
    PASSWORD     text,
    USE_PRINT    integer,
    PAPER_CNT    integer
);
 */
public class SystemSetting {
    public static final String TABLE_NAME="SYSTEM_SETTING";
    public static final String PASSWORD="PASSWORD";
    public static final String VAN_SERIAL="VAN_SERIAL";
    public static final String PAPER_CNT="PAPER_CNT";
    public static final String SW_AUTH_NUM="SW_AUTH_NUM";
    public static final String USE_TID="USE_TID"; // 현재 사용중인 단말기 번호,, 이값이 0이라면 개시거래된 사업자가 없다는뜻이다.
    public static final String MAIN_TID="USE_TID"; // 주사업자의 단말기 번호,, 이값이 0이라면 개시거래된 사업자가 없다는뜻이다.
    public static final String BT_MAINTAIN = "BT_MAINTAIN"; // 0:블루투스연결유지안함 1:블루투스연결유지함
    public static final String EULLYEON_USE = "EULLYEON_USE"; // 0:은련카드결제사용안함 1:은련카드결제사용함
    public static final String BT_AUTO_CONNECT = "BT_AUTO_CONNECT"; // 블루투스 자동연결여부 0:자동연결사용안함 1:자동연결사용함
    public static final String RCT_BT_NUM = "RCT_BT_NUM"; // 최근에 연결했던 블루투스 리더기의 주소
    public static final String IC_AUTO_PAY = "IC_AUTO_PAY"; // 0:자동결제 사용안함  1:자동결제 사용함
}

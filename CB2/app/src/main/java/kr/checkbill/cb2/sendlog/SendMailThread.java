package kr.checkbill.cb2.sendlog;

import android.content.Context;

import kr.checkbill.cb2.util.LOG;

public class SendMailThread extends Thread {

	Context mContext;
	String mMailSubject = "";  // 메일제목
	String mMailBody = "";    // 메일본문
	
	private final static String TAG = "SendMailThread";
	/**
	 * Context를 설정한다.
	 * @param context context
	 */
	public void setContext(Context context) {
		mContext = context;
	}
	
	/**
	 * 메일의 내용을 설정한다.
	 * @param mail_subject 메일제목
	 * @param mail_body 메일본문
	 */
	public void setMail(String mail_subject, String mail_body) {
		mMailSubject = mail_subject;
		mMailBody = mail_body;
	}
	
	@Override
	public void run () {
		LOG.d(TAG, "Thead");
		
		SendLogUtil.saveLog(mContext);
		SendLogUtil.zip(mContext);
		
		GMailSender sender = new GMailSender(SendLogUtil.getMailAddress(), SendLogUtil.getMailPassword());
		try {
			sender.sendMail(mMailSubject, mMailBody, SendLogUtil.getMailAddress(), SendLogUtil.getMailAddress(), SendLogUtil.getFilePath(mContext), "/data/data/com.checkbill.tpp.checkpos.daudata/files/"+"dau_log.txt");
		} catch (Exception e) {
			LOG.d("SendLogActivity", "Exception e"+e.toString());
		}
	}
}

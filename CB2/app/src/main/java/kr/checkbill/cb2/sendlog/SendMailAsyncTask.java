package kr.checkbill.cb2.sendlog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import kr.checkbill.cb2.util.LOG;

public class SendMailAsyncTask extends AsyncTask<Void, Void, Integer> {
	
	Context mContext;
	Activity mActivity;
	ProgressDialog mDialog;
	
	String mMailSubject = "";  // 메일제목
	String mMailBody = "";    // 메일본문
	
	private final static String TAG = "SendMailAsyncTask";
	
	/**
	 * Context를 설정한다.
	 * @param context context
	 */
	public void setContext(Context context) {
		mContext = context;
	}
	
	public void setActivity(Activity act) {
		mActivity = act;
	}
	
	/**
	 * 프로그래스 화면의 문구를 설정한다.
	 * @param message 문구
	 */
	public void setProgress(String message) {
		mDialog = new ProgressDialog(mContext);
		mDialog.setMessage(message);
		mDialog.setIndeterminate(true);
		mDialog.setCancelable(false);
		//mDialog.show();
	}
	
	/**
	 * 메일의 내용을 설정한다.
	 * @param mail_subject 메일제목
	 * @param mail_body 메일본문
	 */
	public void setMail(String mail_subject, String mail_body) {
		mMailSubject = mail_subject;
		mMailBody = mail_body;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		LOG.d(TAG, "onPreExecute");
		if(mDialog != null) {
			mDialog.show();
		}
	}
	
	@Override
	protected Integer doInBackground(Void... params) {
		int nResult = 0;
		
		LOG.d(TAG, "doInBackground");
		
		SendLogUtil.saveLog(mContext);
		SendLogUtil.zip(mContext);
		
		GMailSender sender = new GMailSender(SendLogUtil.getMailAddress(), SendLogUtil.getMailPassword());
		try {
			sender.sendMail(mMailSubject, mMailBody, SendLogUtil.getMailAddress(), SendLogUtil.getMailAddress(), SendLogUtil.getZipFilePath(mContext), "/data/data/com.checkbill.tpp.checkpos.daudata/files/"+"dau_log.txt");
		} catch (Exception e) {
			LOG.d("SendLogActivity", "Exception e"+e.toString());
			nResult = 1;
		}

		return nResult;
	}

	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		
		LOG.d(TAG, "onPostExecute");
		
		if(mDialog != null) {
			mDialog.dismiss();
			LOG.d(TAG, "onPostExecute mDialog.dismiss();");
		}

		if(mContext != null) {
			LOG.d(TAG, "onPostExecute result:"+result);
			if(result==0) {
				Toast.makeText(mContext, "메일 보내기 성공", Toast.LENGTH_SHORT).show();
				if(mActivity != null) {
					mActivity.finish();
				}
			}
			else {
				Toast.makeText(mContext, "메일 보내기 실패", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
}

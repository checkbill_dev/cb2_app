package kr.checkbill.cb2.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.ByteArrayOutputStream;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.base.BaseActivity;
import kr.checkbill.cb2.util.LOG;

public class SignPadActivity extends BaseActivity
        implements View.OnClickListener, SignaturePad.OnSignedListener {
    private final static String TAG = "SignPadActivity";
    private Button btn_ok, btn_ok_tint;
    private SignaturePad ll_landscape_sign_pad;// 정점 하나에 대한 정보를 가지는 클래스

    private String eullyeon;
    private final int REQUEST_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_pad);

        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok_tint = (Button) findViewById(R.id.btn_ok_tint);
        ll_landscape_sign_pad = (SignaturePad) findViewById(R.id.ll_landscape_sign_pad);

        String amount = getIntent().getStringExtra("Amount");
        eullyeon = getIntent().getStringExtra("IsEullyeon");
        SetText(R.id.txt_amount, amount);

        registerOnClickListener();
        registerOnSignedListener();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                if (eullyeon.equals("eullyeon_yes")){
                    Intent intent = new Intent(this, PasswordActivity.class);
                    startActivityForResult(intent, REQUEST_CODE);
//                    final LinearLayout linear_dialog = (LinearLayout) View.inflate(SignPadActivity.this, R.layout.dialog_input_pw,null);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(SignPadActivity.this);
//                    builder.setView(linear_dialog);
//                    builder.setPositiveButton("확인", new DialogInterface.OnClickListener(){
//                                public void onClick(DialogInterface dialog, int whichButton){
//                                    if (GetText(linear_dialog, R.id.edit_pw).length() == 6){
//                                        Intent intent = new Intent();
//                                        Bitmap signatureBitmap = ll_landscape_sign_pad.getSignatureBitmap();
//                                        byte[] byteArray = convertBitmapToByteArray(signatureBitmap);
//                                        intent.putExtra("IsEullyeon", "eullyeon_yes");
//                                        intent.putExtra("SignData", byteArray);
//                                        intent.putExtra("Password", GetText(linear_dialog, R.id.edit_pw));
//                                        setResult(RESULT_OK, intent);
//                                        finish();
//                                    }
//                                    else {
//                                        showToast("비밀번호를 여섯자리로 입력 해주세요.");
//                                    }
//                                }
//                            });
//                    AlertDialog dialog = builder.create();
//                    dialog.show();
                }
                else if (eullyeon.equals("eullyeon_no")){
                    Intent intent = new Intent();
                    Bitmap signatureBitmap = ll_landscape_sign_pad.getSignatureBitmap();
                    byte[] signByteArray = convertBitmapToByteArray(signatureBitmap);
                    intent.putExtra("SignData", signByteArray);
                    intent.putExtra("IsEullyeon", "eullyeon_no");
                    intent.putExtra("Password", "noPassword");
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
        }
    }

    /**
     * 싸인이 시작될때 호출되는 메소드
     */
    @Override
    public void onStartSigning() {
        LOG.d(TAG, "onStartSigning()");
        btn_ok.setVisibility(View.VISIBLE);
        btn_ok_tint.setVisibility(View.GONE);
    }
    /**
     * 싸인 끝났을떄 호출되는 메소드
     */
    @Override
    public void onSigned() {
    }
    @Override
    public void onClear() {
    }

    private void registerOnClickListener() {
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }
    private void registerOnSignedListener() {
        ll_landscape_sign_pad.setOnSignedListener(this);
    }

    private byte[] convertBitmapToByteArray(Bitmap bitmap){
        ByteArrayOutputStream stram = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stram);
        byte[] byteArray = stram.toByteArray();
        return byteArray;
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        switch (requestCode){
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String password = data.getStringExtra("Password");
                    Intent intent = new Intent();
                    Bitmap signatureBitmap = ll_landscape_sign_pad.getSignatureBitmap();
                    byte[] signByteArray = convertBitmapToByteArray(signatureBitmap);
                    intent.putExtra("SignData", signByteArray);
                    intent.putExtra("IsEullyeon", "eullyeon_yes");
                    intent.putExtra("Password", password);

                    setResult(RESULT_OK, intent);
                    finish();
                }
                else {
                    Intent intent = new Intent();
                    setResult(RESULT_CANCELED, intent);
                    finish();
                }
                break;
        }
    }
}


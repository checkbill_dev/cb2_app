package kr.checkbill.cb2.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import kr.checkbill.cb2.activity.base.BaseActivity;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.util.LOG;

public class SplashActivity extends BaseActivity {
    private static final String TAG = "SplashActivity";
    public static int DURATION_FIRST = 1000;//첫번째 애니메이션의 지속시간
    public static int DURATION_SECOND = 200;//두번째 애니메이션의 지속시간

    private Animation mAniUpper;
    private Animation mAniUnder;

    boolean loadingIsFinish = false;
    boolean initDBIsFinish = false;
    private DBHelper mDB;
    class InitDB extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String result = mDB.initDB(SplashActivity.this);

            if(result.equals("success")){
                LOG.i("디비결과","success");
                initDBIsFinish = true; // 디비 초기화결과 성공
                if (loadingIsFinish == true){


                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
            else {
                showAlertDialog("초기화 오류", new String[]{"데이터베이스 초기화중 문제가 발생하였습니다.\n제조사에 문의해주세요."}, true,
                        true, false, false, "확인", null, null, null, null, null);

                LOG.i(TAG,"데이터베이스 초기화중 문제가 발생하였습니다.\n오류내용:\n"+result);
            }

        }
    }
    class SecondEndHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            ImageView image_upper_logo = (ImageView)findViewById(R.id.image_upper_logo);
            ImageView image_under_logo = (ImageView)findViewById(R.id.image_under_logo);

            image_upper_logo.setVisibility(View.INVISIBLE);
            image_under_logo.setVisibility(View.INVISIBLE);

            loadingIsFinish = true; // 로딩창결과 성공
            LOG.i("로딩결과","success");
            if (initDBIsFinish == true){
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                LOG.i("로딩에서시작","로딩에서시작");
            }
        }

    }

    class FirstEndHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            try {
                Thread.sleep(800);//첫번째 애니메이션이 끝나고 두번째 애니메이션의 시작하기 까지의 시간
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            ImageView image_upper_logo = (ImageView)findViewById(R.id.image_upper_logo);
            ImageView image_under_logo = (ImageView)findViewById(R.id.image_under_logo);

            int i_val = findViewById(R.id.layoutBase).getHeight() / 2;

            mAniUpper = new TranslateAnimation(0, 0, 0, -i_val);
            mAniUpper.setDuration(DURATION_SECOND);
            image_upper_logo.setAnimation(mAniUpper);

            mAniUnder = new TranslateAnimation(0, 0, 0, i_val);
            mAniUnder.setDuration(DURATION_SECOND);
            image_under_logo.setAnimation(mAniUnder);

            SecondEndHandler handler = new SecondEndHandler();
            handler.sendEmptyMessageDelayed(0, DURATION_SECOND);


        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        ImageView image_upper_logo = (ImageView)findViewById(R.id.image_upper_logo);
        ImageView image_under_logo = (ImageView)findViewById(R.id.image_under_logo);

        mDB = new DBHelper(this);


        mAniUpper = new TranslateAnimation(0, 0, 500, 0);
        mAniUpper.setDuration(DURATION_FIRST);
        image_upper_logo.setAnimation(mAniUpper);

        mAniUnder = new TranslateAnimation(0, 0, -500, 0);
        mAniUnder.setDuration(DURATION_FIRST);
        image_under_logo.setAnimation(mAniUnder);

//        InitApp initApp = new InitApp();
//        initApp.execute();

        FirstEndHandler handler = new FirstEndHandler();
        handler.sendEmptyMessageDelayed(0, DURATION_FIRST);

        InitDB initDB = new InitDB();
        initDB.sendEmptyMessage(0);


    }

}

package kr.checkbill.cb2.activity.listview;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.checkbill.cb2.activity.base.BaseActivity;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.util.Util;

/**
 * Created by kwonhyeokjin on 2017. 1. 17..
 */

public class PayListAdapter extends BaseAdapter {
    private static final String TAG ="PaymentListAdapter";
    private ArrayList<PayListItem> payListItemArrayList = new ArrayList<PayListItem>();
    private PayListItem payListItem;
    @Override
    public int getCount() {
        return payListItemArrayList.size();
    }

    @Override
    public PayListItem getItem(int position) {
        return payListItemArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Context context = parent.getContext();

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_item_payment, parent, false);
        }
        BaseActivity bs = new BaseActivity();
        payListItem = payListItemArrayList.get(position);
        bs.SetText((LinearLayout) convertView, R.id.txt_date, payListItem.approvalDate);
        bs.SetText((LinearLayout) convertView, R.id.txt_tran_type, payListItem.tranType);
        bs.SetText((LinearLayout) convertView, R.id.txt_total_val, Util.commaFormat(payListItem.totalVal) + "원");
        bs.SetText((LinearLayout) convertView, R.id.txt_shop_name, payListItem.shopName);
        bs.SetText((LinearLayout) convertView, R.id.txt_approval_num, payListItem.approvalNum);
        if (payListItem.cardName.equals("")) {
            (convertView.findViewById(R.id.txt_card_name)).setVisibility(View.GONE);
        }
        else {
            bs.SetText((LinearLayout) convertView, R.id.txt_card_name, payListItem.cardName);
        }
        if (payListItem.canceled.equals("1")){
            String red = "#FF0000";
            ((TextView)convertView.findViewById(R.id.txt_date)).setTextColor(Color.parseColor(red));
            ((TextView)convertView.findViewById(R.id.txt_tran_type)).setTextColor(Color.parseColor(red));
            ((TextView)convertView.findViewById(R.id.txt_total_val)).setTextColor(Color.parseColor(red));
            ((TextView)convertView.findViewById(R.id.txt_shop_name)).setTextColor(Color.parseColor(red));
            ((TextView)convertView.findViewById(R.id.txt_card_name)).setTextColor(Color.parseColor(red));
            ((TextView)convertView.findViewById(R.id.txt_approval_num)).setTextColor(Color.parseColor(red));
        }
        else {
            String black = "#000000";
            ((TextView)convertView.findViewById(R.id.txt_date)).setTextColor(Color.parseColor(black));
            ((TextView)convertView.findViewById(R.id.txt_tran_type)).setTextColor(Color.parseColor(black));
            ((TextView)convertView.findViewById(R.id.txt_total_val)).setTextColor(Color.parseColor(black));
            ((TextView)convertView.findViewById(R.id.txt_shop_name)).setTextColor(Color.parseColor(black));
            ((TextView)convertView.findViewById(R.id.txt_card_name)).setTextColor(Color.parseColor(black));
            ((TextView)convertView.findViewById(R.id.txt_approval_num)).setTextColor(Color.parseColor(black));
        }
        return convertView;
    }
    public void addItem(String _id, String tranType, String cardName, String totalVal, String approvalNum, String approvalDate, String shopName, String canceled){
        PayListItem item = new PayListItem();
        item._id = _id;
        if (canceled.equals("0")){
            switch (tranType){
                case PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE: // 지출증빙
                    item.tranType = PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE_SHOW_OO;
                    break;
                case PaymentHistory.TRANTYPE_CASH_VOLUNTARY: // 자진발급
                    item.tranType = PaymentHistory.TRANTYPE_CASH_VOLUNTARY_SHOW_OO;
                    break;
                case PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION: // 소득공제
                    item.tranType = PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION_SHOW_OO;
                    break;
                case PaymentHistory.TRANTYPE_CASH_DEFAULT: // 현금영수증없는 현금결제
                    item.tranType = PaymentHistory.TRANTYPE_CASH_DEFAULT_SHOW_OO;
                    break;
                case PaymentHistory.TRANTYPE_CREDIT_CARD: // 카드
                    item.tranType = PaymentHistory.TRANTYPE_CREDIT_CARD_SHOW_OO;
                    break;
            }
        }
        else if (canceled.equals("1")) {
            switch (tranType) {
                case PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE: // 지출증빙
                    item.tranType = PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE_CANCEL_SHOW_OX;
                    break;
                case PaymentHistory.TRANTYPE_CASH_VOLUNTARY: // 자진발급
                    item.tranType = PaymentHistory.TRANTYPE_CASH_VOLUNTARY_CANCEL_SHOW_OX;
                    break;
                case PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION: // 소득공제
                    item.tranType = PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION_CANCEL_SHOW_OX;
                    break;
                case PaymentHistory.TRANTYPE_CASH_DEFAULT: // 현금영수증없는 현금결제
                    item.tranType = PaymentHistory.TRANTYPE_CASH_DEFAULT_CANCEL_SHOW_OX;
                    break;
                case PaymentHistory.TRANTYPE_CREDIT_CARD: // 카드
                    item.tranType = PaymentHistory.TRANTYPE_CREDIT_CARD_CANCEL_SHOW_OX;
                    break;
                case PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE_CANCEL: // 지출증빙취소
                    item.tranType = PaymentHistory.TRANTYPE_CASH_EXPENSE_EVIDENCE_CANCEL_SHOW_XX;
                    break;
                case PaymentHistory.TRANTYPE_CASH_VOLUNTARY_CANCEL: // 자진발급취소
                    item.tranType = PaymentHistory.TRANTYPE_CASH_VOLUNTARY_CANCEL_SHOW_XX;
                    break;
                case PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION_CANCEL: // 소득공제취소
                    item.tranType = PaymentHistory.TRANTYPE_CASH_INCOME_DEDUCTION_CANCEL_SHOW_XX;
                    break;
                case PaymentHistory.TRANTYPE_CASH_DEFAULT_CANCEL: // 현금영수증없는 현금결제취소
                    item.tranType = PaymentHistory.TRANTYPE_CASH_DEFAULT_CANCEL_SHOW_XX;
                    break;
                case PaymentHistory.TRANTYPE_CREDIT_CARD_CANCEL: // 카드취소
                    item.tranType = PaymentHistory.TRANTYPE_CREDIT_CARD_CANCEL_SHOW_XX;
                    break;
            }
        }
        item.cardName = cardName;
        item.totalVal = totalVal;
        item.approvalNum = approvalNum;
        item.approvalDate = approvalDate;
        item.shopName = shopName;
        item.canceled = canceled;
        payListItemArrayList.add(item);
    }
}

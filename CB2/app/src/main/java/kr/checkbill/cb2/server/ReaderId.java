package kr.checkbill.cb2.server;

/**
 * Created by kwonhyeokjin on 2017. 2. 21..
 */

public class ReaderId {
    String shop_id;
    String biz_num;
    String shop_name;
    String shop_tel;
    String shop_ceo;
    String shop_address;
    String tid;
    String save_flag;

    public ReaderId(String shop_id, String biz_num, String shop_name,
                    String shop_tel, String shop_ceo, String shop_address, String tid, String save_flag) {
        this.shop_id = shop_id;
        this.biz_num = biz_num;
        this.shop_name = shop_name;
        this.shop_tel = shop_tel;
        this.shop_ceo = shop_ceo;
        this.shop_address = shop_address;
        this.tid = tid;
        this.save_flag = save_flag;
    }
}

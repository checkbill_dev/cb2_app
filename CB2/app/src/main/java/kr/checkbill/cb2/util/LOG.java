package kr.checkbill.cb2.util;

import android.util.Log;

import com.checkbill.cbapi.BuildConfig;

/**
 * Created by kwonhyeokjin on 2017. 3. 13..
 */

public class LOG {

    private static boolean debugLog = BuildConfig.DEBUG;

    public static void i(String _tag, String _msg){
        if(debugLog){
            Log.i(_tag,_msg);
        }
    }

    public static void e(String _tag, String _msg){
        if(debugLog){
            Log.e(_tag,_msg);
        }
    }

    public static void e(String _tag, String _msg, Throwable _tr){
        if(debugLog){
            Log.e(_tag,_msg,_tr);
        }
    }

    public static void v(String _tag, String _msg){
        if( debugLog){
            Log.v(_tag,_msg);
        }
    }

    public static void d(String _tag, String _msg){
        if(debugLog){
            Log.d(_tag,_msg);
        }
    }

    public static void w(String _tag, String _msg){
        if(debugLog){
            Log.w(_tag,_msg);
        }
    }
}

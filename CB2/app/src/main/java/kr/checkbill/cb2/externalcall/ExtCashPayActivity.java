package kr.checkbill.cb2.externalcall;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.checkbill.cbapi.CBApi;
import com.checkbill.cbapi.ResultPayment;

import java.text.NumberFormat;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.base.DaouPayActivity;
import kr.checkbill.cb2.util.LOG;

public class ExtCashPayActivity extends DaouPayActivity
        implements View.OnClickListener, TextWatcher, View.OnFocusChangeListener{
    private static final String TAG = "ExtCashReceiptPublishActivity";

    private EditText edit_cash_receipt_number;
    private Button btn_ok, btn_ok_tint;

    private boolean isKeyIn = true; // 직접 입력시 true, 현금 영수증 카드 이용시 false
    private String tid;
    private int totalVal;
    private int originVal;
    private int vatVal;
    private int serviceVal;
    private String type;
    private String id;
    private String password;
    private String packageName;

    private boolean isOnCardLeading = false;
    private String cardData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ext_cash_pay);

        edit_cash_receipt_number = (EditText) findViewById(R.id.edit_cash_receipt_number);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok_tint = (Button) findViewById(R.id.btn_ok_tint);

        tid = getIntent().getStringExtra("tid");
        totalVal = getIntent().getIntExtra("totalVal", -1);
        originVal = getIntent().getIntExtra("originVal", -1);
        vatVal = getIntent().getIntExtra("vatVal", -1);
        serviceVal = getIntent().getIntExtra("serviceVal", -1);
        type = getIntent().getStringExtra("type"); // 거래구분 "1":소득공제 "2":지출증빙 "3":자진발급
        id = getIntent().getStringExtra("id");
        password = getIntent().getStringExtra("password");
        packageName = getIntent().getStringExtra("packageName");

        LOG.d(TAG, "tid : " + tid);
        LOG.d(TAG, "totalVal : " + totalVal);
        LOG.d(TAG, "originVal : " + originVal);
        LOG.d(TAG, "vatVal : " + vatVal);
        LOG.d(TAG, "serviceVal : " + serviceVal);
        LOG.d(TAG, "type : " + type);
        LOG.d(TAG, "id : " + id);
        LOG.d(TAG, "password : " + password);
        LOG.d(TAG, "packageName : " + packageName);

        SetText(R.id.txt_sw_auth_num, DAOU_GetSWNum());

        registerOnClickListener();
        registerTextWatcher();
        registerOnFocusChangeListener();

        NumberFormat nf = NumberFormat.getInstance();
        String commaTotalVal = nf.format(totalVal);
        SetText(R.id.txt_amount, commaTotalVal);

        switch (type) {
            case "1":
                SetText(R.id.txt_type, "소득공제");
                break;
            case "2":
                SetText(R.id.txt_type, "지출증빙");
                break;
            case "3":
                SetText(R.id.txt_type, "자진발급");
                break;
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cash_receipt_number_clear:
                SetText(R.id.edit_cash_receipt_number, "");
                break;
            case R.id.btn_ok:
                cardData = GetText(R.id.edit_cash_receipt_number);
                DAOU_Cash(type, String.valueOf(totalVal), String.valueOf(originVal), String.valueOf(vatVal), String.valueOf(serviceVal), type,
                        isKeyIn, cardData);
                break;
        }
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (edit_cash_receipt_number.hasFocus()) {
            if (isOnCardLeading) {
                // MSR이 긁힌 경우에 텍스트가 변경되었을때는 isKeyIn을 false로 변경
                isKeyIn = false;
            }
            else {
                // EditText에 포커스가 간다음에 텍스트가 변경되었을때는 isKeyIn을 true로 변경
                isKeyIn = true;
                if (isKeyIn) {
                    edit_cash_receipt_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
                }
            }
            if(GetText(R.id.edit_cash_receipt_number).length()>=1){
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }
        }
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.edit_cash_receipt_number:
                if(hasFocus){//포커스를 얻을때
                    isOnCardLeading = false;
                    if(isKeyIn == true) {
                        SetText(R.id.edit_cash_receipt_number, "");
                    }
                }
                else {
                    edit_cash_receipt_number.setBackgroundResource(R.drawable.gray_radius);
                    if(isKeyIn == true) {
                        cardData = GetText(R.id.edit_cash_receipt_number).toString();

                        StringBuilder sb = new StringBuilder();
                        for(int i=0; i<cardData.length(); i++) {
                            if(i>=3 && i<8 ) {
                                sb.append("*");
                            }
                            else {
                                sb.append(cardData.charAt(i));
                            }
                        }
                        SetText(R.id.edit_cash_receipt_number, "");

                    }
                }
                break;
        }
    }

    @Override
    public void BT_CB(final int resultCode, String BTAdress) {
        super.BT_CB(resultCode, BTAdress);
        runOnUiThread(new Runnable() {
            public void run() {
                switch (resultCode) {
                    case CBApi.BT_DISCONNECTED:
                        SetText(R.id.txt_hw_auth_num, "");
                        break;
                    case CBApi.BT_CANCELED:
                        Intent intent = new Intent();
                        intent.putExtra("message", "블루투스가 취소 되었습니다.");
                        setResult(RESULT_CANCELED, intent);
                        finish();
                        break;
                }
            }
        });
    }

    @Override
    public void DAOU_GetHWNum_CB(final String hwNum) {
        super.DAOU_GetHWNum_CB(hwNum);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(hwNum != null) {
                    SetText(R.id.txt_hw_auth_num, hwNum);
                }
            }
        });
    }



    @Override
    public void DAOU_Payment_CB(final ResultPayment resultPayment) {
        super.DAOU_Payment_CB(resultPayment);
        runOnUiThread(new Runnable() {
            public void run() {
                if (resultPayment == null){
                    LOG.e(TAG, "resultPayment is NULL");
                }
                runOnUiThread(new Runnable() {
                    public void run() {
                        BT_Disconnect();
                        if (resultPayment.resultCode == 0){
                            Intent intent = new Intent();
                            intent.putExtra("resultCode", resultPayment.resultCode + ""); // resultCode
                            intent.putExtra("tid", resultPayment.termNum + ""); // 단말기번호
                            intent.putExtra("date", resultPayment.date + ""); // 승인날짜 yyyyMMddHHmmss
                            intent.putExtra("uniqueNum", resultPayment.uniqueNum + ""); // 거래고유번호
                            intent.putExtra("approveNum", resultPayment.approveNum + ""); // 승인번호
                            intent.putExtra("vatVal", resultPayment.taxPrice + ""); // 부가세
                            intent.putExtra("serviceVal", resultPayment.servicePrice + ""); // 봉사료
                            intent.putExtra("originVal", resultPayment.taxFreePrice + ""); // 물품가액
                            intent.putExtra("totalVal", resultPayment.totalPrice + ""); // 총금액
                            intent.putExtra("printMessage", resultPayment.printMessage); // 프린트 메세지
                            intent.putExtra("screenMessage", resultPayment.screenMessage); // 스크린 메세지

                            LOG.d(TAG, "resultCode: " + resultPayment.resultCode + "");
                            LOG.d(TAG, "tid: " + resultPayment.termNum + "");
                            LOG.d(TAG, "date: " + resultPayment.date + "");
                            LOG.d(TAG, "uniqueNum: " + resultPayment.uniqueNum + "");
                            LOG.d(TAG, "approveNum: " + resultPayment.approveNum + "");
                            LOG.d(TAG, "vatVal: " + resultPayment.taxPrice + "");
                            LOG.d(TAG, "serviceVal: " + resultPayment.servicePrice + "");
                            LOG.d(TAG, "originVal: " + resultPayment.taxFreePrice + "");
                            LOG.d(TAG, "totalVal: " + resultPayment.totalPrice + "");
                            LOG.d(TAG, "printMessage: " + resultPayment.printMessage + "");
                            LOG.d(TAG, "screenMessage: " + resultPayment.screenMessage + "");
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                        else if (resultPayment.resultCode != 0){
                            Intent intent = new Intent();
                            intent.putExtra("resultCode", resultPayment.resultCode + "");
                            intent.putExtra("screenMessage", resultPayment.screenMessage); // 스크린 메세지

                            LOG.d(TAG, "resultCode: " + resultPayment.resultCode + "");
                            LOG.d(TAG, "ScreenMessage: " + resultPayment.screenMessage);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void DAOU_OnMSRRead() {
        super.DAOU_OnMSRRead();
    }
    @Override
    public void DAOU_OnCardData(final String maskedCarData) {
        super.DAOU_OnCardData(maskedCarData);
        runOnUiThread(new Runnable() {
            public void run() {
                isOnCardLeading = true;
                edit_cash_receipt_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                edit_cash_receipt_number.setText("****-####-####-****");
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void DAOU_Error_CB(final int code) {
        super.DAOU_Error_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.edit_cash_receipt_number, "");
            }
        });

    }
    @Override
    public void DAOU_CardError_CB(int code){
        super.DAOU_CardError_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.edit_cash_receipt_number, "");
            }
        });

    }
	
	@Override
        public void DAOU_Payment_Incomplete(int resultCode, String message) {
            super.DAOU_Payment_Incomplete(resultCode, message);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    edit_cash_receipt_number.setText("");
                }
            });
        }
		
    @Override
    public void onBackPressed() {
        BT_Disconnect();
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();
        final boolean packageBT = !this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        if(packageBT){
            showToast("BLE를 지원하지 않습니다.");
            return;
        }
        if (BT_IsConnect() == false) {
            LOG.d(TAG, "블루투스 연결요청");
            BT_Connect(null);//블루투스 연결요청메소드

        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if(BT_IsConnect() == true) {
            BT_Disconnect();
        }
    }
    private void registerOnClickListener() {
        (findViewById(R.id.btn_cash_receipt_number_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }
    private void registerTextWatcher() {
        ((EditText)findViewById(R.id.edit_cash_receipt_number)).addTextChangedListener(this);
    }
    private void registerOnFocusChangeListener() {
        (findViewById(R.id.edit_cash_receipt_number)).setOnFocusChangeListener(this);
    }

    /**
     * 안쓰는 메소드들
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void afterTextChanged(Editable s) {
    }
}

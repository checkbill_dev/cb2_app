package kr.checkbill.cb2.activity;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.base.BaseActivity;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.BizInfo;

public class StartBizResultActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_biz_result);

        long id = getIntent().getIntExtra("BizInfoId", 0);
        String date = getIntent().getStringExtra("Date");
        String expireDate = getIntent().getStringExtra("ExpireDate");

        DBHelper mDB = new DBHelper(this);
        BizInfo bizInfo = new BizInfo();
        Cursor c = mDB.getBizInfoCursor();
        while (c.moveToNext()) {
            bizInfo.fetch(c);
            if (bizInfo._id.equals(id)){
                break;
            }
        }
        SetText(R.id.txt_date, date);
        SetText(R.id.txt_shop_name, bizInfo.shopName);
        SetText(R.id.txt_shop_tel, bizInfo.shopTel);
        SetText(R.id.txt_shop_ceo, bizInfo.shopCeo);
        SetText(R.id.txt_shop_id, bizInfo.shopId);
        SetText(R.id.txt_shop_address, bizInfo.shopAddress);
        SetText(R.id.txt_agency_name, bizInfo.agencyName);
        SetText(R.id.txt_agency_tel, bizInfo.agencyTel);
        SetText(R.id.txt_expire_date, expireDate);

    }
}

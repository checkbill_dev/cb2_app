package kr.checkbill.cb2.activity.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import java.util.ArrayList;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.base.BaseActivity;

/**
 * Created by kwonhyeokjin on 2017. 1. 11..
 */

public class BizListAdapter extends BaseAdapter {
    private ArrayList<BizListItem> bizListItemArrayList = new ArrayList<BizListItem>();
    private BizListItem bizListItem;
    @Override
    public int getCount() {
        return bizListItemArrayList.size();
    }
    @Override
    public BizListItem getItem(int position) {
        return bizListItemArrayList.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Context context = parent.getContext();
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_item_bizinfo, parent, false);
        }
        BaseActivity bs = new BaseActivity();
        bizListItem = bizListItemArrayList.get(position);
        bs.SetText((LinearLayout) convertView, R.id.txt_shop_name, bizListItem.shopName);
        bs.SetText((LinearLayout) convertView, R.id.txt_shop_ceo, bizListItem.shopCeo);
        bs.SetText((LinearLayout) convertView, R.id.txt_tid, bizListItem.tid);
        bs.SetText((LinearLayout) convertView, R.id.txt_biz_num, bizListItem.bizNum);
        bs.SetText((LinearLayout) convertView, R.id.txt_shop_tel, bizListItem.shopTel);
        bs.SetText((LinearLayout) convertView, R.id.txt_shop_address, bizListItem.shopAddress);
        bs.SetText((LinearLayout) convertView, R.id.txt_shop_id, bizListItem.shopId);
        return convertView;
    }
    public void addItem(String... values){ // 순서대로 _ID, TID, BIZ_NUM, SHOP_NAME, SHOP_CEO, SHOP_ADDRESS, SHOP_TEL //총 7개
        BizListItem item = new BizListItem();
        item._id = values[0];
        item.tid = values[1];
        item.bizNum = values[2];
        item.shopName = values[3];
        item.shopCeo = values[4];
        item.shopAddress = values[5];
        item.shopTel = values[6];
        item.shopId = values[7];
        bizListItemArrayList.add(item);
    }
}

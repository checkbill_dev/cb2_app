package kr.checkbill.cb2.server;

import java.util.ArrayList;

/**
 * Created by kwonhyeokjin on 2017. 3. 22..
 */

public class GetOrders {
    String shop_id;
    String poscode;
    String ts;
    public GetOrders(String shop_id, String poscode, String ts){
        this.shop_id = shop_id;
        this.poscode = poscode;
        this.ts = ts;
    }
}

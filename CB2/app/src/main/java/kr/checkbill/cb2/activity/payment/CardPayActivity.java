package kr.checkbill.cb2.activity.payment;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.checkbill.cbapi.ResultPayment;

import kr.checkbill.cb2.activity.base.DaouPayActivity;
import kr.checkbill.cb2.activity.SignPadActivity;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.util.ResultPrice;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.Util;

public class CardPayActivity extends DaouPayActivity
        implements View.OnClickListener, TextWatcher, View.OnFocusChangeListener {
    private final static String TAG = "CreditCardPaymentActivity";
    private EditText edit_input_val, edit_month, edit_service_val;
    private Button btn_ok, btn_ok_tint;
    private CheckBox cb_eullyeon;

    private String monthVal;
    private final int REQUEST_CODE = 0;
    private boolean isIC; // IC카드로 5만원이하이고 할부개월수가 입력되어있을때 자동결제
    private ResultPrice price;
    //////////////////////////////////////
    //                 싸인       비밀번호  //
    //은련O, 5만초과       O          O    //
    //은련O, 5만이하       O          O    //
    //                                  //
    //은련X, 5만초과       O          X    //
    //은련X, 5만이하       X          X    //
    //////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_pay);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        edit_input_val = (EditText) findViewById(R.id.edit_input_val);
        edit_month = (EditText) findViewById(R.id.edit_month);
        edit_service_val = (EditText) findViewById(R.id.edit_service_val);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok_tint = (Button) findViewById(R.id.btn_ok_tint);
        cb_eullyeon = (CheckBox) findViewById(R.id.cb_eullyeon);

        registerOnClickListener();
        registerTextWatcher();
        registerOnFocusChangeListener();

        DBHelper mDB = new DBHelper(this);
        BizInfo bizInfo = Util.getBizInfo(this);

        if (bizInfo.vatValInclude.equals("0")) {
            SetText(R.id.txt_vat_val_include1, "부가세 포함 " + bizInfo.vatValPer + "%");
            SetText(R.id.txt_vat_val_include2, "부가세 포함 " + bizInfo.serviceValPer + "%");
        }
        else if (bizInfo.vatValInclude.equals("1")) {
            SetText(R.id.txt_vat_val_include1, "부가세 별도 " + bizInfo.vatValPer + "%");
            SetText(R.id.txt_vat_val_include2, "부가세 별도 " + bizInfo.serviceValPer + "%");
        }


        if (mDB.getSystemEullYeon().equals("0")){
            cb_eullyeon.setVisibility(View.GONE);
        }
        else if (mDB.getSystemEullYeon().equals("1")){
            cb_eullyeon.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_card_number_clear:
                SetText(R.id.txt_card_number, "");
                break;
            case R.id.btn_input_val_clear:
                SetText(R.id.edit_input_val, "");
                break;
            case R.id.btn_month_clear:
                SetText(R.id.edit_month, "");
                break;
            case R.id.btn_service_val_clear:
                SetText(R.id.edit_service_val, "");
                break;
            case R.id.btn_ok:

                if (GetText(R.id.edit_month).isEmpty()
                        || GetText(R.id.edit_month).equals("0")
                        || GetText(R.id.edit_month).equals("1")){
                    monthVal = "0";
                }
                else {
                    monthVal = GetText(R.id.edit_month);
                }



                ResultPrice price = Util.calculatePrice(this, GetText(R.id.edit_input_val), GetText(R.id.edit_service_val), true);

                if (cb_eullyeon.isChecked()) { // 은련카드일때는 50,000 원 초과 검사를 하지않는다.(무조건 싸인을 받음)
                    Intent intent = new Intent(this, SignPadActivity.class);
                    intent.putExtra("Amount", GetText(R.id.edit_input_val));
                    intent.putExtra("IsEullyeon", "eullyeon_yes");
                    startActivityForResult(intent, REQUEST_CODE);
                }
                else {
                    if (price.totalPrice > 50000) {
                        Intent intent = new Intent(this, SignPadActivity.class);
                        intent.putExtra("Amount", GetText(R.id.edit_input_val));
                        intent.putExtra("IsEullyeon", "eullyeon_no");
                        startActivityForResult(intent, REQUEST_CODE);
                    }
                    else {
                        DAOU_Credit(PaymentHistory.TRANTYPE_CREDIT_CARD, null, null,
                                GetText(R.id.edit_input_val), GetText(R.id.edit_service_val),
                                monthVal, 0);
                    }
                }

                break;
        }
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (edit_input_val.hasFocus()) {
            if(GetText(R.id.txt_card_number).length()>=1
                    && GetText(R.id.edit_input_val).length()>=1){
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }

            String commaRemove = (GetText(R.id.edit_input_val)).replace(",", "");;
            if (!commaRemove.isEmpty()){
                if (Double.valueOf(commaRemove) >= 500000000) {
                    StringBuilder sb = new StringBuilder(commaRemove);
                    sb.setCharAt(commaRemove.length() - 1, 'A');
                    SetText(R.id.edit_input_val, sb.toString().replace("A", ""));
                    showToast("500,000,000미만의 값만 입력가능합니다.");
                }
                else {
                    price = Util.calculatePrice(CardPayActivity.this, GetText(R.id.edit_input_val), GetText(R.id.edit_service_val), true);

                    SetText(R.id.txt_total_val, Util.commaFormat(price.totalPrice));
                    SetText(R.id.txt_origin_val, Util.commaFormat(price.taxFreePrice));
                    SetText(R.id.txt_service_val, Util.commaFormat(price.servicePrice));
                    SetText(R.id.txt_vat_val, Util.commaFormat(price.taxPrice));
                    if (!s.toString().equals(Util.commaFormat(commaRemove))) {
                        SetText(R.id.edit_input_val, Util.commaFormat(commaRemove));
                        LOG.d(TAG, "Util.commaFormat(commaRemove): " + Util.commaFormat(commaRemove));
                        edit_input_val.setSelection(GetText(R.id.edit_input_val).length());
                    }
                }
            }

        }
        else if (edit_month.hasFocus()) {
            if (!s.toString().equals("")
                    && Integer.parseInt(s.toString()) > 11) {
                StringBuilder sb = new StringBuilder(s);
                sb.setCharAt(s.length() - 1, 'A');
                SetText(R.id.edit_month, sb.toString().replace("A", ""));
                edit_month.setSelection(edit_month.length());
                showToast("할부는 최대 11개월 까지만 가능합니다.");
            }

            if(GetText(R.id.txt_card_number).length()>=1
                    && GetText(R.id.edit_input_val).length()>=1){
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }
        }
        else if (edit_service_val.hasFocus()) {
            if(GetText(R.id.txt_card_number).length()>=1
                    && GetText(R.id.edit_input_val).length()>=1){
                btn_ok.setVisibility(View.VISIBLE);
                btn_ok_tint.setVisibility(View.GONE);
            }
            else{
                btn_ok.setVisibility(View.GONE);
                btn_ok_tint.setVisibility(View.VISIBLE);
            }

            String commaRemove = (GetText(R.id.edit_service_val)).replace(",", "");;
            if (!commaRemove.isEmpty()) {
                if (Double.valueOf(commaRemove) >= 500000000) {
                    StringBuilder sb = new StringBuilder(commaRemove);
                    sb.setCharAt(commaRemove.length() - 1, 'A');
                    SetText(R.id.edit_service_val, sb.toString().replace("A", ""));
                    showToast("500,000,000미만의 값만 입력가능합니다.");
                }
                else {
                    price = Util.calculatePrice(CardPayActivity.this, GetText(R.id.edit_input_val), GetText(R.id.edit_service_val), true);

                    SetText(R.id.txt_total_val, Util.commaFormat(price.totalPrice));
                    SetText(R.id.txt_origin_val, Util.commaFormat(price.taxFreePrice));
                    SetText(R.id.txt_service_val, Util.commaFormat(price.servicePrice));
                    SetText(R.id.txt_vat_val, Util.commaFormat(price.taxPrice));
                    if (!s.toString().equals(Util.commaFormat(commaRemove))) {
                        SetText(R.id.edit_service_val, Util.commaFormat(commaRemove));
                        edit_service_val.setSelection(GetText(R.id.edit_service_val).length());
                    }
                }
            }

        }
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.edit_input_val:
                if(hasFocus){//포커스를 얻을때
                    edit_input_val.setBackgroundResource(R.drawable.blue_radius);
                }
                else{
                    edit_input_val.setBackgroundResource(R.drawable.gray_radius);
                }
                break;
            case R.id.edit_month:
                if(hasFocus){//포커스를 얻을때
                    edit_month.setBackgroundResource(R.drawable.blue_radius);
                }
                else{
                    edit_month.setBackgroundResource(R.drawable.gray_radius);
                }
                break;
            case R.id.edit_service_val:
                if(hasFocus){//포커스를 얻을때
                    edit_service_val.setBackgroundResource(R.drawable.blue_radius);
                }
                else{
                    edit_service_val.setBackgroundResource(R.drawable.gray_radius);
                }
                break;
        }
    }

    @Override
    public void DAOU_Payment_Incomplete(int resultCode, String message) {
        super.DAOU_Payment_Incomplete(resultCode, message);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SetText(R.id.txt_card_num, "");
            }
        });
    }
		
    @Override
    public void BT_CB(final int resultCode, String BTAdress) {
        super.BT_CB(resultCode, BTAdress);
    }



    @Override
    public void DAOU_Payment_CB(final ResultPayment resultPayment) {
        super.DAOU_Payment_CB(resultPayment);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (resultPayment.resultCode != 0) {
                    SetText(R.id.txt_card_number, "");
                }
            }
        });
    }


    @Override
    public void DAOU_OnICInsert() {
        super.DAOU_OnICInsert();
        isIC = true;
    }
    @Override
    public void DAOU_OnMSRRead() {
        super.DAOU_OnMSRRead();
        isIC = false;
    }

    @Override
    public void DAOU_OnICRemove() {
        super.DAOU_OnICRemove();
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }

    @Override
    public void DAOU_OnFallback(final int fallback) {
        super.DAOU_OnFallback(fallback);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }

    @Override
    public void DAOU_OnCardData(final String maskedCarData) {
        super.DAOU_OnCardData(maskedCarData);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "****-####-####-****");
                if (getSystemIcAutoPay().equals("1")) {
                    if (GetText(R.id.edit_month).isEmpty()
                            || GetText(R.id.edit_month).equals("0")
                            || GetText(R.id.edit_month).equals("1")){
                        monthVal = "0";
                    }
                    else {
                        monthVal = GetText(R.id.edit_month);
                    }
                    if (isIC && !GetText(R.id.edit_input_val).equals("0") && !GetText(R.id.edit_input_val).isEmpty()) {
                        ResultPrice price = Util.calculatePrice(CardPayActivity.this, GetText(R.id.edit_input_val), GetText(R.id.edit_service_val), true);
                        if (price.totalPrice < 50000 && price.totalPrice > 0) {

                            DAOU_Credit(PaymentHistory.TRANTYPE_CREDIT_CARD, null, null,
                                    GetText(R.id.edit_input_val), GetText(R.id.edit_service_val),
                                    monthVal, 0);
                        }
                    }
                }

            }
        });
    }

    @Override
    public void DAOU_Error_CB(final int code) {
        super.DAOU_Error_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }
    @Override
    public void DAOU_CardError_CB(int code){
        super.DAOU_CardError_CB(code);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_card_number, "");
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        LOG.i(TAG, "onActivityResult");
        switch (requestCode){
            case REQUEST_CODE:
                if (resultCode == RESULT_OK){
                    byte[] signByteArray = data.getByteArrayExtra("SignData");
                    String eullyeon = data.getStringExtra("IsEullyeon");
                    String password = data.getStringExtra("Password");
                    LOG.d(TAG, "password: " + password);

                    Bitmap bitmap = BitmapFactory.decodeByteArray(signByteArray, 0, signByteArray.length);

                    if (eullyeon.equals("eullyeon_yes")){
                        DAOU_Credit(PaymentHistory.TRANTYPE_CREDIT_CARD, password, bitmap,
                                GetText(R.id.edit_input_val), GetText(R.id.edit_service_val),
                                monthVal, 0);
                    }
                    else if (eullyeon.equals("eullyeon_no")){
                        DAOU_Credit(PaymentHistory.TRANTYPE_CREDIT_CARD, null, bitmap,
                                GetText(R.id.edit_input_val), GetText(R.id.edit_service_val),
                                monthVal, 0);
                    }
                }
                else if (resultCode == RESULT_CANCELED){
                    showToast("결제가 취소 되었습니다.");
                    finish();
                }
                break;
        }
    }



    private void registerOnClickListener() {
        (findViewById(R.id.img_back)).setOnClickListener(this);
        (findViewById(R.id.btn_card_number_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_input_val_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_month_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_service_val_clear)).setOnClickListener(this);
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
    }
    private void registerTextWatcher() {
        ((EditText) findViewById(R.id.edit_input_val)).addTextChangedListener(this);
        ((EditText) findViewById(R.id.edit_month)).addTextChangedListener(this);
        ((EditText) findViewById(R.id.edit_service_val)).addTextChangedListener(this);
        ((TextView) findViewById(R.id.txt_card_number)).addTextChangedListener(this);
    }
    private void registerOnFocusChangeListener() {
        (findViewById(R.id.edit_input_val)).setOnFocusChangeListener(this);
        (findViewById(R.id.edit_month)).setOnFocusChangeListener(this);
        (findViewById(R.id.edit_service_val)).setOnFocusChangeListener(this);
    }


    /**
     * 안쓰는 메소드들\
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void afterTextChanged(Editable s) {
    }
}

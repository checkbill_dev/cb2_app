package kr.checkbill.cb2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.base.BaseActivity;
import kr.checkbill.cb2.db.DBHelper;

public class SettingEtcActivity extends BaseActivity
        implements View.OnClickListener, CompoundButton.OnCheckedChangeListener{
    private static final String TAG = "SettingEtcActivity";
    private Switch sw_bt_maintain, sw_eullyeon_use, sw_bt_auto_connect, sw_ic_auto_pay;
    private DBHelper mDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_etc);

        sw_bt_maintain = (Switch) findViewById(R.id.sw_bt_maintain);
        sw_eullyeon_use = (Switch) findViewById(R.id.sw_eullyeon_use);
        sw_bt_auto_connect = (Switch) findViewById(R.id.sw_bt_auto_connect);
        sw_ic_auto_pay = (Switch) findViewById(R.id.sw_ic_auto_pay);

        mDB = new DBHelper(this);

        registerOnClickListener();
        registerOnCheckedChangeListener();

        if (mDB.getSystemBtMaintain().equals("0")){
            sw_bt_maintain.setChecked(false);
        }
        else if (mDB.getSystemBtMaintain().equals("1")){
            sw_bt_maintain.setChecked(true);
        }

        if (mDB.getSystemEullYeon().equals("0")){
            sw_eullyeon_use.setChecked(false);
        }
        else if (mDB.getSystemEullYeon().equals("1")){
            sw_eullyeon_use.setChecked(true);
        }

        if (mDB.getSystemBtAutoConnect().equals("0")) {
            sw_bt_auto_connect.setChecked(false);
        }
        else {
            sw_bt_auto_connect.setChecked(true);
        }

        if (mDB.getSystemIcAutoPay().equals("0")) {
            sw_ic_auto_pay.setChecked(false);
        }
        else {
            sw_ic_auto_pay.setChecked(true);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_password_change:
                Intent intent = new Intent(SettingEtcActivity.this, PasswordChangeActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.sw_bt_maintain:
                if (isChecked){
                    mDB.setSystemBtMaintainOn();
                }
                else {
                    mDB.setSystemBtMaintainOff();
                }
                break;
            case R.id.sw_eullyeon_use:
                if (isChecked){
                    mDB.setSystemEullYeonOn();
                }
                else {
                    mDB.setSystemEullYeonOff();
                }
                break;
            case R.id.sw_bt_auto_connect:
                if (isChecked){
                    mDB.setSystemBtAutoConnectON();
                }
                else {
                    mDB.setSystemBtAutoConnectOFF();
                }
                break;
            case R.id.sw_ic_auto_pay:
                if (isChecked){
                    mDB.setSystemIcAutoPayOn();
                }
                else {
                    mDB.setSystemIcAutoPayOff();
                }
                break;
        }
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        showToast("저장되었습니다.");
    }
    private void registerOnClickListener(){
        (findViewById(R.id.img_back)).setOnClickListener(this);
    }
    private void registerOnCheckedChangeListener(){
        sw_bt_maintain.setOnCheckedChangeListener(this);
        sw_eullyeon_use.setOnCheckedChangeListener(this);
        sw_bt_auto_connect.setOnCheckedChangeListener(this);
        sw_ic_auto_pay.setOnCheckedChangeListener(this);
    }
}

package kr.checkbill.cb2.activity.base;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;

import com.checkbill.cbapi.ResultStartBusness;

import kr.checkbill.cb2.R;
import kr.checkbill.cb2.activity.StartBizResultActivity;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.server.ReaderIdResponse;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.Util;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by jinreol on 2017. 4. 13..
 */

public class DaouStartBizActivity extends DaouActivity{
    private static final String TAG = "DaouTerminalActivity";

    private String mIpAddress;
    private String mPortNumber;
    private boolean mIsResult;
    private String mShopId;
    private String mBizNnm;
    private ResultStartBusness mResultStartBusness;
    private String mTid;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * 단말기 개시거래
     * @param ip              IP 어드레스
     * @param port            Port 넘버
     * @param bizNum          사업자번호
     * @param termNum         단말기번호
     * @param shopId          가맹점ID
     * @param isResult        개시거래 화면에서 호출유무
     *
     */
    public void DAOU_StartBusness(String ip, String port, String bizNum,
                                  String termNum, String shopId, boolean isResult) {

//        //하드웨어 넘버
//        String mHwnum;
//        //터미널 넘버
//        String mTermNum = null;
//        //생산일련번호
//        String vanSerial;

//        mHwnum = SingleTon.getInstance().getHWNum();
//        vanSerial = SingleTon.getInstance().getVanSerial();

//        if(Const.isTest) {
//            mTid = Const.TEST_TERMINAL_NUMBER;
//            vanSerial = Const.TEST_VAN_NUMBER;
//            mHwnum = SingleTon.getInstance().getHWNum();
//        }
//        else {
//            mTid = termNum;
//            vanSerial = Const.VAN_NUMBER;
////            vanSerial = SingleTon.getInstance().getVanSerial();
////            mHwnum = Const.HW_NUMBER;
//        }
        mIpAddress = ip;
        mPortNumber = port;
        mIsResult = isResult;
        mShopId = shopId;
        mBizNnm = bizNum;
        mTid = termNum;
        DAOU_StartBusness(ip, port, getHwNum(), bizNum, termNum, getVanSerial(), null);
    }

    @Override
    public void DAOU_StartBusness_CB(final ResultStartBusness resultStartBusness) {
        super.DAOU_StartBusness_CB(resultStartBusness);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mResultStartBusness = resultStartBusness;
                if (resultStartBusness.resultCode == 0) {
                    if(mIsResult) {
                        if (mIsResult) {
                            registerBizInfo(mShopId, mBizNnm, resultStartBusness, "");
                            if(mShopId == null || mShopId.isEmpty() == true) {
                                showToast("단말기 개시거래가 완료되었습니다.");
                            }
                        }
                        else {
                            showToast("단말기 개시거래가 완료되었습니다.");
                        }
                    }
                }
                else {
                    showAlertDialog("알림", new String[]{"(" + resultStartBusness.resultCode + ") " + resultStartBusness.screenMessage}, true,
                            true, false, false, "확인", null, null, null, null, null);
                }
            }
        });
    }
    @Override
    public void onResponse(Call<Object> call, final Response<Object> response) {
        super.onResponse(call, response);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String result_val = ((ReaderIdResponse) response.body()).result_val;
                final String result = ((ReaderIdResponse) response.body()).result;
                final String poscode = ((ReaderIdResponse) response.body()).poscode;
                final String pin = ((ReaderIdResponse) response.body()).pin;
                DBHelper db = new DBHelper(DaouStartBizActivity.this);
                LOG.i(TAG, "result: " + ((ReaderIdResponse) response.body()).result);
                LOG.d(TAG, "result_val: " + result_val);

                if (result.equals("ok")){ // 사업자 번호와 shop_id가 둘다 일치하거나 둘다 존재하지 않거나 save_flag:Y로 보낸경우 => 성공 생성또는 로그인이된다.
                    long id = db.addBizInfo(mTid, mBizNnm, mResultStartBusness.name,
                            mResultStartBusness.ceo, mResultStartBusness.addr, mResultStartBusness.tel,
                            mShopId, mResultStartBusness.agencyName, mResultStartBusness.agencyTel,
                            "", "10", "0", "0", "", "0", poscode, pin, "", mIpAddress, mPortNumber); // DB에 사업자 정보를 입력

                    Intent intent = new Intent(DaouStartBizActivity.this, StartBizResultActivity.class);
                    LOG.d(TAG, "id: " + id);
                    intent.putExtra("BizInfoId", id);
                    intent.putExtra("Date", Util.getDateString(mResultStartBusness.date)); // 개시거래일자
                    intent.putExtra("ExpireDate", mResultStartBusness.expireDate); // 암호키 만료일
                    startActivity(intent);



//                    LinearLayout linear_dialog = (LinearLayout) View.inflate(DaouStartBizActivity.this, R.layout.dialog_terminal_start,null);
//                    final AlertDialog.Builder builder = new AlertDialog.Builder(DaouStartBizActivity.this);
//                    builder.setView(linear_dialog);
//                    SetText(linear_dialog, R.id.txt_date, Util.getDateString(mResultStartBusness.date));
//                    SetText(linear_dialog, R.id.txt_shop_name, mResultStartBusness.name);
//                    SetText(linear_dialog, R.id.txt_shop_tel, mResultStartBusness.tel);
//                    SetText(linear_dialog, R.id.txt_shop_ceo, mResultStartBusness.ceo);
//                    SetText(linear_dialog, R.id.txt_shop_address, mResultStartBusness.addr);
//                    SetText(linear_dialog, R.id.txt_agency_name, mResultStartBusness.agencyName);
//                    SetText(linear_dialog, R.id.txt_agency_tel, mResultStartBusness.agencyTel);
//                    SetText(linear_dialog, R.id.txt_expire_date, mResultStartBusness.expireDate);
//                    SetText(linear_dialog, R.id.txt_shop_id, mShopId);
//                    db.setSystemUseTid(mTid);//단말기 개시거래 시작상태로 변경
//                    db.setSystemMainTid(0, mTid);
//                    db.addBizInfo(mTid, mBizNnm, mResultStartBusness.name,
//                            mResultStartBusness.ceo, mResultStartBusness.addr, mResultStartBusness.tel,
//                            mShopId, mResultStartBusness.agencyName, mResultStartBusness.agencyTel,
//                            "", "10", "0", "0", "", "0", poscode, pin, "", mIpAddress, mPortNumber); // DB에 사업자 정보를 입력
//                    builder.setPositiveButton("확인", new DialogInterface.OnClickListener(){
//                        public void onClick(DialogInterface dialog, int whichButton){
//                            finish();
//                        }
//                    });
//                    AlertDialog dialog = builder.create();
//                    dialog.show();


                }
                else if (result.equals("duplicate")){ // 사업자 번호와 shop_id중에 하나가 중복일 경우
                    if (result_val.equals("duplicate_shop_id")){
                        showAlertDialog("ID 중복", new String[]{"이미 사용중인 ID 입니다."}, true,
                                true, false, false, "확인", null, null, null, null, null);
                    }
                    else if (result_val.equals("duplicate_biz_num")){
                        View.OnClickListener event = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                registerBizInfo(mShopId, mBizNnm, mResultStartBusness, "Y");
                            }
                        };
                        showAlertDialog("ID 재확인", new String[]{"해당 사업자로 가입된 ID가 이미 있습니다.\n" + mShopId + "으(로) 생성을 원하시면 확인 버튼을 눌러주세요."}, false,
                                true, false, false, "확인", "취소", null, event, null, null);
                    }
                }
                else if (result.equals("false")){
                    showAlertDialog("등록 실패", new String[]{"내용을 확인 한 후 다시 시도해주세요."}, true,
                            true, false, false, "확인", null, null, null, null, null);
                }
            }
        });

    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {
        super.onFailure(call, t);
        LOG.e(TAG, "setReaderIdApi onFailure: " + t.getMessage());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DBHelper db = new DBHelper(DaouStartBizActivity.this);
                LinearLayout linear_dialog = (LinearLayout) View.inflate(DaouStartBizActivity.this, R.layout.dialog_terminal_start,null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(DaouStartBizActivity.this);
                builder.setView(linear_dialog);
                SetText(linear_dialog, R.id.txt_date, Util.getDateString(mResultStartBusness.date));
                SetText(linear_dialog, R.id.txt_shop_name, mResultStartBusness.name);
                SetText(linear_dialog, R.id.txt_shop_tel, mResultStartBusness.tel);
                SetText(linear_dialog, R.id.txt_shop_ceo, mResultStartBusness.ceo);
                SetText(linear_dialog, R.id.txt_shop_address, mResultStartBusness.addr);
                SetText(linear_dialog, R.id.txt_agency_name, mResultStartBusness.agencyName);
                SetText(linear_dialog, R.id.txt_agency_tel, mResultStartBusness.agencyTel);
                SetText(linear_dialog, R.id.txt_expire_date, mResultStartBusness.expireDate);
                SetText(linear_dialog, R.id.txt_shop_id, mShopId);
                db.setSystemUseTid(mTid);//단말기 개시거래 시작상태로 변경
                db.setSystemMainTid(0, mTid);
                db.addBizInfo(mTid, mBizNnm, mResultStartBusness.name,
                        mResultStartBusness.ceo, mResultStartBusness.addr, mResultStartBusness.tel,
                        mShopId, mResultStartBusness.agencyName, mResultStartBusness.agencyTel,
                        "", "10", "0", "0", "", "0", "", "", "", mIpAddress, mPortNumber); // DB에 사업자 정보를 입력
                builder.setPositiveButton("확인", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int whichButton){
                        finish();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }
}

package kr.checkbill.cb2.activity.listview;

/**
 * Created by kwonhyeokjin on 2017. 1. 17..
 */

public class PayListItem {
    public String _id;
    public String tranType;
    public String cardNum;
    public String cardName;
    public String cardCompany;
    public String balance;
    public String totalVal;
    public String originVal;
    public String vatVal;
    public String serviceVal;
    public String monthVal;
    public String approvalNumOrg;
    public String approvalNum;
    public String approvalDate;
    public String isKeyIn;
    public String resultCode;
    public String shopTid;
    public String shopBizNum;
    public String shopName;
    public String shopCeo;
    public String shopAddress;
    public String shopTel;
    public String userMsg;
    public String cancelReason;
    public String canceled;
    public String printMessage;
    public String userName;
}

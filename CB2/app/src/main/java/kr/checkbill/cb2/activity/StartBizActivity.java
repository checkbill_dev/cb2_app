package kr.checkbill.cb2.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.checkbill.cbapi.ResultStartBusness;
import kr.checkbill.cb2.activity.base.DaouStartBizActivity;
import kr.checkbill.cb2.R;
import retrofit2.Call;
import retrofit2.Response;

public class StartBizActivity extends DaouStartBizActivity
        implements View.OnClickListener, TextWatcher, View.OnFocusChangeListener{
    private static final String TAG = "TerminalStartActivity";
    private EditText edit_biz_num, edit_shop_id;
    private Button btn_ok, btn_ok_tint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_biz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        edit_biz_num = (EditText) findViewById(R.id.edit_biz_num);
        edit_shop_id = (EditText) findViewById(R.id.edit_shop_id);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok_tint = (Button) findViewById(R.id.btn_ok_tint);

        edit_biz_num.requestFocus();
        edit_shop_id.setPrivateImeOptions("defaultInputmode=english;");// 영문 키보드

        SetText(R.id.edit_ip_address, DAOU_getDefaultIP(false));
        SetText(R.id.edit_port_number, DAOU_getDefaultPort(false));

        registerOnClickListener();
        registerTextWatcher();
        registerOnFocusChangeListener();


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_ok:
                DAOU_StartBusness(GetText(R.id.edit_ip_address), GetText(R.id.edit_port_number), GetText(R.id.edit_biz_num),
                        GetText(R.id.edit_tid), GetText(R.id.edit_shop_id), true);
                break;
            case R.id.btn_biz_num_clear:
                SetText(R.id.edit_biz_num, "");
                break;
            case R.id.btn_tid_clear:
                SetText(R.id.edit_biz_num, "");
                break;
            case R.id.btn_ip_address_clear:
                SetText(R.id.edit_ip_address, "");
                break;
            case R.id.btn_port_number_clear:
                SetText(R.id.edit_port_number, "");
                break;
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        btnOkVisible();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            v.setBackgroundResource(R.drawable.blue_radius);
        }
        else {
            v.setBackgroundResource(R.drawable.gray_radius);
        }
    }
    public void btnOkVisible(){
        if(GetText(R.id.edit_biz_num).length()>=1
                && GetText(R.id.edit_tid).length()>=1
                && GetText(R.id.edit_shop_id).length()>=1
                && GetText(R.id.edit_ip_address).length()>=1
                && GetText(R.id.edit_port_number).length()>=1
                ){
            btn_ok.setVisibility(View.VISIBLE);
            btn_ok_tint.setVisibility(View.GONE);
        }
        else{
            btn_ok.setVisibility(View.GONE);
            btn_ok_tint.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void DAOU_StartBusness_CB(final ResultStartBusness resultStartBusness) {
        super.DAOU_StartBusness_CB(resultStartBusness);
    }

    @Override
    public void onResponse(Call<Object> call, final Response<Object> response) {
        super.onResponse(call, response);
    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {
        super.onFailure(call, t);
    }

    private void registerOnClickListener() {
        (findViewById(R.id.img_back)).setOnClickListener(this);
        (findViewById(R.id.btn_ok)).setOnClickListener(this);
        (findViewById(R.id.btn_biz_num_clear)).setOnFocusChangeListener(this);
        (findViewById(R.id.btn_tid_clear)).setOnFocusChangeListener(this);
        (findViewById(R.id.btn_ip_address_clear)).setOnFocusChangeListener(this);
        (findViewById(R.id.btn_port_number_clear)).setOnFocusChangeListener(this);
    }
    private void registerTextWatcher() {
        ((EditText) findViewById(R.id.edit_biz_num)).addTextChangedListener(this);
        ((EditText) findViewById(R.id.edit_tid)).addTextChangedListener(this);
        ((EditText) findViewById(R.id.edit_ip_address)).addTextChangedListener(this);
        ((EditText) findViewById(R.id.edit_port_number)).addTextChangedListener(this);
    }
    private void registerOnFocusChangeListener() {
        (findViewById(R.id.edit_biz_num)).setOnFocusChangeListener(this);
        (findViewById(R.id.edit_tid)).setOnFocusChangeListener(this);
        (findViewById(R.id.edit_ip_address)).setOnFocusChangeListener(this);
        (findViewById(R.id.edit_port_number)).setOnFocusChangeListener(this);
    }


    /**
     * 안쓰는 메소드들
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void afterTextChanged(Editable s) {
    }
}

package kr.checkbill.cb2.db.table;

import android.database.Cursor;

import java.io.Serializable;

/**
 * Created by kwonhyeokjin on 2017. 1. 10..
 */

/*
CREATE TABLE BIZ_INFO(
    _id               integer PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
    TID               text,
    BIZ_NUM           text,
    SHOP_NAME         text,
    SHOP_CEO          text,
    SHOP_ADDRESS      text,
    SHOP_TEL          text,
    SHOP_ID           text,
    PIN_NUMBER        text,
    POSCODE           text
);
 */
public class BizInfo implements Serializable {
    private static final String TAG = "BizInfo";

    public static final String TABLE_NAME="BIZ_INFO";

    public static final String _ID="_id";
    public static final String TID="TID"; // 단말기번호
    public static final String BIZ_NUM="BIZ_NUM"; // 사업자 등록번호
    public static final String SHOP_NAME="SHOP_NAME"; // 가맹점명
    public static final String SHOP_CEO="SHOP_CEO"; // 대표자명
    public static final String SHOP_ADDRESS="SHOP_ADDRESS"; // 가맹점주소
    public static final String SHOP_TEL="SHOP_TEL"; // 전화번호
    public static final String SHOP_ID="SHOP_ID"; // 사업자 id
    public static final String AGENCY_NAME="AGENCY_NAME"; // 관리대리점이름
    public static final String AGENCY_TEL="AGENCY_TEL"; // 관리대리점 전화번호
    public static final String USER_NAME="USER_NAME"; // 사용자이름
    public static final String VAT_VAL_PER="VAT_VAL_PER"; // 부가세 세율
    public static final String SERVICE_VAL_PER="SERVICE_VAL_PER"; // 봉사료 세율
    public static final String VAT_VAL_INCLUDE="VAT_VAL_INCLUDE"; // 입력금액에 부가세 포함여부 0:부가세포함 1:부가세 별도
    public static final String USER_MSG="USER_MSG"; // 영수증 한줄 메모
    public static final String SERVER_SYNC="SERVER_SYNC"; // 서버랑 동기화하는지 0:동기화안함 1:동기화함
    public static final String POSCODE="POSCODE"; // 직원번호
    public static final String PIN="PIN"; // 핀넘버
    public static final String TS = "TS"; // 서버와 동기화시 사용하는 타임스탬프
    public static final String IP_ADDRESS = "IP_ADDRESS"; // IP주소
    public static final String PORT_NUMBER = "PORT_NUMBER"; // PORT넘버

    public String _id;
    public String tid;
    public String bizNum;
    public String shopName;
    public String shopCeo;
    public String shopAddress;
    public String shopTel;
    public String shopId;
    public String agencyName;
    public String agencyTel;
    public String userName;
    public String vatValPer;
    public String serviceValPer;
    public String vatValInclude;
    public String userMsg;
    public String serverSync;
    public String poscode;
    public String pin;
    public String ts;
    public String ipAdress;
    public String portNumber;


    /*전달받은 커서를 기반으로 사업자정보를 추출하는 메소드*/
    public void fetch(Cursor c){
        /*1. 각 컬럼의 index값을 얻어옴 */
        int idx_id = c.getColumnIndex(BizInfo._ID);
        int idxTid = c.getColumnIndex(BizInfo.TID);
        int idxBizNum = c.getColumnIndex(BizInfo.BIZ_NUM);
        int idxShopName = c.getColumnIndex(BizInfo.SHOP_NAME);
        int idxShopCeo = c.getColumnIndex(BizInfo.SHOP_CEO);
        int idxShopAddress = c.getColumnIndex(BizInfo.SHOP_ADDRESS);
        int idxShopTel = c.getColumnIndex(BizInfo.SHOP_TEL);
        int idxShopId = c.getColumnIndex(BizInfo.SHOP_ID);
        int idxAgencyName = c.getColumnIndex(BizInfo.AGENCY_NAME);
        int idxAgencyTel = c.getColumnIndex(BizInfo.AGENCY_TEL);
        int idxUserName = c.getColumnIndex(BizInfo.USER_NAME);
        int idxVatValPer = c.getColumnIndex(BizInfo.VAT_VAL_PER);
        int idxServiceValPer = c.getColumnIndex(BizInfo.SERVICE_VAL_PER);
        int idxServerSync = c.getColumnIndex(BizInfo.SERVER_SYNC);
        int idxVatValInclude = c.getColumnIndex(BizInfo.VAT_VAL_INCLUDE);
        int idxUserMsg = c.getColumnIndex(BizInfo.USER_MSG);
        int idxPoscode = c.getColumnIndex(BizInfo.POSCODE);
        int idxPin = c.getColumnIndex(BizInfo.PIN);
        int idxTs = c.getColumnIndex(BizInfo.TS);
        int idxIpAddress = c.getColumnIndex(BizInfo.IP_ADDRESS);
        int idxPortNumber = c.getColumnIndex(BizInfo.PORT_NUMBER);



        /*2. 컬럼의 index의 값을 기반으로 쿼라한 값들을 변수에 저장*/
        _id = c.getString(idx_id);
        tid = c.getString(idxTid);
        bizNum = c.getString(idxBizNum);
        shopName = c.getString(idxShopName);
        shopCeo = c.getString(idxShopCeo);
        shopAddress = c.getString(idxShopAddress);
        shopTel = c.getString(idxShopTel);
        shopId = c.getString(idxShopId);
        agencyName = c.getString(idxAgencyName);
        agencyTel = c.getString(idxAgencyTel);
        userName = c.getString(idxUserName);
        vatValPer = c.getString(idxVatValPer);
        serviceValPer = c.getString(idxServiceValPer);
        serverSync = c.getString(idxServerSync);
        vatValInclude = c.getString(idxVatValInclude);
        userMsg = c.getString(idxUserMsg);
        poscode = c.getString(idxPoscode);
        pin = c.getString(idxPin);
        ts = c.getString(idxTs);
        ipAdress = c.getString(idxIpAddress);
        portNumber = c.getString(idxPortNumber);
    }
}

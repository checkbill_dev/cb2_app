package kr.checkbill.cb2.externalcall;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kr.checkbill.cb2.R;

public class ExtMain extends AppCompatActivity {
    private static final String ExtMain = "ExtMain";
    private final int REQUEST_CODE = 0;
    private int tranType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ext_main);

        tranType = getIntent().getIntExtra("tranType", -1);


        String tid;
        String id;
        String password;
        String packageName;
        Intent intent;
        switch (tranType) {
            case 0: // 단말기 개시 거래
                String bizNum = getIntent().getStringExtra("bizNum");
                tid = getIntent().getStringExtra("tid");
                String ipAddress = getIntent().getStringExtra("ipAddress");
                String portNumber = getIntent().getStringExtra("portNumber");
                id = getIntent().getStringExtra("id");
                password = getIntent().getStringExtra("password");
                packageName = getIntent().getStringExtra("packageName");

                intent = new Intent(this, ExtStartBizActivity.class);
                intent.putExtra("bizNum", bizNum);
                intent.putExtra("tid", tid);
                intent.putExtra("ipAddress", ipAddress);
                intent.putExtra("portNumber", portNumber);
                intent.putExtra("id", id);
                intent.putExtra("password", password);
                intent.putExtra("packageName", packageName);
                startActivityForResult(intent, REQUEST_CODE);
                break;
            case 1: // 카드결제
                tid = getIntent().getStringExtra("tid");
                int totalVal = getIntent().getIntExtra("totalVal", -1);
                int originVal = getIntent().getIntExtra("originVal", -1);
                int vatVal = getIntent().getIntExtra("vatVal", -1);
                int serviceVal = getIntent().getIntExtra("serviceVal", -1);
                String month = getIntent().getStringExtra("month");
//                int unionPay = getIntent().getIntExtra("unionPay", -1); // 0:은련카드아님 1:은련카드
                id = getIntent().getStringExtra("id");
                password = getIntent().getStringExtra("password");
                packageName = getIntent().getStringExtra("packageName");

                intent = new Intent(this, ExtCardPayActivity.class);
                intent.putExtra("tid", tid);
                intent.putExtra("totalVal", totalVal);
                intent.putExtra("originVal", originVal);
                intent.putExtra("vatVal", vatVal);
                intent.putExtra("serviceVal", serviceVal);
                intent.putExtra("month", month);
//                intent.putExtra("unionPay", unionPay);
                intent.putExtra("id", id);
                intent.putExtra("password", password);
                intent.putExtra("packageName", packageName);
                startActivityForResult(intent, REQUEST_CODE);
                break;
            case 2: // 카드취소
                tid = getIntent().getStringExtra("tid");
                totalVal = getIntent().getIntExtra("totalVal", -1);
                month = getIntent().getStringExtra("month");
//                unionPay = getIntent().getIntExtra("unionPay", -1); // 0:은련카드아님 1:은련카드
                String date = getIntent().getStringExtra("date"); // 승인날짜 yyyyMMdd
                String approveNum = getIntent().getStringExtra("approveNum");
                id = getIntent().getStringExtra("id");
                password = getIntent().getStringExtra("password");
                packageName = getIntent().getStringExtra("packageName");

                intent = new Intent(this, ExtCardPayCancelActivity.class);
                intent.putExtra("tid", tid);
                intent.putExtra("totalVal", totalVal);
                intent.putExtra("month", month);
                intent.putExtra("date", date);
                intent.putExtra("approveNum", approveNum);
//                intent.putExtra("unionPay", unionPay);
                intent.putExtra("id", id);
                intent.putExtra("password", password);
                intent.putExtra("packageName", packageName);

                startActivityForResult(intent, REQUEST_CODE);
                break;
            case 3: // 현금결제
                tid = getIntent().getStringExtra("tid");
                totalVal = getIntent().getIntExtra("totalVal", -1);
                originVal = getIntent().getIntExtra("originVal", -1);
                vatVal = getIntent().getIntExtra("vatVal", -1);
                serviceVal = getIntent().getIntExtra("serviceVal", -1);
                String type = getIntent().getStringExtra("type"); // 거래구분 "1":소득공제 "2":지출증빙 "3":자진발급
                id = getIntent().getStringExtra("id");
                password = getIntent().getStringExtra("password");
                packageName = getIntent().getStringExtra("packageName");

                intent = new Intent(this, ExtCashPayActivity.class);
                intent.putExtra("tid", tid);
                intent.putExtra("totalVal", totalVal);
                intent.putExtra("originVal", originVal);
                intent.putExtra("vatVal", vatVal);
                intent.putExtra("serviceVal", serviceVal);
                intent.putExtra("type", type);
                intent.putExtra("id", id);
                intent.putExtra("password", password);
                intent.putExtra("packageName", packageName);

                startActivityForResult(intent, REQUEST_CODE);

                break;
            case 4: // 현금취소

                tid = getIntent().getStringExtra("tid");
                totalVal = getIntent().getIntExtra("totalVal", -1);
                type = getIntent().getStringExtra("type"); // 거래취소구분 "A":소득공제 "B":지출증빙 "E":자진발급
                String cancelReason = getIntent().getStringExtra("cancelReason"); // 취소사유코드 "1":거래취소 "2":오류발급 "3":기타
                date = getIntent().getStringExtra("date"); // 승인날짜 yyyyMMdd
                approveNum = getIntent().getStringExtra("approveNum");
                id = getIntent().getStringExtra("id");
                password = getIntent().getStringExtra("password");
                packageName = getIntent().getStringExtra("packageName");

                intent = new Intent(this, ExtCashPayCancelActivity.class);

                intent.putExtra("tid", tid);
                intent.putExtra("totalVal", totalVal);
                intent.putExtra("date", date);
                intent.putExtra("approveNum", approveNum);
                intent.putExtra("type", type);
                intent.putExtra("cancelReason", cancelReason);
                intent.putExtra("id", id);
                intent.putExtra("password", password);
                intent.putExtra("packageName", packageName);

                startActivityForResult(intent, REQUEST_CODE);
                break;
            default:
                intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent intent = new Intent();
        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    switch (tranType){
                        case 0:
                            intent.putExtra("resultCode", data.getStringExtra("resultCode")); // resultCode
                            intent.putExtra("tid", data.getStringExtra("tid")); // 단말기번호
                            intent.putExtra("date", data.getStringExtra("date")); // 개시거날일 yyyyMMddHHmmss
                            intent.putExtra("shopName", data.getStringExtra("shopName")); // 가맹점명
                            intent.putExtra("shopTel", data.getStringExtra("shopTel")); // 가맹점 전화번호
                            intent.putExtra("shopCeo", data.getStringExtra("shopCeo")); // 가맹점대표자명
                            intent.putExtra("shopAddress", data.getStringExtra("shopAddress")); // 가맹점주소
                            intent.putExtra("agencyName", data.getStringExtra("agencyName")); // 관리대리점
                            intent.putExtra("agencyTel", data.getStringExtra("agencyTel")); // 관리대리점 전화번호
                            intent.putExtra("expireDate", data.getStringExtra("expireDate")); // 암호키만료일
                            intent.putExtra("screenMessage", data.getStringExtra("screenMessage")); // 스크린메시지
                            setResult(RESULT_OK, intent);
                            finish();
                            break;
                        case 1:
                            intent.putExtra("resultCode", data.getStringExtra("resultCode")); // resultCode
                            intent.putExtra("tid", data.getStringExtra("tid")); // 단말기번호
                            intent.putExtra("date", data.getStringExtra("date")); // 승인날짜 yyyyMMddHHmmss
                            intent.putExtra("uniqueNum", data.getStringExtra("uniqueNum")); // 거래고유번호
                            intent.putExtra("approveNum", data.getStringExtra("approveNum")); // 승인번호
                            intent.putExtra("issueName", data.getStringExtra("issueName")); // 발급사
                            intent.putExtra("cardName", data.getStringExtra("cardName")); // 매입사
                            intent.putExtra("month", data.getStringExtra("month")); // 할부개월
                            intent.putExtra("vatVal", data.getStringExtra("vatVal")); // 부가세
                            intent.putExtra("serviceVal", data.getStringExtra("serviceVal")); // 봉사료
                            intent.putExtra("originVal", data.getStringExtra("originVal")); // 물품가액
                            intent.putExtra("totalVal", data.getStringExtra("totalVal")); // 총금액
                            intent.putExtra("balance", data.getStringExtra("balance")); // 잔액
                            intent.putExtra("printMessage", data.getStringExtra("printMessage")); // 프린트메시지
                            intent.putExtra("screenMessage", data.getStringExtra("screenMessage")); // 스크린메시지
                            setResult(RESULT_OK, intent);
                            finish();
                            break;
                        case 2:
                            intent.putExtra("resultCode", data.getStringExtra("resultCode")); // resultCode
                            intent.putExtra("tid", data.getStringExtra("tid")); // 단말기번호
                            intent.putExtra("date", data.getStringExtra("date")); // 승인날짜
                            intent.putExtra("uniqueNum", data.getStringExtra("uniqueNum")); // 거래고유번호
                            intent.putExtra("approveNum", data.getStringExtra("approveNum")); // 승인번호
                            intent.putExtra("issueName", data.getStringExtra("issueName")); // 발급사
                            intent.putExtra("cardName", data.getStringExtra("cardName")); // 매입사
                            intent.putExtra("month", data.getStringExtra("month")); // 할부개월
                            intent.putExtra("totalVal", data.getStringExtra("totalVal")); // 총금액
                            intent.putExtra("balance", data.getStringExtra("balance")); // 잔액
                            intent.putExtra("printMessage", data.getStringExtra("printMessage")); // 프린트메시지
                            intent.putExtra("screenMessage", data.getStringExtra("screenMessage")); // 스크린메시지
                            setResult(RESULT_OK, intent);
                            finish();
                            break;
                        case 3:
                            intent.putExtra("resultCode", data.getStringExtra("resultCode")); // resultCode
                            intent.putExtra("tid", data.getStringExtra("tid")); // 단말기번호
                            intent.putExtra("date", data.getStringExtra("date")); // 승인날짜 yyyyMMddHHmmss
                            intent.putExtra("uniqueNum", data.getStringExtra("uniqueNum")); // 거래고유번호
                            intent.putExtra("approveNum", data.getStringExtra("approveNum")); // 승인번호
                            intent.putExtra("vatVal", data.getStringExtra("vatVal")); // 부가세
                            intent.putExtra("serviceVal", data.getStringExtra("serviceVal")); // 봉사료
                            intent.putExtra("originVal", data.getStringExtra("originVal")); // 물품가액
                            intent.putExtra("totalVal", data.getStringExtra("totalVal")); // 총금액
                            intent.putExtra("printMessage", data.getStringExtra("printMessage")); // 프린트메시지
                            intent.putExtra("screenMessage", data.getStringExtra("screenMessage")); // 스크린메시지
                            setResult(RESULT_OK, intent);
                            finish();
                            break;
                        case 4:
                            intent.putExtra("resultCode", data.getStringExtra("resultCode")); // resultCode
                            intent.putExtra("tid", data.getStringExtra("tid")); // 단말기번호
                            intent.putExtra("date", data.getStringExtra("date")); // 승인날짜 yyyyMMddHHmmss
                            intent.putExtra("uniqueNum", data.getStringExtra("uniqueNum")); // 거래고유번호
                            intent.putExtra("approveNum", data.getStringExtra("approveNum")); // 승인번호
                            intent.putExtra("totalVal", data.getStringExtra("totalVal")); // 총금액
                            intent.putExtra("printMessage", data.getStringExtra("printMessage")); // 프린트메시지
                            intent.putExtra("screenMessage", data.getStringExtra("screenMessage")); // 스크린메시지
                            setResult(RESULT_OK, intent);
                            finish();
                            break;
                    }
                }
                else if (resultCode == RESULT_CANCELED){ // 모든취소
                    intent = new Intent();
                    setResult(RESULT_CANCELED, intent);
                    finish();
                }
                break;
        }
    }
}

package kr.checkbill.cb2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import kr.checkbill.cb2.activity.base.DaouActivity;
import kr.checkbill.cb2.db.table.BizInfo;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.util.Util;

public class SettingActivity extends DaouActivity
        implements View.OnClickListener{
    private static final String TAG = "SettingActivity";
    private DBHelper mDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDB = new DBHelper(this);
        registerOnClickListener();
    }
    @Override
    public void onClick(View v){
        Intent intent;
        switch (v.getId()){
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_biz_list:
                intent = new Intent(SettingActivity.this, BizListActivity.class);
                intent.putExtra("CallReason", "SettingActivity");
                startActivity(intent);
                break;

            case R.id.btn_terminal_start:

                if (BT_IsConnect()){
                    intent = new Intent(SettingActivity.this, StartBizActivity.class);
                    startActivity(intent);
                }
                else {
                    showAlertDialog("알림", new String[]{"앱을 리더기와 연결을 한 후에 이용해주세요."}, true,
                            true, false, false, "확인", null, null, null, null, null);
                }

                break;
            case R.id.btn_integrity_check:
                if (BT_IsConnect()){
                    DAOU_VerifyFW();
                }
                else {
                    showAlertDialog("알림", new String[]{"앱을 리더기와 연결을 한 후에 이용해주세요."}, true,
                            true, false, false, "확인", null, null, null, null, null);
                }
                break;

            case R.id.btn_integrity_check_list:
                DAOU_ShowVerifyList();
                break;

            case R.id.btn_security_key_renewal:
                if (BT_IsConnect()){
                    if (mDB.getSystemUseTid().equals("0")){
                        showAlertDialog("알림", new String[]{"앱을 리더기와 연결을 한 후에 이용해주세요."}, true,
                                true, false, false, "확인", null, null, null, null, null);
                    }
                    else {
                        final BizInfo info = Util.getBizInfo(SettingActivity.this);
                        DAOU_KeyUpdate(info.tid, getHwNum(), getVanSerial());
                    }
                }
                else {
                    showAlertDialog("알림", new String[]{"앱을 리더기와 연결을 한 후에 이용해주세요."}, true,
                            true, false, false, "확인", null, null, null, null, null);
                }

                break;

            case R.id.btn_password_change:
                intent = new Intent(SettingActivity.this, SettingEtcActivity.class);
                startActivity(intent);
                break;
        }
    }
    private void registerOnClickListener(){
        (findViewById(R.id.img_back)).setOnClickListener(this);
        (findViewById(R.id.btn_biz_list)).setOnClickListener(this);
        (findViewById(R.id.btn_terminal_start)).setOnClickListener(this);
        (findViewById(R.id.btn_integrity_check)).setOnClickListener(this);
        (findViewById(R.id.btn_integrity_check_list)).setOnClickListener(this);
        (findViewById(R.id.btn_security_key_renewal)).setOnClickListener(this);
        (findViewById(R.id.btn_password_change)).setOnClickListener(this);
    }

}

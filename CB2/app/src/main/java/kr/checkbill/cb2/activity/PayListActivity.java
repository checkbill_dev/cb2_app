package kr.checkbill.cb2.activity;

import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import kr.checkbill.cb2.activity.base.DaouPayActivity;
import kr.checkbill.cb2.activity.listview.PayListAdapter;
import kr.checkbill.cb2.R;
import kr.checkbill.cb2.db.DBHelper;
import kr.checkbill.cb2.db.table.PaymentHistory;
import kr.checkbill.cb2.util.LOG;
import kr.checkbill.cb2.util.Util;

public class PayListActivity extends DaouPayActivity
        implements View.OnClickListener, AdapterView.OnItemClickListener{
    private static final String TAG = "PaymentListActivity";
    private ListView lv_payment_list;
    private TextView txt_biz_empty_message, txt_search_data;
    private EditText edit_search_data;

    private PayListAdapter payListAdapter;
    private String cardOrCash;
    private String startDate;
    private String endDate;
    private String cardData = "";
    private ArrayAdapter<String> adSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txt_search_data = (TextView) findViewById(R.id.txt_search_data);
        lv_payment_list = (ListView) findViewById(R.id.lv_payment_list);
        txt_biz_empty_message = (TextView) findViewById(R.id.txt_biz_empty_message);
        edit_search_data = (EditText) findViewById(R.id.edit_search_data);

        cardOrCash = getIntent().getStringExtra("CardOrCash");
        startDate = getIntent().getStringExtra("StartDate");
        endDate = getIntent().getStringExtra("EndDate");
        SetText(R.id.txt_start_date, startDate);
        SetText(R.id.txt_end_date, "~ " + endDate);



        registerOnClickListener();
        registerOnItemClickListener();

        setIsPaymentListActivity(true);
        lv_payment_list.setAdapter(payListAdapter);

        adSearch = new ArrayAdapter(PayListActivity.this, android.R.layout.simple_list_item_1);
        adSearch.add("승인번호");
        adSearch.add("카드리딩");
        adSearch.add("승인금액");
        adSearch.add("담당자");
        if (cardOrCash.equals("cash")) {
            adSearch.add("현금영수증 번호");
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.txt_search_type:
                LinearLayout linear_dialog = (LinearLayout) View.inflate(PayListActivity.this, R.layout.dialog_search_type_select,null);
                AlertDialog.Builder builder = new AlertDialog.Builder(PayListActivity.this);
                ListView lv_search_type_select;
                lv_search_type_select = (ListView) linear_dialog.findViewById(R.id.lv_search_type_select);
                lv_search_type_select.setAdapter(adSearch);
                builder.setView(linear_dialog);
                final AlertDialog dialog = builder.create();
                dialog.show();

                lv_search_type_select.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String item = adSearch.getItem(position);
                        SetText(R.id.txt_search_type, item);
                        SetText(R.id.edit_search_data, "");
                        SetText(R.id.txt_search_data, "");

                        if (position == 1) {
                            txt_search_data.setVisibility(View.VISIBLE);
                            edit_search_data.setVisibility(View.GONE);
                        }
                        else {
                            edit_search_data.setVisibility(View.VISIBLE);
                            txt_search_data.setVisibility(View.GONE);
                        }
                        dialog.dismiss();

                    }
                });
                break;
            case R.id.btn_search:
                setListView();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startPayResult(1, Long.parseLong(payListAdapter.getItem(position)._id));
    }
    @Override
    public void BT_CB(final int resultCode, String BTAdress) {
        super.BT_CB(resultCode, BTAdress);
    }
    @Override
    public void DAOU_OnICInsert() {
        super.DAOU_OnICInsert();
    }
    @Override
    public void DAOU_OnMSRRead() {
        super.DAOU_OnMSRRead();
    }
    //
    @Override
    public void DAOU_OnICRemove() {
        super.DAOU_OnICRemove();
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_search_data, "");
            }
        });
    }

    @Override
    public void DAOU_OnFallback(final int fallback) {
        super.DAOU_OnFallback(fallback);
    }

    @Override
    public void DAOU_OnCardData(final String maskedCarData) {
        super.DAOU_OnCardData(maskedCarData);
        runOnUiThread(new Runnable() {
            public void run() {
                SetText(R.id.txt_search_data, "****-####-####-****");
                cardData = maskedCarData;
                LOG.d(TAG, "DAOU_OnCardData cardData: " + cardData);
            }
        });
    }
    @Override
    public void DAOU_Error_CB(final int code) {
        super.DAOU_Error_CB(code);
    }
    private void setListView(){
        payListAdapter = new PayListAdapter();
        lv_payment_list.setAdapter(payListAdapter);
        DBHelper mDB = new DBHelper(this);

        String type = null; // 카드와 현금을 구분한다.
        if (cardOrCash.equals("card")){
            type = "5, 10";
        }
        else if (cardOrCash.equals("cash")){
            type = "1, 2, 3, 4, 6, 7, 8, 9";
        }

        String where = String.format("(APPROVAL_DATE >= '%s' AND APPROVAL_DATE <= '%s') AND TRAN_TYPE IN (%s) ", startDate, endDate, type);
        if (GetText(R.id.txt_search_type).equals(adSearch.getItem(0))) { // 승인번호
            where += String.format("AND APPROVAL_NUM = '%s'", GetText(R.id.edit_search_data));
            LOG.d(TAG, "승인번호로검색: " + adSearch.getItem(0));
            LOG.d(TAG, "승인번호로검색: " + GetText(R.id.edit_search_data));
        }
        else if (GetText(R.id.txt_search_type).equals(adSearch.getItem(1))) { // 카드리딩
            LOG.d(TAG, "카드리딩로검색: " + adSearch.getItem(1));
            LOG.d(TAG, "카드리딩로검색: " + cardData);
            where += String.format("AND (CARD_NUM = '%s')", cardData);
            Util.clearString(cardData);

        }
        else if (GetText(R.id.txt_search_type).equals(adSearch.getItem(2))) { // 승인금액 -> editText마다 ,를 붙혀주고 검색시에는 ,를 제거하고 검색한다.
            LOG.d(TAG, "승인금액으로검색: " + adSearch.getItem(2));
            LOG.d(TAG, "승인금액으로검색: " + GetText(R.id.edit_search_data).replace(",", ""));
            String amount;
            amount = GetText(R.id.edit_search_data).replace(",", "");
            where += String.format("AND (TOTAL_VAL %s)", "IN ('" + amount + "', '-" + amount + "')");
        }
        else if (GetText(R.id.txt_search_type).equals(adSearch.getItem(3))) { // 담당자
            LOG.d(TAG, "담당자로검색: " + adSearch.getItem(3));
            LOG.d(TAG, "담당자로검색: " + GetText(R.id.edit_search_data));
            where += String.format("AND (USER_NAME = '%s')", GetText(R.id.edit_search_data).toString());
        }

        if (cardOrCash.equals("cash")) {
            if (GetText(R.id.txt_search_type).equals(adSearch.getItem(4))) { // 현금영수증번호
                LOG.d(TAG, "현금영수증번호로검색: " + adSearch.getItem(4));
                LOG.d(TAG, "현금영수증번호로검색: " + GetText(R.id.edit_search_data).toString());

                try {
                    where += String.format("AND (CARD_NUM = '%s')", Util.cashNumMasking(GetText(R.id.edit_search_data).toString()));
                }
                catch (Exception e) {
                    where += String.format("AND (CARD_NUM = '%s')", GetText(R.id.edit_search_data).toString());
                }

            }
        }


        PaymentHistory paymentHistory = new PaymentHistory();
        Cursor paymentHistoryCursor = mDB.getPaymentHistoryCursorWhere(where);

        if (paymentHistoryCursor.getCount() == 0) {
            txt_biz_empty_message.setVisibility(View.VISIBLE);
        }
        else {
            txt_biz_empty_message.setVisibility(View.GONE);
            paymentHistoryCursor.moveToLast();
            paymentHistory.fetch(paymentHistoryCursor);
            payListAdapter.addItem(
                    paymentHistory._id,
                    paymentHistory.tranType,
                    paymentHistory.cardName,
                    paymentHistory.totalVal,
                    paymentHistory.approvalNum,
                    paymentHistory.approvalDate,
                    paymentHistory.shopName,
                    paymentHistory.canceled);
            runOnUiThread(new Runnable() { // 이거 안써주면 가끔씩 UI Thread 에러남
                @Override
                public void run() {
                    payListAdapter.notifyDataSetChanged();
                }
            });
            while (paymentHistoryCursor.moveToPrevious()){
                paymentHistory.fetch(paymentHistoryCursor);
                payListAdapter.addItem(
                        paymentHistory._id,
                        paymentHistory.tranType,
                        paymentHistory.cardName,
                        paymentHistory.totalVal,
                        paymentHistory.approvalNum,
                        paymentHistory.approvalDate,
                        paymentHistory.shopName,
                        paymentHistory.canceled);
                runOnUiThread(new Runnable() { // 이거 안써주면 가끔씩 UI Thread 에러남
                    @Override
                    public void run() {
                        payListAdapter.notifyDataSetChanged();
                    }
                });
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        setListView();

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Util.clearString(cardData);
    }
    private void registerOnClickListener() {
        (findViewById(R.id.img_back)).setOnClickListener(this);
        (findViewById(R.id.txt_search_type)).setOnClickListener(this);
        (findViewById(R.id.btn_search)).setOnClickListener(this);
    }
    private void registerOnItemClickListener() {
        lv_payment_list.setOnItemClickListener(this);
    }


}

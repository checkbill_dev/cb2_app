//
//  Application 에서 아래와 같이 추가해서 사용하시면 됩니다
//public void onCreate() {
//		Thread.setDefaultUncaughtExceptionHandler( 
//				new CustomUncaughtExceptionHandler(getBaseContext() , getContentResolver() , Thread.getDefaultUncaughtExceptionHandler()) );
//	}


package kr.checkbill.cb2.sendlog;

import android.content.Context;
import android.content.SharedPreferences;

import kr.checkbill.cb2.util.LOG;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;



public class CustomUncaughtExceptionHandler implements UncaughtExceptionHandler {

	private final static String TAG = "CustomUncaughtExceptionHandler";
	private final static String PREF = "pref_anr";
	private final static String IS_ANR = "isANR";
	
	public static boolean isANR = false;
	
	private Context mContext;
	private UncaughtExceptionHandler mDefaultUncaughtExceptionHandler;
	
	public CustomUncaughtExceptionHandler(Context context, UncaughtExceptionHandler uncaughtExceptionHandler) {
		this.mContext = context;
		this.mDefaultUncaughtExceptionHandler = uncaughtExceptionHandler;
	}
	
    public static void saveIsANR(Context context, boolean is_anr) {
    	 SharedPreferences pref = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
         SharedPreferences.Editor edit = pref.edit();
         edit.putBoolean(IS_ANR, is_anr);
         edit.commit();
    }
    
    public static boolean getIsANR(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        boolean is_anr  = pref.getBoolean(IS_ANR, false);
        return is_anr;
    }
    

	@Override
	public void uncaughtException(Thread _thread, Throwable _throwable) {
		LOG.e(TAG, "uncaughtException:");
		
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		_throwable.printStackTrace( printWriter );
		String stacktrace = result.toString();
		printWriter.close();
		
		String mail_title = "다우데이터";

		String mail_body = "";
		mail_body = "1) App 패키지명: "+ SendLogUtil.getPackageName(mContext) +
				"\n\n"+ "2) App 버전: "+ SendLogUtil.getVersionNumber(mContext) +
	           "\n\n"+ "3) 안드로이드 버전: "+ SendLogUtil.getAndroidVersion() +
		       "\n\n"+ "4) 커널정보: " + SendLogUtil.getFormattedKernelVersion() +
		       "\n\n"+ "5) 빌드넘버: "+ SendLogUtil.getBuildNumber()+
		       "\n\n"+ "6) 사용자 ID: "+SendLogUtil.getCurrentID(mContext) +
		       "\n\n"+ "7) 단말기 S/N: "+SendLogUtil.getSerialNumber(mContext) +
		       "\n\n" + "8) stacktrace:\n" + stacktrace;
		
		LOG.e(TAG, "stacktrace:"+stacktrace);
		
		
		SendLogUtil.sendMailNoUI(mContext, "[App Stop] " + mail_title, mail_body); //로그 보내기
		
		saveIsANR(mContext, true);
		mDefaultUncaughtExceptionHandler.uncaughtException(_thread, _throwable);
	}

}

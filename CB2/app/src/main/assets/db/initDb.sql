CREATE TABLE SYSTEM_SETTING(
    PASSWORD           text,
    VAN_SERIAL         text,
    PAPER_CNT          integer,
    SW_AUTH_NUM        text,
    USE_TID            text,
    MAIN_TID           text,
    BT_MAINTAIN        text,
    EULLYEON_USE       text,
    BT_AUTO_CONNECT    text,
    RCT_BT_NUM         text,
    IC_AUTO_PAY        text
);


CREATE TABLE BIZ_INFO(
    _id               integer PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
    TID               text,
    BIZ_NUM           text,
    SHOP_NAME         text,
    SHOP_CEO          text,
    SHOP_ADDRESS      text,
    SHOP_TEL          text,
    SHOP_ID           text,
    AGENCY_NAME       text,
    AGENCY_TEL        text,
    USER_NAME         text,
    VAT_VAL_PER       text,
    SERVICE_VAL_PER   text,
    SERVER_SYNC       text,
    VAT_VAL_INCLUDE   text,
    USER_MSG          text,
    POSCODE           text,
    PIN               text,
    TS                text,
    IP_ADDRESS        text,
    PORT_NUMBER       text
);

CREATE TABLE PAYMENT_HISTORY(
    _id                 integer PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
    TRAN_TYPE           integer,
    CARD_NUM            text,
    CARD_NAME           text,
    CARD_COMPANY        text,
    BALANCE             integer,
    TOTAL_VAL           integer,
    ORIGIN_VAL          integer,
    VAT_VAL             integer,
    SERVICE_VAL         integer,
    MONTH_VAL           integer,
    APPROVAL_NUM_ORG    text,
    APPROVAL_NUM        text,
    APPROVAL_DATE       text,
    IS_KEY_IN           text,
    RESULT_CODE         text,
    SHOP_TID            text,
    SHOP_BIZ_NUM        text,
    SHOP_NAME           text,
    SHOP_CEO            text,
    SHOP_ADDRESS        text,
    SHOP_TEL            text,
    USER_MSG            text,
    CANCEL_REASON       integer,
    CANCELED            integer,
    SEND_DATA           text,
    SEND_FAILED_REASON  text,
    SHOP_ID             text,
    PIN                 text,
    POSCODE             text,
    EULLYEON            integer,
    VAT_VAL_PER         text,
    SERVICE_VAL_PER     text,
    VAT_VAL_INCLUDE     text,
    USER_NAME           text,
    IS_SERVER_DATA      text,
    PRINT_MESSAGE       text,
    IS_MASKED           text,
    APPROVAL_NUM_DEF    text
);


INSERT INTO SYSTEM_SETTING(PASSWORD, VAN_SERIAL, PAPER_CNT, SW_AUTH_NUM, USE_TID, MAIN_TID, BT_MAINTAIN, EULLYEON_USE, BT_AUTO_CONNECT, RCT_BT_NUM, IC_AUTO_PAY)
VALUES('991232', '0', 1, 'swCHECKBILL0011000', '0', '0', '1', '0', '0', '0', '0');
